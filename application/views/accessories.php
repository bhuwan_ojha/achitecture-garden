﻿<html>
<head>
    <title> Architectural Gardens | Accessories</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/js_website_language.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
<div class="banar_content" style="background:rgba(0,0,0,0) url(<?php echo BASE_URL();?>assets/images/category/coll_img15_lg-min.png) no-repeat scroll center top">
<div class="container">
<div class="banar_in">
<h2>ACCESSORIES</h2>
</div>
</div>
</div>
<div>
<div class="left"><div class="left_top_bg"></div><div class="left_center_bg"></div><div class="left_bottom_bg"></div><div style="clear:both"></div></div><div class=""><div class="center_content"><div id="products_list_content"><div class='page_nav_line'></div><div id="products_list_content" class="product_list_grid">
<div class="container">
<div class="product_nav" style="background:none;margin-bottom:5px">
<div id='top_breadcrumb'><span><div><span itemscope itemtype="#"><a href="#" class="headerNavigation" itemprop="url"><span itemprop="title">Home</span></a></span> > <span itemscope itemtype="#"><a href="<?php echo BASE_URL();?>collection" class="headerNavigation" itemprop="url"><span itemprop="title">Collection</span></a></span> > <span itemscope itemtype="#"><span style="color:#000;text-transform:uppercase;font-weight:bold" itemprop="title">ACCESSORIES</span></span></div></span></div><div class='clear'></div> <div class="clear"></div>
</div>
</div>
<div style="overflow:auto;position:relative;height:60px;line-height:60px;border-top:1px solid #ededed;border-bottom:1px solid #ededed">
<div class="container subcategory" style="">
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">Cantilevers</span>
</a>
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">SPECIALTY</span>
</a>
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">MARKET</span>
</a>
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">COMMERCIAL</span>
</a>
<a href="#" style="margin-right:45px">
<span style="font-weight:bold;text-transform:uppercase">ACCESSORIES</span>
</a>
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">Fabrics</span>
</a>
</div>
</div>
<div class="product">
<div class="container">
<div class="product_in">
<div class="left_box">
<div class="left_box_title category_box_title"></div><div lhref="#" lcid="54" class="leftbox_line main_line" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">BASE COLLECTION</a></div></div><div lhref="#" lcid="55" class="leftbox_line main_line" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">UMBRELLA LIGHTS</a></div></div><div lhref="#" lcid="59" class="leftbox_line main_line has_child" style="cursor:pointer"><div style="margin-right:20px"><a class="" href="#">OUTDOOR RUGS</a></div></div><div lpcid="59" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Gold Collection</a></div></div><div lpcid="59" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Silver Collection</a></div></div><div lhref="#" lcid="60" class="leftbox_line main_line has_child" style="cursor:pointer"><div style="margin-right:20px"><a class="" href="#">PROTECTIVE FURNITURE COVERS</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Fire Pit Collection</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Modular Collection</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Round Tables & Chairs</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Oval/Rectangle/Square Tables & Chairs</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Chair / Chaise / Sofa / Loveseat</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Deep Seating</a></div></div><div lpcid="60" class="leftbox_line sub_line hide_div" style="cursor:pointer"><div style="border:0 solid red"><a class="" href="#">Outdoor Accessories</a></div></div></div><script>$(function(){$(".leftbox_line.main_line.selected.has_child").each(function(){var a=$(this).attr("lcid");var b=0;$(".leftbox_line.sub_line").each(function(){if($(this).attr("lpcid")==a){$(this).show()}})});$(".leftbox_line.main_line.has_child").click(function(){var a=$(this).attr("lcid");if($(this).attr("class").indexOf("selected")>0){$(".leftbox_line.sub_line").each(function(){if($(this).attr("lpcid")==a){$(this).hide()}});$(this).removeClass("selected")}else{var b=0;$(".leftbox_line.sub_line").each(function(){if($(this).attr("lpcid")==a){$(this).show();b++}});if(b>0){$(this).addClass("selected")}else{document.location.href=$(this).attr("lhref")}}return false})});</script> <ul class="plist threeinarow" style="width:75%">
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png'"></span>
<small>Commercial</small><em>BA150 Series <br>Cast Aluminum - 150 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png'"></span>
<small>Steel Base w/casters</small><em>BSK120 Series <br>Steel - 120 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png'"></span>
<small>Monaco</small><em>BKM100 Round Series <br>Cast Aluminum - 100 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BKMSQ100_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BKMSQ100_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BKMSQ100_thumb_min.png'"></span>
<small>Monaco</small><em>BKMSQ100 Square Series <br>Cast Aluminum - 100 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BA50_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BA50_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BA50_thumb_min.png'"></span>
<small>Art Deco</small><em>BA50 Series <br>Cast Aluminum - 50 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BG50_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BG50_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BG50_thumb_min.png'"></span>
<small>Garden</small><em>BG50 Series <br>Cast Aluminum - 50 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BW50_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BW50_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BW50_thumb_min.png'"></span>
<small>Classic</small><em>BW50 Series <br>Cast Iron - 50 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BS359_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BS359_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BS359_thumb_min.png'"></span>
<small>Steel</small><em>BS Series <br>Steel - 35 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png'"></span>
<small>Steel</small><em>BS Series <br>Steel - 70 lbs.</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BW30_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BW30_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BW30_thumb_min.png'"></span>
<small>Add-On Weight</small><em>Cast Iron - 30 lbs. <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BT_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BT_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BT_thumb_min.png'"></span>
<small>2″ Aluminum Stem </small><em>For Cast Aluminum Bases <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BT_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BT_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BT_thumb_min.png'"></span>
<small>2″ Aluminum Stem </small><em>For BW50 Series Base Only <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BASE_13_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BASE_13_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BASE_13_thumb_min.png'"></span>
<small>AKZ13 Base</small><em>400lb. Base Option for all AKZ's <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BASE_AKZ_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BASE_AKZ_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BASE_AKZ_thumb_min.png'"></span>
<small>AKZ Base</small><em>200lb. Base Option for AKZ and AKZSQ <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AMK_C_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AMK_C_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AMK_C_thumb_min.png'"></span>
<small>Concrete Mount Kit</small><em>Mount Kit for AKZ Series <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AMK_G_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AMK_G_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AMK_G_thumb_min.png'"></span>
<small>In-Ground Mount Kit</small><em>Mount Kit for AKZ Series <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AMK_W_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AMK_W_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AMK_W_thumb_min.png'"></span>
<small>Wood Mount Kit</small><em>Mount Kit for AKZ Series <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BASE_AKZ_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BASE_AKZ_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BASE_AKZ_thumb_min.png'"></span>
<small>AG Base</small><em>200lb. Base for AG28 Series <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BF10_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BF10_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BF10_thumb_min.png'"></span>
<small>Resin Base Weights</small><em>For AG19 Series <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BXAG_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BXAG_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BXAG_thumb_min.png'"></span>
<small>BX-AG Cross Base Weights</small><em>For AG19 Series <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png'"></span>
<small>LUNA</small><em>Umbrella Light with Bluetooth Speaker <br>16 LED Lights</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png'"></span>
<small>Vega-L</small><em>Umbrella Light <br>36 LED Lights</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/MauiGold_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/MauiGold_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/MauiGold_16_thumb_min.png'"></span>
<small>Maui - Gold</small><em>Gold Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/MauiSilver_16_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/MauiSilver_16_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/MauiSilver_16_thumb.png'"></span>
<small>Maui - Silver</small><em>Gold Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Birmingham_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Birmingham_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Birmingham_16_thumb_min.png'"></span>
<small>Birmingham - Almond</small><em>Gold Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Savannah_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Savannah_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Savannah_17_thumb.png'"></span>
<small>Savannah - Cottonwood</small><em>Gold Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/NorthShore_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/NorthShore_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/NorthShore_17_thumb.png'"></span>
<small>North Shore - Lagoon</small><em>Gold Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/RidgeCharcoal_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/RidgeCharcoal_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/RidgeCharcoal_17_thumb.png'"></span>
<small>Ridge - Charcoal</small><em>Gold Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Athens_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Athens_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Athens_17_thumb.png'"></span>
<small>Athens - Silver</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Champagne_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Champagne_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Champagne_16_thumb_min.png'"></span>
<small>Champagne - Neptune</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Blueberry_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Blueberry_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Blueberry_16_thumb_min.png'"></span>
<small>Garden Cottage - Blueberry</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Charleston_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Charleston_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Charleston_16_thumb_min.png'"></span>
<small>Charleston - Spa</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CharlestonHoney_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CharlestonHoney_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CharlestonHoney_16_thumb_min.png'"></span>
<small>Charleston - Honey</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CharlestonHenna_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CharlestonHenna_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CharlestonHenna_16_thumb_min.png'"></span>
<small>Charleston - Henna</small><em>Silver Collections <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/FlorenceMocha_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/FlorenceMocha_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/FlorenceMocha_16_thumb_min.png'"></span>
<small>Florence - Mocha</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/FlorenceChestnut_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/FlorenceChestnut_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/FlorenceChestnut_16_thumb_min.png'"></span>
<small>Florence - Chestnut</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/LatticeTeak_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/LatticeTeak_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/LatticeTeak_16_thumb_min.png'"></span>
<small>Lattice - Teak and Black</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Redwood_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Redwood_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Redwood_16_thumb_min.png'"></span>
<small>Lodge - Redwood</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/TuscanBirch_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/TuscanBirch_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/TuscanBirch_17_thumb.png'"></span>
<small>Tuscan - Birch</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Caramel_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Caramel_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Caramel_16_thumb_min.png'"></span>
<small>Linen - Caramel Macchiato</small><em>Silver Collection <br></em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP929_16_thumb_minb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP929_16_thumb_minb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP929_16_thumb_minb.png'"></span>
<small>CP929</small><em>Fire Pit Collection <br>Fits all 36" to 42" Round Fire Pits</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP930_16_thumb_minb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP930_16_thumb_minb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP930_16_thumb_minb.png'"></span>
<small>CP930</small><em>Fire Pit Collection <br>Fits 48" to 54" Round Fire Pits</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP932_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP932_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP932_16_thumb_min.png'"></span>
<small>CP932</small><em>Fire Pit Collection <br>Fits 42" to 48" Square Fit Pits</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP933_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP933_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP933_16_thumb_min.png'"></span>
<small>CP933</small><em>Fire Pit Collection <br>Fits 50" x 30" Rectangle Chat and Fire Pits</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP936_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP936_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP936_16_thumb_min.png'"></span>
<small>CP936</small><em>Fire Pit Collection <br>Fits 58" x 38" Rectangle Chat and Fire Pits</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP301_16_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP301_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP301_16_thumb.png'"></span>
<small>CP301</small><em>Modular Style Collection <br>Fits End Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP302_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP302_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP302_16_thumb_min.png'"></span>
<small>CP302</small><em>Modular Style Collection <br>Fits Middle Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP303_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP303_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP303_16_thumb_min.png'"></span>
<small>CP303</small><em>Modular Style Collection <br>Fits End Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP304_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP304_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP304_16_thumb_min.png'"></span>
<small>CP304</small><em>Modular Style Collection <br>Fits Corner Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP305_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP305_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP305_16_thumb_min.png'"></span>
<small>CP305</small><em>Modular Style Collection <br>Extends Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP306_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP306_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP306_16_thumb_min.png'"></span>
<small>CP306</small><em>Modular Style Collection <br>Fits Corner Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP531_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP531_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP531_16_thumb_min.png'"></span>
<small>CP531</small><em>Round Table and Chairs <br>36" Bistro/Cafe Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP551_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP551_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP551_16_thumb_min.png'"></span>
<small>CP551</small><em>Round Tables and Chairs <br>48" Round Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP552_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP552_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP552_16_thumb_min.png'"></span>
<small>CP552</small><em>Round Table and Chairs <br>48" Round Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP571_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP571_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP571_16_thumb_min.png'"></span>
<small>CP571</small><em>Round Table and Chairs <br>54" Round Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP572_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP572_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP572_16_thumb_min.png'"></span>
<small>CP572</small><em>Round Table and Chairs <br>54" Round Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP590_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP590_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP590_16_thumb_min.png'"></span>
<small>CP590</small><em>Round Table and Chairs <br>60" Round Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP591_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP591_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP591_16_thumb_min.png'"></span>
<small>CP591</small><em>Round Table and Chairs <br>60" Round Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP586_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP586_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP586_16_thumb_min.png'"></span>
<small>CP586</small><em>Oval/Rectangle/Square Table and Chairs <br>Fits Small Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP587_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP587_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP587_16_thumb_min.png'"></span>
<small>CP587</small><em>Oval/Rectangle/Square Table and Chairs <br>Fits Small Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP584_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP584_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP584_16_thumb_min.png'"></span>
<small>CP584</small><em>Oval/Rectangle/Square Table and Chairs <br>Fits Medium Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP585_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP585_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP585_16_thumb_min.png'"></span>
<small>CP585</small><em>Oval/Rectangle/Square Table and Chairs <br>Fits Medium Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP699_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP699_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP699_16_thumb_min.png'"></span>
<small>CP699</small><em>Oval/Rectangle/Square Table and Chairs <br>X-Large Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP694_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP694_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP694_16_thumb_min.png'"></span>
<small>CP694</small><em>Oval/Rectangle/Square Table and Chairs <br>X-Large Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP697_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP697_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP697_16_thumb_min.png'"></span>
<small>CP697</small><em>Oval/Rectangle/Square Table and Chairs <br>2XL Large Oval/Rectangle Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP598_16_thumbb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP598_16_thumbb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP598_16_thumbb_min.png'"></span>
<small>CP598</small><em>Oval/Rectangle/Square Table and Chairs <br>72" Square Table and Chairs</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP111_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP111_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP111_16_thumb_min.png'"></span>
<small>CP111</small><em>Chair/Chaise/Sofa Loveseat <br>Stack of Chairs/Bar Stools</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP112_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP112_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP112_16_thumb_min.png'"></span>
<small>CP112</small><em>Chair/Chaise/Sofa Loveseat <br>High Back Chair</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP113_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP113_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP113_16_thumb_min.png'"></span>
<small>CP113</small><em>Chair/Chaise/Sofa Loveseat <br>Recliner Chair</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP122_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP122_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP122_16_thumb_min.png'"></span>
<small>CP122</small><em>Chair/Chaise/Sofa Loveseat <br>Loveseat Glider</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP121S_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP121S_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP121S_16_thumb_min.png'"></span>
<small>CP121S</small><em>Chair/Chaise/Sofa Loveseat <br>Small Chaise Lounge</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP121L_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP121L_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP121L_16_thumb_min.png'"></span>
<small>CP121L</small><em>Chair/Chaise/Sofa Loveseat <br>Large Chaise Lounge</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP141_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP141_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP141_16_thumb_min.png'"></span>
<small>CP141</small><em>Chair/Chaise/Sofa Loveseat <br>Double Chaise Lounge</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP210_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP210_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP210_16_thumb_min.png'"></span>
<small>CP210</small><em>Deep Seating <br>Large Ottoman</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP211_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP211_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP211_16_thumb_min.png'"></span>
<small>CP211</small><em>Deep Seating <br>Lounge Chair</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP212_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP212_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP212_16_thumb_min.png'"></span>
<small>CP212</small><em>Deep Seating <br>Large Lounge Chair or Rocker</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP222_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP222_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP222_16_thumb_min.png'"></span>
<small>CP222</small><em>Deep Seating <br>Loveseat</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP223_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP223_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP223_16_thumb_min.png'"></span>
<small>CP223</small><em>Deep Seating <br>Sofa</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP241_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP241_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP241_16_thumb_min.png'"></span>
<small>CP241</small><em>Deep Seating <br>X-Large Lounge Chair</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP242_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP242_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP242_16_thumb_min.png'"></span>
<small>CP242</small><em>Deep Seating <br>X-Large Loveseat or Corner Sectional</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP243_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP243_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP243_16_thumb_min.png'"></span>
<small>CP243</small><em>Deep Seating <br>X-Large Sofa</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP901_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP901_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP901_16_thumb_min.png'"></span>
<small>CP901</small><em>Outdoor Accessories <br>Large Umbrella Style</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png'"></span>
<small>CP902</small><em>Outdoor Accessories <br>X-Large</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP910_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP910_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP910_16_thumb_min.png'"></span>
<small>CP910</small><em>Outdoor Accessories <br>Rectangle Market Styles</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP912_16_thumbb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP912_16_thumbb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP912_16_thumbb_min.png'"></span>
<small>CP912</small><em>Outdoor Accessories <br>AG19 and AG3 Cantilever Styles</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP908_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP908_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP908_16_thumb_min.png'"></span>
<small>CP908</small><em>Outdoor Accessories <br>AG28 Series Styles</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP920_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP920_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP920_16_thumb_min.png'"></span>
<small>CP920</small><em>Outdoor Accessories <br>AKZ Series Styles</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP922_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP922_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP922_16_thumb_min.png'"></span>
<small>CP922</small><em>Outdoor Accessories <br>Fits all 28" Square and 24" Round Tables</em>
</a>
</li>
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP923_16_thumb_min.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP923_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP923_16_thumb_min.png'"></span>
<small>CP923</small><em>Outdoor Accessories <br>Fits all Oval and Rectangle Occasional Tables</em>
</a>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
</div>
</div><div class='page_nav_line'></div></div></div></div><div class="right"></div>
</div>
<div class="bottom">
<?php include "includes/footer.php";?>