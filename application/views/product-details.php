<html>
<head>
    <title> Architectural Gardens | Product Details </title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
<div class="center">
<div class="left"></div><div class="center"><div class="home"><div style="display:none" class="detail_page_navigation"><div id='top_breadcrumb'><span><div><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://treasuregarden.com/default.php" class="headerNavigation" itemprop="url"><span itemprop="title">Home</span></a></span> > <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://treasuregarden.com/category/1/Collection.html" class="headerNavigation" itemprop="url"><span itemprop="title">Collection</span></a></span> > <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://treasuregarden.com/category/47/COMMERCIAL.html" class="headerNavigation" itemprop="url"><span itemprop="title">COMMERCIAL</span></a></span> > <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span style="color:#000;text-transform:uppercase;font-weight:bold" itemprop="title">UCP409</span></span></div></span></div><div class='clear'></div></div><div style="display:none" class="product_info clearfix" itemscope itemtype=""><div class="product_info_l"><div class="product_info_inner"><div class="product_info_inner_l"><div style="text-align:left;margin-top:20px"></div></div><div class="product_info_inner_r"><div class="product_name" id="id_product_name" itemprop="name" style="">Commercial</div><div style="margin-top:20px;border-top:2px solid #ddd;border-bottom:2px solid #ddd;height:40px;line-height:40px"><div style="float:left;margin-right:120px"><div id="id_product_list_price_and_you_save" style="float:left;margin-right:10px"></div></div><div class="customer_reviews_line" style="margin:10px 0;float:left"><div style="float:left">
<div class=product_detail_short_comment_info_c>
<img align="absmiddle" style="float:left" src="<?php echo BASE_URL();?>assets/images/star0.png">
<div id="id_customer_review" style="float:left;margin-left:2px;margin-top:2px;color:#000;font-size:14px;height:12px;line-height:12px">
(0)
</div>
<div class="clear"></div>
</div>
</div></div><div style="float:right"><div class="button_submit"><a class="btn_write_review_link" href="javascript:" onclick="avascript:js_user.login('http://treasuregarden.com/commercial-p-109.html',1)">WRITE A REVIEW</a></div></div><div class="clear"></div></div><div style="padding:20px 0"><div class="tilling_section">
<div class="container">
<div class="tilling_img">
<img alt="img" height="" src="<?php echo BASE_URL();?>assets/images/UCP_blk_hub.png" width=""></div>
<div class="tilling_txt">
<img alt="img" height="80" src="<?php echo BASE_URL();?>assets/images/icon_commpopup.png" width="80">
<div class="clear">
&nbsp;</div>
<h2>
POP-UP FEATURE</h2>
<p>
Umbrella slides up and pops into place.</p>
</div>
<div class="clear">
&nbsp;</div>
</div>
</div>
<div class="base_section">
<div class="container">
<div class="base_img base_img_ext">
<img alt="img" height="" src="<?php echo BASE_URL();?>assets/images/UCP_blk_pole_785x550.png" width=""></div>
<div class="base_txt base_txt_ext">
<img alt="img" height="81" src="<?php echo BASE_URL();?>assets/images/icon_commgrade.png" width="80">
<div class="clear">
&nbsp;</div>
<h2>
COMMERCIAL GRADE</h2>
<p>
1.5 mm thick aluminum pole is<br>
built for commercial use.</p>
</div>
<div class="clear">
&nbsp;</div>
</div>
</div>
</div></div><div class="clear"></div></div></div><div class="clear"></div></div><div class="pro_details">
<div class="container">
<div class="specifications">
<div class="rectangle">
<div><img src="<?php echo BASE_URL();?>assets/images/products/UCP409_17_main.png" alt="img"><h2>Commercial</h2></div>
<p style="background:none"><div>
<span>Item Shown: <em>UCP409-09</em></span><br>
<span>Fabric: <em>5403 Jockey Red</em></span><br>
<span>Base: <em>BKM1009 (Sold separately)</em></span></div>
</p>
<div>
<div>
<div>
<div class="rectangle_txt1">
<span>Item No.: <em>UCP409</em></span> <span>Size: <em>9&rsquo;</em></span> <span>Ribs: <em>8</em></span> <span>Shape: <em>Octagon</em></span> <span>Height Open: <em>93.38&rdquo;</em></span> <span>Coverage: <em>57 Sq. Ft.</em></span></div>
<div class="rectangle_txt2">
<span><span>Lift: <em>Pop-up / SS Pin</em></span></span> <span><span>Pole Diameter: <em>1.53&rdquo; Smooth </em></span></span><span>Pole Wall: <em>0.06&quot;</em></span> <span>Weight: <em>18 lbs</em></span> <span>Vent Style: <em>Single Wind Vent (SWV), Double Wind Vent (DWV)</em></span></div>
</div>
</div>
</div>
<div>
&nbsp;</div>
<div class="clear"></div>
<h3>Available Finishes:</h3>
<ul>
<li><a class="color1" style="cursor:default;background:rgba(0,0,0,0) url(http://treasuregarden.com/images/sysicons/Finish_Comm_SS.jpg) no-repeat scroll 0 0" href="javascript:">Silver Shadow, Anodized</a><h4>UCP409-SS</h4></li>
<li><a class="color1" style="cursor:default;background:rgba(0,0,0,0) url(http://treasuregarden.com/images/sysicons/Finish_Auto_Bronze.jpg) no-repeat scroll 0 0" href="javascript:">Bronze</a><h4>UCP409-00</h4></li>
<li><a class="color1" style="cursor:default;background:rgba(0,0,0,0) url(http://treasuregarden.com/images/sysicons/Finish_Auto_Black.jpg) no-repeat scroll 0 0" href="javascript:">Black</a><h4>UCP409-09</h4></li>
</ul>
<div class="clear"></div>
<div class="button1">
<ul>
<li><a href="#">DESIGN STUDIO</a></li>
<li><a class="click1">DIMENSIONS</a></li>
</ul>
<div class="clear"></div>
</div>
    <br>
    <div class="rate_widget">
        <div class="star_1 ratings_stars"></div>
        <div class="star_2 ratings_stars"></div>
        <div class="star_3 ratings_stars"></div>
        <div class="star_4 ratings_stars"></div>
        <div class="star_5 ratings_stars"></div>
    </div>
</div>

<div class="rectangle_img"><img src="<?php echo BASE_URL();?>assets/images/products/UCP409_17_main.png" width="700" alt="img"></div>
<div class="clear"></div>
</div>
<div class="domentions">
<div class="pro_hover">
<div class="">
<div class="pro_hover_img">
<img src="<?php echo BASE_URL();?>assets/images/products/UCP409_dimens_17.png" width="1155" alt="img">
<a class="click2">BACK</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tilling_section">
<div class="container">
<div class="tilling_img">
<img alt="img" height="" src="<?php echo BASE_URL();?>assets/images/UCP_blk_hub.png" width=""></div>
<div class="tilling_txt">
<img alt="img" height="80" src="<?php echo BASE_URL();?>assets/images/icon_commpopup.png" width="80">
<div class="clear">
&nbsp;</div>
<h2>
POP-UP FEATURE</h2>
<p>
Umbrella slides up and pops into place.</p>
</div>
<div class="clear">
&nbsp;</div>
</div>
</div>
<div class="base_section">
<div class="container">
<div class="base_img base_img_ext">
<img alt="img" height="" src="<?php echo BASE_URL();?>assets/images/UCP_blk_pole_785x550.png" width=""></div>
<div class="base_txt base_txt_ext">
<img alt="img" height="81" src="<?php echo BASE_URL();?>assets/images/icon_commgrade.png" width="80">
<div class="clear">
&nbsp;</div>
<h2>
COMMERCIAL GRADE</h2>
<p>
1.5 mm thick aluminum pole is<br>
built for commercial use.</p>
</div>
<div class="clear">
&nbsp;</div>
</div>
</div>
<script>$(document).ready(function(){$(".fancybox-media").attr("rel","media-gallery").fancybox({openEffect:"none",closeEffect:"none",prevEffect:"none",nextEffect:"none",arrows:false,helpers:{media:{},buttons:{}}});$(".fancybox-media-name").click(function(){$(this).parent().find(".fancybox-media").trigger("click")})});</script>
<div class="related_section">
<div class="container">
<h3>Related Accessories</h3>
<div class="related_slide">
<ul class="r_slider">
<li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png" width="260" height="204" alt="img"></span>
CP902 </a>
</li> <li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png" width="260" height="204" alt="img"></span>
Steel </a>
</li> <li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png" width="260" height="204" alt="img"></span>
Steel Base w/casters </a>
</li> <li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png" width="260" height="204" alt="img"></span>
Commercial </a>
</li> <li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png" width="260" height="204" alt="img"></span>
LUNA </a>
</li> <li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png" width="260" height="204" alt="img"></span>
Vega-L </a>
</li> <li>
<a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png" width="260" height="204" alt="img"></span>
Monaco </a>
</li> </ul>
<div class="clear"></div>
</div>
</div>
</div>
<script>$(".r_slider").bxSlider({slideWidth:269,minSlides:4,maxSlides:5,moveSlides:1,slideMargin:2});</script>
<div class="videos_section">
<div class="container">
<h3>INSTRUCTIONAL VIDEOS</h3>
<div class="videos related_slide">
<ul class="video_slider" style="text-align:center">
<li style="float:none">
<div class="video1" style="background:rgba(0,0,0,0) url(http://treasuregarden.com/upload/pdf/detail_img10_min.png) no-repeat scroll 0 0">
<span>
<a style="background:none" class="fancybox-media" href="https://www.youtube.com/watch?v=-_W5J3pNzPo"><img src="<?php echo BASE_URL();?>assets/images/play_button.png" width="90" height="89" alt="play"></a>
</span>
</div>
<a class="fancybox-media-name" style="background:0;max-width:240px" href="javascript:">50lb. Market Base and Add-On Weight</a>
</li>
<li style="float:none">
<div class="video1" style="background:rgba(0,0,0,0) url(http://treasuregarden.com/upload/pdf/detail_img9_min.png) no-repeat scroll 0 0">
<span>
<a style="background:none" class="fancybox-media" href="https://www.youtube.com/watch?v=NNQlClb1K4k"><img src="<?php echo BASE_URL();?>assets/images/play_button.png" width="90" height="89" alt="play"></a>
</span>
</div>
<a class="fancybox-media-name" style="background:0;max-width:240px" href="javascript:">Vega Light for Center Pole Applications</a>
</li>
<li style="float:none">
<div class="video1" style="background:rgba(0,0,0,0) url(http://treasuregarden.com/upload/pdf/detail_img12_min.png) no-repeat scroll 0 0">
<span>
<a style="background:none" class="fancybox-media" href="https://www.youtube.com/watch?v=2mTgclhzeqw"><img src="<?php echo BASE_URL();?>assets/images/play_button.png" width="90" height="89" alt="play"></a>
</span>
</div>
<a class="fancybox-media-name" style="background:0;max-width:240px" href="javascript:">Protective Cover</a>
</li>
<li style="float:none">
<div class="video1" style="background:rgba(0,0,0,0) url("") no-repeat scroll 0 0">
<span>
<a style="background:none" class="fancybox-media" href="https://youtu.be/bBbgKRWoQlc?list=UUlM1iqxfBOAtF9rRwPLwdRA"><img src="<?php echo BASE_URL();?>assets/images/play_button.png" width="90" height="89" alt="play"></a>
</span>
</div>
<a class="fancybox-media-name" style="background:0;max-width:240px" href="javascript:">How to Replace Canopy</a>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="download_pdf">
<ul>
<li><a target="_blank" href="#">Download PDF Instruction Manual for UCP Series</a></li>
</ul>
<div class="clear"></div>
</div>
</div>
</div>
<script></script><div class="clear"></div><div id="id_n_parent_product_id" class="hide_div">109</div>
<div id="id_onlyonechild_id" class="hide_div"></div>
<div id="id_defaultchild_id" class="hide_div"></div>
<div id="id_n_product_type" class="hide_div">1</div></div></div> </div>
<div class="bottom">
<?php include "includes/footer.php"?>
    <script>
        $(function () {
           $('.rating').on("click", function () {
               debugger;
               $('.rating').css("color","black");
           }) ;
        });
    </script>