<html>
<head>
    <title> Architectural Gardens | How To Buy </title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/msdropdown/dd.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/msdropdown/jquery.dd.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/prettify.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/scrollpane.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>

</head>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpNL4Qf6p9IPEUzRttw0JLmn0QWM8Vq4I&callback=initMap"></script>
<div class="center">
<div class="mock" style="padding:0">
<div class="mock_in" id="sl_div">
<div class="mock_lft no_bg">
<div id="id_white_bg" class="" style="position:absolute;left:0;top:0;width:100%;height:200px;display:block;z-index:-1"></div>
<div class="mock_lft_main">
<div class="mock_lft_in">
<h2>Dealer Locator</h2>
<form action="" id="searchForm" onsubmit="cslmap.searchLocations();return false">
<div class="mock_form">
<ul>
<li>
<label class="mock_lable">Zip / Postal Code:</label>
<input class="textfield" id="addressInput" name="addressInput" type="text" value="" onclick="if(this.value==''){this.value=''}" onblur="if(this.value==''){this.value=''}">
</li>
<li class="pad_last">
<div class="select_content">
<label class="mock_lable">Radius</label>
<select id="radiusSelect">
<option value="1">1 miles</option>
<option value="5">5 miles</option>
<option value="10">10 miles</option>
<option selected="selected" value="15">15 miles</option>
<option value="25">25 miles</option>
<option value="50">50 miles</option>
</select>
</div>
</li>
<li class="searching">
<a id="btn_search" href="javascript:">SEARCH</a>
<a style="background-color:#505050;width:200px;margin-top:10px" href="#">INTERNATIONAL DEALERS</a>
</li>
</ul>
<div class="clear"></div>
</div>
</form>
</div>
</div>
</div>
<div class="mock_rgt">
<div id="map" style="min-height:850px">
</div>
</div>
<div class="clear"></div>
</div>
</div>
<script>$(document).ready(function(e){$(document).ready(function(e){$('select').msDropdown();});$(function(){$('.scroll-pane').jScrollPane({autoReinitialise:true});});});</script>
<script>/*<![CDATA[*/$("#addressInput").keypress(function(e){code=(e.keyCode?e.keyCode:e.which);if(code==13)
{$("#btn_search").trigger('click');}});var map;var rs_data;function loadResults(data)
{var items,markers_data=[];if(data.venues.length>0)
{items=data.venues;for(var i=0;i<items.length;i++)
{var item=items[i];if(item.location.lat!=undefined&&item.location.lng!=undefined)
{var icon='images/location.png';var index=i;var lat=item.location.lat;var lng=item.location.lng;var template=$('#edit_marker_template').text();markers_data.push({lat:lat,lng:lng,title:item.name,icon:{size:new google.maps.Size(32,32),url:icon},infoWindow:{content:content},click:function(e){map.setCenter(e.position.lat(),e.position.lng());}});}}}
map.addMarkers(markers_data);}
function printResults(data)
{$('#foursquare-results').text(JSON.stringify(data));prettyPrint();}
$(document).on('click','.pan-to-marker h2, .pan-to-marker p',function(e)
{e.preventDefault();var position,lat,lng,index;index=$(this).parent().data('marker-index');position=map.markers[index].getPosition();lat=position.lat();lng=position.lng();map.setCenter(lat,lng);google.maps.event.trigger(map.markers[index],'click');});var default_lat=search_lat=28;var default_lng=search_lng=-100;$(document).ready(function()
{$(".back_top").hide();prettyPrint();initMap();});function initMap(){map=new GMaps({div:'#map',lat:search_lat,lng:search_lng,zoom:4,mapTypeControl:true,zoomControl:true,zoomControlOptions:{}});}
$('#btn_search').click(function(){g_js.windows.mask.busy.show();var address=$('#addressInput').val();var radus=$('#radiusSelect').val();if(address!=''){GMaps.geocode({address:address,callback:function(results,status)
{if(status=='OK')
{var latlng=results[0].geometry.location;search_lat=latlng.lat();search_lng=latlng.lng();doSearch();}else{g_js.windows.mask.busy.close();return false;}}});}else{g_js.windows.mask.busy.close();return false;}});function doSearch(){var address=$('#addressInput').val();var radus=$('#radiusSelect').val();g_js.js.ajax.fg.get(16,1,'&address='+address+'&radus='+radus+'&lat='+search_lat+'&lng='+search_lng,function(v){$('#map_sidebar ul').html('');map.removeMarkers();$('.mock_lft').removeClass('no_bg');var data=g_js.json.get(v);rs_data=data;if(data.venues.length>0){loadResults(data);}else{$('#map_sidebar ul').html('<li style="font-size:18px;">No results found.</li>');}
g_js.windows.mask.busy.close();});}/*]]>*/</script>
</div>
<div class="bottom">
<?php include "includes/footer.php";?>