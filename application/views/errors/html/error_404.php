<?php $base_url = load_class('Config')->config['base_url']; ?>
<html lang="en" class="error-404">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title><?php echo SITE_NAME;?></title>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="wrap">
    <div class="page-body">
        <div class="row animated bounce ">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel mt-xlg">
                    <div class="w3-container">
                        <div class="w3-panel w3-card-4">
                            <div class="panel-content">
                                <h1 class="error-number" style="font-size: 95px;">404</h1>
                                <h2 class="error-name"> Page not found</h2>
                                <p class="error-text">Sorry, the page you are looking for cannot be found.
                                    <br/>Please check the url for errors or try one of the options below</p>
                                <div class="row mt-xlg">
                                    <div class="col-sm-6  col-sm-offset-3">
                                        <button class="btn btn-sm btn-darker-2 btn-block" onclick="history.back();">Previous page</button>
                                        <a href="<?php echo $base_url;?>home" class="btn btn-sm btn-primary btn-block">Home</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

