<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SITE_NAME;?> | Login</title>

    <link href="<?php echo BASE_URL();?>admin-assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL();?>admin-assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo BASE_URL();?>admin-assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo BASE_URL();?>admin-assets/css/style.css" rel="stylesheet">

</head>

<body style="background-color: white!important;">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name" style="margin-left: -40px;"><img src="<?php echo BASE_URL();?>assets/images/logo.png"</h1>

            </div>
            <h3>Welcome to Architectural Gardens</h3>

            <form class="m-t" method="post" role="form" action="<?php echo BASE_URL();?>auth">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" name="username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="<?php echo BASE_URL();?>forgot-password"><small>Forgot password?</small></a>

            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo BASE_URL();?>admin-assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/js/bootstrap.min.js"></script>

</body>

</html>
