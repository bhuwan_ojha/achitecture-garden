
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Forgot password</title>

    <link href="<?php echo BASE_URL();?>admin-assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL();?>admin-assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo BASE_URL();?>admin-assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo BASE_URL();?>admin-assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <?php echo failedflash();?>
            <div class="ibox-content">
                <h2 class="font-bold">Forgot password</h2>

                <p>
                    Enter your email address and your password will be reset and emailed to you.
                </p>

                <div class="row">

                    <div class="col-lg-12">
                        <form class="m-t" role="form" method="post" action="<?php BASE_URL();?>check-user">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email address" required="">
                            </div>

                            <button type="submit" class="btn btn-primary block full-width m-b">Send new password</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright <?php echo SITE_NAME;?>
        </div>
        <div class="col-md-6 text-right">
            <small>© 2014-2015</small>
        </div>
    </div>
</div>

</body>

</html>
