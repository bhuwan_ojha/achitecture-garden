<?php include "includes/header.php"; ?>
<body>
<style>
    .test {
        margin: auto;
        width: 500px;
        padding: 10px;
    }

    .table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

    h1 {
        font-family: arial, sans-serif;
        color: blue;
    }
</style>
<div id="wrapper">
    <?php include "includes/side-nav.php"; ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <?php include "includes/top-nav.php"; ?>
        </div>
        <div class="ibox-content">
            <table id="mytable" class="table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Zip</th>
                    <th>Email</th>
                    <th>Home</th>
                    <th>Cell</th>
                    <th>Work</th>
                    <th>Contact</th>
                    <th>Job Address</th>
                    <th>Job Contact</th>
                    <th>Job Phone</th>
                    <th>Discount</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $value): ?>
                    <tr>
                        <td id="id"><?php echo $value['id']; ?></td>
                        <td contenteditable="false"><?php echo $value['first_name']; ?></td>
                        <td contenteditable="false"><?php echo $value['last_name']; ?></td>
                        <td contenteditable="false"><?php echo $value['address']; ?></td>
                        <td contenteditable="false"><?php echo $value['city']; ?></td>
                        <td contenteditable="false"><?php echo $value['state']; ?></td>
                        <td contenteditable="false"><?php echo $value['zip']; ?></td>
                        <td contenteditable="false"><?php echo $value['email']; ?></td>
                        <td contenteditable="false"><?php echo $value['home']; ?></td>
                        <td contenteditable="false"><?php echo $value['cell']; ?></td>
                        <td contenteditable="false"><?php echo $value['work']; ?></td>
                        <td contenteditable="false"><?php echo $value['contact']; ?></td>
                        <td contenteditable="false"><?php echo $value['job_address']; ?></td>
                        <td contenteditable="false"><?php echo $value['job_contact']; ?></td>
                        <td contenteditable="false"><?php echo $value['job_phone']; ?></td>
                        <td contenteditable="false"><?php echo $value['discount']; ?></td>
                        <td id="action">
                            <button class="btn btn-success btn-xs" onclick="update(this)" style="display: none"
                                    id="savebtn"><i class="glyphicon glyphicon-check"></i></button>
                            <button class="btn btn-info btn-xs" onclick="contentEdit(this,true)" id="editbtn"><i
                                    class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-xs" onclick="deleteThis(this)"
                                    value="<?php echo $value['id']; ?>" id="delbtn"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="17" style="text-align: right;">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-plus"></i></button>
                    </td>
                    </td>
                </tr>
                </tbody>
            </table>
            <img src="http://sampsonresume.com/labs/pIkfp.gif" class="loading" style="display:none">
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <form action="<?php echo BASE_URL(); ?>Customer/Add" method="post">
                        <div class="modal-body">

                            <div class="col-md-6 form-group">
                                <label for="usr">First Name:</label>
                                <input type="text" class="form-control" name="first_name" id="first_name">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Last Name:</label>
                                <input type="text" class="form-control" name="last_name" id="last_name">
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="usr">Address:</label>
                                <input type="text" class="form-control" name="address" id="address">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">City:</label>
                                <input type="text" class="form-control" name="city" id="city">
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="usr">State:</label>
                                <input type="text" class="form-control" name="state" id="state">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Zip:</label>
                                <input type="text" class="form-control" name="zip" id="zip">
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="usr">Email:</label>
                                <input type="text" class="form-control" name="email" id="email">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Home:</label>
                                <input type="text" class="form-control" name="home" id="home">
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="usr">Cell:</label>
                                <input type="text" class="form-control" name="cell" id="cell">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Work:</label>
                                <input type="text" class="form-control" name="work" id="work">
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="usr">Contact:</label>
                                <input type="text" class="form-control" name="contact" id="contact">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Job Address:</label>
                                <input type="text" class="form-control" name="job_address" id="job_address">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Job Contact:</label>
                                <input type="text" class="form-control" name="job_contact" id="job_contact">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="usr">Job Phone:</label>
                                <input type="text" class="form-control" name="job_phone" id="job_phone">
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="usr">Discount:</label>
                                <input type="text" class="form-control" name="discount" id="discount">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


</div>

</div>
</div>
</body>
<?php include "includes/footer.php"; ?>
<script>
    function deleteThis(thisObj) {
        var id = $(thisObj).val();
        $.ajax({
            type: 'POST',
            url: BASE_URL + 'Customer/Delete',
            data: {id: id},
            beforeSend: function () {
                $(".loading").show();
            },
            success: function () {
                $(thisObj).parent().parent().remove();
                //location.reload();
                $(".loading").fadeOut("slow");
            }
        });
    }


    function contentEdit(thisObj, isContentEditable) {
        var currentTD = $(thisObj).parents('tr').find('td');

        currentTD = $(thisObj).parents('tr').find('td');
        $.each(currentTD, function () {
            $(this).prop('contenteditable', isContentEditable);
            if(isContentEditable){
                $(this).find('#editbtn').css("display", "none");
                $(this).find('#savebtn').css("display", "");
            }else{
                $(this).find('#editbtn').css("display", "");
                $(this).find('#savebtn').css("display", "none");
            }
            $(thisObj).parents('tr').find("td[id='id']").attr('contenteditable', false);
            $(thisObj).parents('tr').find("td[id='action']").attr('contenteditable', false);
        });

    }


    function update(thisObj) {

        var thisTd = $('table tr td');
        var id = thisTd.eq(0).text();
        var first_name = thisTd.eq(1).text();
        var last_name = thisTd.eq(2).text();
        var address = thisTd.eq(3).text();
        var city = thisTd.eq(4).text();
        var state = thisTd.eq(5).text();
        var zip = thisTd.eq(6).text();
        var email = thisTd.eq(7).text();
        var home = thisTd.eq(8).text();
        var cell = thisTd.eq(9).text();
        var work = thisTd.eq(10).text();
        var contact = thisTd.eq(11).text();
        var job_address = thisTd.eq(12).text();
        var job_contact = thisTd.eq(13).text();
        var job_phone = thisTd.eq(14).text();
        var discount = thisTd.eq(15).text();

        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL()?>admin/Customer/Update",
            data: {
                id: id,
                first_name: first_name,
                last_name: last_name,
                address: address,
                city: city,
                state: state,
                zip: zip,
                email: email,
                home: home,
                cell: cell,
                work: work,
                contact: contact,
                job_address: job_address,
                job_contact: job_contact,
                job_phone: job_phone,
                discount: discount
            },
            success: function (data) {
                var editBtn = $('#editbtn');
                editBtn.css("display", "");
                $('#savebtn').css("display", "none");
                //location.reload();

                contentEdit(editBtn, false);
            }
        });
    }
</script>


