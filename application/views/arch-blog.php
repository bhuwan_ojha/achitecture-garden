<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/blog/base.min.css"/>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/blog/fancy.min.css"/>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/blog/main.css"/>
    <script src="<?php echo BASE_URL();?>assets/blog/compatibility.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/blog/theViewer.min.js"></script>
    <script>
        try{
            theViewer.defaultViewer = new theViewer.Viewer({});
        }catch(e){}
    </script>
    <title></title>
</head>
<body>
<div id="sidebar">
    <div id="outline">
    </div>
</div>
<div id="page-container">
    <div id="pf1" class="pf w0 h0" data-page-no="1">
        <div class="pc pc1 w0 h0">
            <img class="bi x0 y0 w1 h1" alt="" src="<?php echo BASE_URL();?>assets/blog/bg1.png"/>
            <div class="t m0 x1 h2 y1 ff1 fs0 fc0 sc0 ls0 ws0">Professional Designers • Architects • Landscape Contractors</div>
            <div class="t m0 x2 h3 y2 ff1 fs1 fc1 sc0 ls0 ws0">#archgardens</div>
            <div class="t m0 x3 h4 y3 ff2 fs2 fc2 sc0 ls0 ws0">CATEGORIES</div>
            <div class="t m0 x3 h5 y4 ff1 fs3 fc1 sc0 ls0 ws0">Plants &amp; Greens</div>
            <div class="t m0 x3 h5 y5 ff1 fs3 fc1 sc0 ls0 ws0">Brick &amp; Bluestone</div>
            <div class="t m0 x3 h5 y6 ff1 fs3 fc1 sc0 ls0 ws0">Pavilions &amp; Overhead</div>
            <div class="t m0 x3 h5 y7 ff1 fs3 fc1 sc0 ls0 ws0">Modern &amp; Cool</div>
            <div class="t m0 x3 h5 y8 ff1 fs3 fc1 sc0 ls0 ws0">Cooking &amp; Fire</div>
            <div class="t m0 x3 h5 y9 ff1 fs3 fc1 sc0 ls0 ws0">Fine Carpentry</div>
            <div class="t m0 x3 h5 ya ff1 fs3 fc1 sc0 ls0 ws0">Hearth &amp; Home</div>
            <div class="t m0 x3 h5 yb ff1 fs3 fc1 sc0 ls0 ws0">Patio &amp; Fabric</div>
            <div class="t m0 x3 h5 yc ff1 fs3 fc1 sc0 ls0 ws0">Illumination</div>
            <div class="t m0 x3 h5 yd ff1 fs3 fc1 sc0 ls0 ws0">Stone &amp; Masonry</div>
            <div class="t m0 x3 h5 ye ff1 fs3 fc1 sc0 ls0 ws0">Elusive Path</div>
            <div class="t m0 x3 h5 yf ff1 fs3 fc1 sc0 ls0 ws0">Uncycled &amp; Sheek</div>
            <div class="t m0 x3 h5 y10 ff1 fs3 fc1 sc0 ls0 ws0">Picnics &amp; Parks</div>
            <div class="t m0 x3 h5 y11 ff1 fs3 fc1 sc0 ls0 ws0">Gardens &amp; Picnics</div>
            <div class="t m0 x3 h5 y12 ff1 fs3 fc1 sc0 ls0 ws0">Water &amp; Koi</div>
            <div class="t m0 x3 h5 y13 ff1 fs3 fc1 sc0 ls0 ws0">Flower Color Palette</div>
            <div class="t m0 x3 h5 y14 ff1 fs3 fc1 sc0 ls0 ws0">Drought Sensitive</div>
            <div class="t m0 x3 h6 y15 ff2 fs4 fc2 sc0 ls0 ws0">Archives</div>
            <div class="t m0 x3 h5 y16 ff1 fs3 fc1 sc0 ls0 ws0">Subscribe</div>
            <div class="t m0 x4 h7 y17 ff3 fs5 fc1 sc0 ls0 ws0">Old Fashioned Flowers</div>
            <div class="t m0 x5 h8 y18 ff3 fs6 fc1 sc0 ls0 ws0">She loves me She Loves Me Not</div>
            <div class="t m0 x6 h9 y19 ff4 fs7 fc2 sc0 ls0 ws0">Be Inspired</div>
            <div class="t m0 x7 ha y1a ff3 fs8 fc1 sc0 ls0 ws0">GARDEN </div>
            <div class="t m0 x7 hb y1b ff5 fs9 fc1 sc0 ls0 ws1">      </div>
            <div class="t m0 x7 hc y1c ff6 fsa fc2 sc0 ls0 ws2">    <span class="_ _8"></span><span class="ff4 ws0">Vines</span>
            </div>
            <div class="t m0 x8 hd y1d ff7 fsb fc0 sc0 ls0 ws0">&amp;</div>
            <div class="t m0 x9 h3 y1e ff1 fs1 fc1 sc0 ls0 ws0">Margaret Morrissette</div>
            <div class="t m0 x9 h3 y1f ff1 fs1 fc1 sc0 ls0 ws0">736 N. Western Ave. #109 </div>
            <div class="t m0 x9 h3 y20 ff1 fs1 fc1 sc0 ls0 ws0">Lake Forest, IL 60045</div>
            <div class="t m0 x9 h3 y21 ff1 fs1 fc1 sc0 ls0 ws0">847.691.7345</div>
            <div class="t m0 x9 h3 y22 ff1 fs1 fc1 sc0 ls0 ws0">architecturalgardendesign.com
            </div>
        </div>
        <div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'>

        </div>
    </div>
</div>
<div class="loading-indicator">

</div>
</body>
</html>
