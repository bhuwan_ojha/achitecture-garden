<html>
<head>
    <title> Architectural Gardens | Product Search</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
    <div class="center collection">
<div class="left"><div class="left_top_bg"></div><div class="left_center_bg"></div><div class="left_bottom_bg"></div></div><div class="center"><div class="center_content inner"><div id="products_list_content"><div class='page_nav_line'></div><div class="product_list_grid">
<div class="container">
    <div class="product_nav" style="background:none;margin-bottom:5px;">
        <div id="top_breadcrumb"><span><div><span itemtype="" itemscope=""><a itemprop="url" class="headerNavigation" href="default.php"><span itemprop="title">Home</span></a></span> &gt; <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope=""><span itemprop="title" style="color:#000;text-transform:uppercase;">Search Result</span></span></div></span></div>                      <div class="clear"></div>
    </div>
</div>

<div style="overflow:auto;position:relative; height:60px;line-height:60px;border-top:1px solid #ededed;border-bottom:1px solid #ededed;">
    <div class="container subcategory" style="">
        <a href="#" style="margin-right:45px;">
            <span style="text-transform:uppercase;">Cantilevers</span>
        </a>
        <a href="#" style="margin-right:45px;">
            <span style="text-transform:uppercase;">SPECIALTY</span>
        </a>
        <a href="#" style="margin-right:45px;">
            <span style="text-transform:uppercase;">MARKET</span>
        </a>
        <a href="#" style="margin-right:45px;">
            <span style="text-transform:uppercase;">COMMERCIAL</span>
        </a>
        <a href="#" style="margin-right:45px;">
            <span style="text-transform:uppercase;">ACCESSORIES</span>
        </a>
        <a href="#" style="margin-right:45px;">
            <span style="text-transform:uppercase;">Fabrics</span>
        </a>
    </div>
</div>
<div class="product">
<div class="container">

<div class="product_in">


<ul class="plist " style="width:100%;">
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP920_16_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP920_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP920_16_thumb_min.png'" ></span>
        <small>CP920</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP908_16_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP908_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP908_16_thumb_min.png'" ></span>
        <small>CP908</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP912_16_thumbb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP912_16_thumbb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP912_16_thumbb_min.png'" ></span>
        <small>CP912</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP910_16_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP910_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP910_16_thumb_min.png'" ></span>
        <small>CP910</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP902_16_thumb_min.png'" ></span>
        <small>CP902</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/CP901_16_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/CP901_16_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/CP901_16_thumb_min.png'" ></span>
        <small>CP901</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/VegaL_thumb_min.png'" ></span>
        <small>Vega-L</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Luna_16_thumb.png'" ></span>
        <small>LUNA</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BS709_thumb_min.png'" ></span>
        <small>Steel</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BS359_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BS359_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BS359_thumb_min.png'" ></span>
        <small>Steel</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BW50_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BW50_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BW50_thumb_min.png'" ></span>
        <small>Classic</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BG50_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BG50_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BG50_thumb_min.png'" ></span>
        <small>Garden</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BA50_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BA50_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BA50_thumb_min.png'" ></span>
        <small>Art Deco</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BKMSQ100_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BKMSQ100_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BKMSQ100_thumb_min.png'" ></span>
        <small>Monaco</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BKM100_thumb_min.png'" ></span>
        <small>Monaco</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BSK120_thumb_min.png'" ></span>
        <small>Steel Base w/casters</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/BA150_thumb_min.png'" ></span>
        <small>Commercial</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UCP407SQ_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407SQ_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407SQ_17_thumb.png'" ></span>
        <small>Commercial</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UCP407_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407_17_thumb.png'" ></span>
        <small>Commercial</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UCP409_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UCP409_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UCP409_17_thumb.png'" ></span>
        <small>Commercial</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM8091_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM8091_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM8091_17_thumb.png'" ></span>
        <small>Quad Pulley Lift</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UMTK_0810_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UMTK_0810_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UMTK_0810_17_thumb.png'" ></span>
        <small>Teak Crank Lift</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM906CK_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM906CK_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM906CK_17_thumb.png'" ></span>
        <small>Push Button Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM907_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM907_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM907_17_thumb.png'" ></span>
        <small>Push Button Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM920_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM920_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM920_17_thumb.png'" ></span>
        <small>Push Button Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM8811RT_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM8811RT_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM8811RT_17_thumb.png'" ></span>
        <small>Crank Lift</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM8810RT_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM8810RT_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM8810RT_17_thumb.png'" ></span>
        <small>Auto Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM810_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM810_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM810_17_thumb.png'" ></span>
        <small>Auto Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM812_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM812_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM812_17_thumb.png'" ></span>
        <small>Auto Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM800_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM800_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM800_17_thumb.png'" ></span>
        <small>Collar Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM801_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM801_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM801_17_thumb.png'" ></span>
        <small>Collar Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM8009SL_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM8009SL_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM8009SL_17_thumb.png'" ></span>
        <small>Starlight Collar Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/UM8010SL_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UM8010SL_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UM8010SL_17_thumb.png'" ></span>
        <small>Starlight Collar Tilt</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Veranda_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Veranda_close_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Veranda_17_thumb.png'" ></span>
        <small>Veranda</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/USA459_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/USA459_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/USA459_17_thumb.png'" ></span>
        <small>Shanghai</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/Lotus_17_thumb.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/Lotus_tilt_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/Lotus_17_thumb.png'" ></span>
        <small>Lotus</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AG19_thumb_17.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AG19_tilt_thumb_17.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AG19_thumb_17.png'" ></span>
        <small>AG19 Cantilever</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AG28_thumb_17.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AG28_tilt_thumb_17.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AG28_thumb_17.png'" ></span>
        <small>AG28 Cantilever</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AKZSQ_thumb_17.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AKZSQ_tilt_thumb_17.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AKZSQ_thumb_17.png'" ></span>
        <small>AKZSQ Cantilever</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AKZ_thumb_17.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AKZ_tilt_thumb_17.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AKZ_thumb_17.png'" ></span>
        <small>AKZ Cantilever</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AKZRT_thumb_17.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AKZRT_tilt_thumb_17.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AKZRT_thumb_17.png'" ></span>
        <small>AKZRT Cantilever</small><em> <br></em>
    </a>
</li>
<li>
    <a href="#"><span><img src="<?php echo BASE_URL();?>assets/images/products/AKZ13_thumb_17.png" width="260" height="204"  onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/AKZ13_tilt_thumb_17.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/AKZ13_thumb_17.png'" ></span>
        <small>AKZ13 Cantilever</small><em> <br></em>
    </a>
</li>
</ul>

<div class="clear"></div>
</div>
</div>
</div>
</div><div class='page_nav_line'></div></div></div></div><div class="right"></div></div>
<div class="bottom">
<?php include "includes/footer.php";?>