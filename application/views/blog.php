<html>
<head>
    <title> Architectural Gardens | Blog</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include 'includes/nav.php';?>
<div id="homecenter">
<div id="contentleft">
<div class="postarea">
<div class="press">
<div class="container">
<div class="press_in">
<h2>Upcoming events</h2>
<div class="upcomming">
<div class="press_nav">
<ul>
<li>
<p>January 22 - 26, 2017</p>
</li>
<li>
<p>Las Vegas Winter Market</p>
</li>
<li class="pad_last">
<p>C1215 World Market, Las Vegas NV</p>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="press_nav pre_nav">
<ul>
<li>
<p>July 11 - 13, 2017</p>
</li>
<li>
<p>ICFA Pre-Market</p>
</li>
<li class="pad_last">
<p>1655 Merchandise Mart, Chicago IL</p>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="press_nav">
<ul>
<li>
<p>July 30 - August 3, 2017</p>
</li>
<li>
<p>Las Vegas Summer Market</p>
</li>
<li class="pad_last">
<p>C1215 World Market, Las Vegas NV</p>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="press_nav pre_nav">
<ul>
<li>
<p>September 12 - 15, 2017</p>
</li>
<li>
<p>ICFA Market</p>
</li>
<li class="pad_last">
<p>1655 Merchandise Mart, Chicago IL</p>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
<div class="event_news">
<div>
<div class="event_news_in block2">
<div class="event_rgt">
<div class="tbl">
<div class="tbl_cell">
<div class="paper1">
&nbsp;
</div>
<h3>
2016 Event News
</h3>
<p>
For the seventh year in a row, Treasure Garden has been recognized by the ICFA retail membership with the Manufacturer Leadership Award in the Shade Category. Presented during the 2016 Casual Market Chicago, the award recognized the products, people and production efforts of Treasure Garden for overall manufacturing and customer service excellence. From left to right, Gary McCray, ICFA Executive Chairman and President at Klaussner Outdoor, Margaret Chang, President at Treasure Garden and Oliver Ma, CEO at Treasure Garden.
</p>
</div>
</div>
</div>
<div class="event_lft paper2" id="event_paper1">
<img alt="event_img1" height="225" src="<?php echo BASE_URL();?>assets/images/event_2016_Award_group.jpg" width="400">
</div>
</div>
</div>
<div>
<div class="event_news_in block1">
<div class="event_lft">
<img alt="event_img1" height="225" src="<?php echo BASE_URL();?>assets/images/event_2015_Award_group.jpg" width="400">
</div>
<div class="event_rgt">
<div class="tbl">
<div class="tbl_cell">
<h3>
2015 Event News
</h3>
<p>
For the sixth year in a row, Treasure Garden was honored during the International Casual Furniture and Accessories Market as the recipient of the 2015 Manufacturer Leadership Award in the Shade Category. In acknowledging Treasure Garden, the ICFA recognized the company&#39;s quality of manufactured shade products, design and customer service excellence. This is Treasure Garden&#39;s <span data-dobid="hdw">twelfth </span>time overall for manufacturing excellence. From left to right, Jeff B. Dorough, VP of Sales and Marketing at Treasure Garden, Margaret Chang, President at Treasure Garden, Gary McCray, ICFA Executive Chairman and President at Klaussner Outdoor and Ben Ma, Executive Associate at Treasure Garden.
</p>
</div>
</div>
</div>
</div>
<div>
&nbsp;
</div>
</div>
<div>
<div class="event_news_in block2">
<div class="event_rgt">
<div class="tbl">
<div class="tbl_cell">
<div class="paper1">
&nbsp;
</div>
<h3>
2014 Event News
</h3>
<p>
Treasure Garden announces that it was honored during the recent International Casual Furniture and Accessories Market as the recipient of the 2014 Manufacturer Leadership Award in the Shade Category. In acknowledging Treasure Garden, the ICFA recognized the company&#39;s quality of manufactured shade products, design and customer service excellence. This is Treasure Garden&#39;s fifth consecutive year and the eleventh time overall for manufacturing excellence. From left to right, Ward Usmar, ICFA Executive Chairman and Sr. VP of Sales at Tuuci, Jeff B. Dorough, VP of Sales and Marketing at Treasure Garden, Margaret Chang, President at Treasure Garden and Oliver Ma, CEO at Treasure Garden.
</p>
</div>
</div>
</div>
<div class="event_lft paper2" id="event_paper1">
<img alt="event_img1" height="225" src="<?php echo BASE_URL();?>assets/images/event_img1.png" width="400">
</div>
</div>
</div>
<div>
<div class="event_news_in block1">
<div class="event_lft">
<img alt="event_img2" height="225" src="<?php echo BASE_URL();?>assets/images/event_img2.png" width="400">
</div>
<div class="event_rgt">
<div class="tbl">
<div class="tbl_cell">
<h3>
2013 Event News
</h3>
<p>
Treasure Garden, award-winning manufacturer of outdoor umbrellas, shade and accessory products, announces that it was honored during the recent Casual Furniture Market as the recipient of the 2013 Manufacturer Leadership Award in the Shade category. Presented by the International Casual Furnishings Association (ICFA), this distinguished award from the outdoor industry, recognized Treasure Garden for the fourth consecutive year and the tenth time overall for manufacturing excellence. In acknowledging Treasure Garden with the 2013 Manufacturer Leadership Award, the ICFA recognized the company&rsquo;s quality of manufactured shade products, design, and customer service excellence.
</p>
</div>
</div>
</div>
</div>
</div>
<div>
<div class="event_news_in block2">
<div class="event_rgt">
<div class="tbl">
<div class="tbl_cell">
<div class="paper3">
&nbsp;
</div>
<h3>
2012 Event News
</h3>
<p class="eve">
Treasure Garden was awarded on September 21, 2012 with the 2012 Manufacturer Leadership Award in the shade category at International Casual Furnishings Association. From left to right: Joseph Logan, Executive Director at with International Casual Furnishings Association, Margaret Chang, COO at Treasure Garden, Jeff B. Dorough, VP of Sales and Marketing at Treasure Garden, Tiffany Tillman, Customer Service Manager at Treasure Garden, and Eric Parsons, President at Gloster Furniture. For the third consecutive year, and the eighth time overall has Treasure Garden been the recipient of this prestigious award recognizing the company&rsquo;s quality of manufactured goods, design, and exceptional customer service. &ldquo;This was all made possible by the support and relationships with our dealers,&rdquo; said Margaret Chang, Chief Operating Officer, Treasure Garden. &ldquo;Without them, this award could not have been possible and Treasure Garden expresses gratitude to all who helped contribute to this distinguished honor.&rdquo;
</p>
<p>
At the same market, Treasure Garden also received the 2012 Permanent Showroom of the Year Award on September 19, 2012 by the International Casual Furnishings Association. From left to right: Margaret Chang, COO, Jeff B. Dorough, VP of Sales and Marketing and Oliver Ma, President/CEO at Treasure Garden. This award recognized Treasure Garden&rsquo;s ongoing commitment of time and effort to the presentation of their 10,000 sq. ft. showroom within Chicago&rsquo;s Merchandise Mart facility. On display throughout the showroom were the company&rsquo;s latest product introductions as well as the latest in fabric trends and merchandising ideas. &ldquo;We are very happy and thrilled our showroom was recognized this year. We really strive to set the trends in shade for the industry,&rdquo; notes Dorough.
</p>
</div>
</div>
</div>
<div class="event_lft paper4" id="event_paper3">
<img alt="event_img3" height="225" src="<?php echo BASE_URL();?>assets/images/event_img3.png" width="400"> <img alt="event_img4" height="225" src="<?php echo BASE_URL();?>assets/images/event_img4.png" width="400">
</div>
</div>
</div>
<div>
<div class="event_news_in no_bd block1">
<div class="event_lft">
<img alt="event_img5" height="225" src="<?php echo BASE_URL();?>assets/images/event_img5.png" width="400"> <img alt="event_img6" height="225" src="<?php echo BASE_URL();?>assets/images/event_img6.png" width="400">
</div>
<div class="event_rgt">
<div class="tbl">
<div class="tbl_cell">
<h3>
2011 Event News
</h3>
<p class="eve">
Treasure Garden&#39;s Red Ribbon Ceremony was held on September 12, 2011 for their new expanded 1655 showroom in Chicago. Treasure Garden&#39;s showroom covers 10,000 square feet of product, fabrics and their contract/hospitality line, Shademaker. From left to right: John H. Brennen III, Executive Vice President at Merchandise Mart, Oliver Ma, President/CEO at Treasure Garden, Margaret Chang, COO at Treasure Garden and Whitney Gillespie, Director of Leasing and Sales at Merchandise Mart.
</p>
<p>
Treasure Garden was awarded on September 13, 2011 with the 2011 Manufacturer Leadership Award at International Casual Furnishings Association. From left to right: Joseph Logan, Executive Director at with International Casual Furnishings Association, Jeff B. Dorough, VP of Sales and Marketing at Treasure Garden, Margaret Chang, COO at Treasure Garden and Rory Rehmert, VP of Sales at Pride Family Brands.
</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="whats_new">
<h2>Advertising</h2>
<ul class="whatnew_content">
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_CL0916.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_CL0916.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>CASUAL LIVING</h4>
<p>Treasure Garden's NEW Lotus 10' Collar Tilt in 4815 Sunset was featured in Casual Living September 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_PHPR716-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_PHPR716-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Patio & Hearth Products Report</h4>
<p>Treasure Garden's 9' Collar Tilt in 58039 Gateway Mist Stripe was featured in Patio & Hearth Products Report July/August 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_CL0616_cover-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_CL0616_cover-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Causal Living</h4>
<p>Treasure Garden's 8'x11' Crank Lift and 7' UCP Commercial in 5404 Natural was featured on the front cover of Casual Living's June 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_PHPR516-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_PHPR516-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Patio & Hearth Products Report</h4>
<p>Treasure Garden's 8'x10' Auto Tilt in 4815 Sunset was featured in Patio & Hearth Products Report May/June 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_OceanHome_416-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_OceanHome_416-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Ocean Home</h4>
<p>Treasure Garden's 9' Starlight Collar Tilt in 5404 Natural was featured in Ocean Home April 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_Sunset_416-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_Sunset_416-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Sunset West</h4>
<p>Treasure Garden's 11' AG28 and 11' Collar Tilt in 5404 Natural was featured in Sunset West April 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_HH416-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_HH416-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Hearth & Home</h4>
<p>Treasure Garden's 8'x11' Crank Lift and 7' UCP Commercial was featured in Hearth and Home April 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_CL0616-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_CL0616-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Patio & Hearth Products Report</h4>
<p>Treasure Garden's 11' AG28 and 11' Starlight Collar Tilts in 5404 Natural was featured in Patio & Hearth Products Report March/April 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_BridgeForDesign_0315-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_BridgeForDesign_0315-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Bridge for Design</h4>
<p>Treasure Garden's 11' Octagon AKZ in 56095 Astoria Sunset Stripe was featured in Bridge for Design March 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_Domino-Spring16-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_Domino-Spring16-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Domino</h4>
<p>Treasure Garden's 9' Auto Tilt in 56080 Milano Cobalt Stripe was featured in Domino Spring 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_HH216-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_HH216-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Hearth & Home</h4>
<p>Treasure Garden's 11' AG28 in 5438 Buttercup was featured in Hearth and Home February 2016 issue.</p>
</li>
<li class="l_item">
<div class="dark_block">
<a class="fancy_item" href="<?php echo BASE_URL();?>assets/images/ad_Furniture-Today-012516-min.jpg"><img src="<?php echo BASE_URL();?>assets/images/ad_Furniture-Today-012516-min.jpg" width="250" height="322" alt="add1">
</a>
<div class="dark_hover"></div>
</div>
<h4>Furniture Today</h4>
<p>Treasure Garden's 9' Auto Tilt, DC7 Style in 6433 Neptune and 5404 Natural was featured in Furniture Today January 2016 issue.</p>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="furniture" style="display:none">
<h2>FURNITURE TODAY ADS</h2>
<ul class="furniture_content">
</ul>
<div class="clear"></div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<script></script>
<script>$(document).ready(function(){$(".fancy_item").attr("rel","media-gallery").fancybox({openEffect:"fade",closeEffect:"fade",openSpeed:1000,closeSpeed:500,arrows:false,helpers:{media:{},buttons:{}}});$(".dark_hover").click(function(){$(this).parent().find(".fancy_item").trigger("click")})});</script>
</div>
</div>
</div>
</div>
<div class="bottom">
<?php include 'includes/footer.php'; ?>