<html>
<head>
    <title> Architectural Gardens | Policy </title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include 'includes/nav.php';?>
<div id="homecenter">
<div id="contentleft">
<div class="postarea">
<div class="cs_i_center">
<div class=cs_i_pic></div>
<div class=cs_i_content>
<div class="cs_i_c_content">
<div class="container">
<div style="text-align:left">
<h3>
&nbsp;
</h3>
<div>
&nbsp;
</div>
<div>
<h3>
<strong>PRIVACY POLICY</strong>
</h3>
</div>
<div>
&nbsp;
</div>
<div>
<strong>Information Collection and Use </strong>
</div>
<div>
Treasure Garden is the sole owner of the information collected on this site. We will not sell, share, or rent this information to any outside parties, except as outlined in this policy. Treasure Garden collects information from our users at several different points on our website in order to process orders and better serve you with pertinent information. &nbsp;Information collected includes name, shipping address, telephone numbers, and e-mail address. Treasure Garden also requires you to submit a username and password of your choice for future access to your information. Your user name and password is to remain confidential and you should not share this information with anyone.
</div>
<div>
&nbsp;
</div>
<div>
<strong>Registration</strong>
</div>
<div>
This feature is for <strong><u>authorized dealers only</u></strong>. In order to process your orders using the express order form on this website, a user must first register with TeamTG. During registration a user is required to give their contact information, which includes name, e-mail, telephone number and physical address. This information is used to contact the user about the services on our site such as sending automated order status updates.
</div>
<div>
&nbsp;
</div>
<div>
<strong>Cookies</strong>
</div>
<div>
With respect to cookies, we use cookies to customize Web page content based on visitors browser type or other information that the visitor sends. If a user rejects the cookie, he/she may still use our site, however he/she may not be able to use certain features of the website. We do not share any information with any third party sites. Cookies are used strictly for the purpose of enhancing your personal experience with our site. When you close your browser, the cookies will be terminated.
</div>
<div>
&nbsp;
</div>
<div>
<strong>Log Files </strong>
</div>
<div>
We use IP addresses to analyze trends, administer the site, track user&rsquo;s movement, and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information. We do not distribute or share IP information with any third parties.
</div>
<div>
&nbsp;
</div>
<div>
<strong>Links</strong>
</div>
<div>
This web site contains links to other sites. Please be aware Treasure Garden is not responsible for the privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of each and every website that collects personally identifiable information. This privacy statement applies solely to information collected by this web site.
</div>
<div>
&nbsp;
</div>
<div>
<strong>Security </strong>
</div>
<div>
Treasure Garden website takes every precaution to protect our users information. When users submit sensitive information via the website, your information is protected both online and off-line. When our registration/order form asks users to enter sensitive information, that information is encrypted and is protected by SSL. Our SSL technology is the most advanced 128bit encryption by Authorize.net.
</div>
</div>
</div>
<div>
&nbsp;
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="bottom">
<?php include 'includes/footer.php';?>