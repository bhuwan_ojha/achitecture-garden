<div class="wrapper" style="border: 1px solid #cccccc;width:800px;margin:0 auto;padding:15px">
<div class="header" style="background-color:#001E47;padding:10px;height:30px;font-family:'Droid Serif',serif">
<div class="biz-name" style="float:left;width:48%;font-size:20px;text-transform:uppercase;color:#fff"><?= SITE_NAME ?></div>
<div class="phone" style="float:right;width:48%;color:#fff;font-size:20px;text-align:right">
Phone: <a href="tel:" style="color:#fff;font-size:20px;"><?= SITE_NUMBER ?></a>
</div>
</div>
<div class="clear" style="clear:both"></div>
<strong>Dear Admin,</strong><br>
<p>You've received a Contact query from a customer.</p>
<div class="table" style="margin:20px 0">
<table style="width:100%;border:1px solid #eeeeee;margin-bottom:20px">
<tr style="border:1px solid #eeeeee;color:#001E47;font-family:'Droid Serif',serif;font-size:18px">
<th colspan="2" style="text-align:left;padding:5px;border-bottom:1px solid #ccc">Message Details</th>
</tr>
<tr>
<td style="padding:5px;color:#FF9900;font-weight:bold">Name</td>
<td style="padding:5px"><?php echo $data['name']; ?></td>
</tr>
<tr>
<td style="padding:5px;color:#FF9900;font-weight:bold">Email</td>
<td style="padding:5px;"><?php echo $data['email']; ?></td>
</tr>
<tr>
<td style="padding:5px;color:#FF9900;font-weight:bold">Phone</td>
<td style="padding:5px"><?php echo $data['phone']; ?></td>
</tr>
<tr>
<td style="padding:5px;color:#FF9900;font-weight:bold">Company Name</td>
<td style="padding:5px"><?php echo $data['companyName']; ?></td>
</tr>
<tr>
<td style="padding:5px;color:#FF9900;font-weight:bold">Message</td>
<td style="padding:5px"><?php echo $data['comment']; ?></td>
</tr>
</table>
</div>

<div class="thank-you">
<b>Thank you!</b><br>
<h3><?php echo SITE_NAME ?></h3>
</div>
<div class="copyright" style="background-color:#001E47;padding:8px;color:#fff;text-align:center">
Copyright &copy; <?php echo date('Y');?>. <a href="<?php echo SITE_EMAIL ?>" style="color:#fff"><?php echo SITE_NAME ?></a>. All Right Reserved.
</div>
</div>