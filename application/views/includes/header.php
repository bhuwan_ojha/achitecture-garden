<html>
<head>
<title> Architectural Gardens | The World's Favorite Shade</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="keywords" content="Architectural Gardens">
<meta name="description" content="Architectural Gardens">
<base>
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/1.6/jquery-ui-1.8.13/css/smoothness/jquery-ui-1.8.13.custom.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/msdropdown/dd.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/ez-overlay/ez_style.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/scrollpane.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.css">
<script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
<script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/js_comment.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/js_website_language.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/1.6/jquery-ui-1.8.13/js/jquery-ui-1.8.13.custom.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jq.cookie/jquery.cookies.2.2.0.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/msdropdown/jquery.dd.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/ez-overlay/ez-overlay.min.js"></script>
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.css">
<script src="<?php echo BASE_URL();?>assets/js/bxslider/plugins/jquery.easing.1.3.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery.scrollTo.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/prettify.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/scrollpane.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/mousewheel.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/angular/angular.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/js_header.js"></script>
</head>