<div class="back_top"><a id="back_top" href="#top"><img src="<?php echo BASE_URL();?>assets/images/back_top.png" width="68" height="68"></a></div>
<footer>
<div class="message">
<p style="color:white">
Learn how we can help you with your project. Call us today at <a href="tel:847.691.7345" style="color:#669463">847.691.7345</a> or use our online form
</p>
</div>
<div class="container">
<div class="fotter_lft">
<div class="clear"></div>
</div>
<!--<div class="fotter_rit">
<ul>
<li><a href="<?php /*echo BASE_URL();*/?>home">Home</a></li>
<li><a href="<?php /*echo BASE_URL();*/?>service">Services</a></li>
<li><a href="<?php /*echo BASE_URL();*/?>gallery">Gallery</a></li>
<li><a href="<?php /*echo BASE_URL();*/?>us-and-you">Us & You!</a></li>
<li><a href="<?php /*echo BASE_URL();*/?>contact-us">Get a Meeting!</a></li>
</ul>
<div class="clear"></div>
</div>-->
<div class="clear"></div>
<!--<div style="text-align: center;">
<img style="margin-bottom: -135px;margin-top: -116px;" src="<?php /*echo BASE_URL();*/?>assets/images/ArchGardens.png">
</div>-->
<div class="fotter_nav">
<ul>
<li style="color:white">Our Community</li>
<li><a target="_blank" href="https://www.facebook.com/landscapingchicago">Find us on Facebook</a></li>
<li><a target="_blank" href="https://www.instagram.com/">Follow us on Instagram</a></li>
<li><a href="<?php echo BASE_URL();?>blog">Join our Blog</a></li>
</ul>
<ul>
<li style="color:white">Our Services</li>
<li><a href="#">Landscape Design & Installation</a></li>
<li><a href="#">Landscape Architect Drawings</a></li>
<li><a href="#">Landscape Maintenance</a></li>
<li><a href="#">Seasonal Rotation of Annuals</a></li>
</ul>
<ul class="lastul">
<li style="color:white">Our Location</li>
<li style="color:#669463">736 N. Western Ave. #109</li>
<li style="color:#669463">Lake Forest, IL 60045</li>
<li style="color:#669463"><a href="tel:">847.691.7345</a></li>
</ul>
</div>
<div style="color:white;text-align:center"> &copy; Architectural Gardens, Inc <?php echo DATE_TIME; ?></div>
</div>
</footer>
<a class="send_contactus"></a>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
<script>(function(){window.alert=function(a){$.jq_alert(a)}})();</script>
<script>
$("a[href='#top']").click(function() {
$("html, body").animate({ scrollTop: 0 }, "slow");
return false;
});
function subscription_success(obj,msg){
obj.addClass('field_success');
obj.parent().find(".success_msg").remove();
obj.after('<span class="success_msg">'+msg+'</span>');
$('.field_success:first').focus();
$('.success_msg').fadeOut(5000);
};
function check_sub_email() {
if ($("#sub_email").val() == "" || $("#sub_email").val() == "Enter Your Email Address") {
$.jq_field_error($('input[name="sub_email"]'), "Please enter a valid e-mail address");
return false
}else{
var email = $('#sub_email').val();
$.ajax({
url:'<?php echo BASE_URL();?>subscribe',
data:{email:email},
type:'post',
success:function(){
subscription_success($('input[name="sub_email"]'), "Thank you for subscribing to Architectural Garden’s Newsletter!");
}
})
}
}
</script>
</div>
</div>
<script>$(window).ready(function(){$(".home_zoom").each(function(a,b){$(this).html('<a href="'+$(this).attr("lhref")+'">'+$(this).html()+"</a>")})});</script>
</body>
</html>