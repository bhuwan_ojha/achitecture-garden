<html>
<head>
    <title> Architectural Gardens | Collection</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include 'includes/nav.php';?>
<div class="left">
<div class="left_top_bg"></div>
<div class="left_center_bg"></div>
<div class="left_bottom_bg"></div>
<div style="clear:both"></div>
</div>
<div>
<div class="center_content">
<div class="collection">
<div class="container">
<div class="collection_in">
<ul>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/category/icon_cantilever.png" width="350" height="350">
<span><small class=""><em>Cantilevers</em></small></span>
<span><small class="dark_hover"><em>Cantilevers</em></small></span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/category/icon_specialty17.jpg" width="350" height="350">
<span><small class=""><em>SPECIALTY</em></small></span>
<span><small class="dark_hover"><em>SPECIALTY</em></small></span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/category/icon_market17.jpg" width="350" height="350">
<span><small class=""><em>MARKET</em></small></span>
<span><small class="dark_hover"><em>MARKET</em></small></span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/category/icon_comm.png" width="350" height="350">
<span><small class=""><em>COMMERCIAL</em></small></span>
<span><small class="dark_hover"><em>COMMERCIAL</em></small></span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/category/Accessories_Auburn_icon.png" width="350" height="350">
<span><small class=""><em>ACCESSORIES</em></small></span>
<span><small class="dark_hover"><em>ACCESSORIES</em></small></span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/category/icon_fabric17.jpg" width="350" height="350">
<span><small class=""><em>Fabrics</em></small></span>
<span><small class="dark_hover"><em>Fabrics</em></small></span>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="right"></div>
</div>
<div class="bottom">
<?php include 'includes/footer.php';?>