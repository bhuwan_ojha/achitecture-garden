<html>
<head>
<title>Architectural Gardens | Gallery </title>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
<meta name="keywords" content="Gallery">
<meta name="description" content="Gallery">
<base>
<link rel="stylesheet" href="<?php echo BASE_URL(); ?>assets/css/stylesheet.css">
<script>
/*<![CDATA[*/var g_js_web_root_dir="<?php echo BASE_URL();?>";var g_js_web_language="en";var g_js_string_spliter="________________________";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;function js_get_themes_root(){return g_js_web_root_dir+"assets/";}/*]]>*/
</script>
<script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
<script src='<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js'></script>
<script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js" ></script>
<script src="<?php echo BASE_URL();?>assets/js/image-scale.js"></script>
<script>
$(document).ready(function(e){$(".about_hover a").click(function(event){if($(this).hasClass('link')){return;}
if(!$(this).parent().parent().parent().parent().parent().hasClass('active')){$('.explore').hide();$('.explore').parent().removeClass('active');}
$(this).parent().parent().parent().parent().parent().find(".explore").slideToggle();$(this).parent().parent().parent().parent().parent().toggleClass('active');var exp=$(this);});});$(window).resize(function(){$(function(){$("img.scale").imageScale();});});$(window).load(function(){$(function(){$("img.scale").imageScale();});});
</script>
<script src="<?php echo BASE_URL();?>assets/js/custom.js"></script>
<script>
document.createElement("header");document.createElement("nav");document.createElement("section");document.createElement("article");document.createElement("aside");document.createElement("footer");
</script>
</head>
<script>
function start_scroll(){var item=$("#tinyscrollbar");item.jscroll({W:"10px",BgUrl:"",Bg:"#F0752F",Bar:{Pos:"up",Bd:{Out:"#F0752F",Hover:"#F0752F"},Bg:{Out:"#F0752F",Hover:"#F0752F",Focus:"#F0752F"}},Btn:{btn:false,uBg:{Out:"#F0752F",Hover:"#F0752F",Focus:"#F0752F"},dBg:{Out:"#F0752F",Hover:"#F0752F",Focus:"#F0752F"}},Fn:function(){}});}</script>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
<div>
<div id="homecenter">
<div id="contentleft">
<div class="postarea">
<div class="cs_i_center">
<div class="cs_i_pic"></div>
<div class="cs_i_content">
<div class="cs_i_c_content">
<div class="about">
<div class="about_in">
<ul>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img1" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img1.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span>
</figure>
<div class="about_hover">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>OUR HISTORY</h2>
<a>Explore Now</a>
</div>
</div>
</div>
</div>
<div class="explore">
<div class="container">
<div class="explore_lft"><img alt="explore_img1" height="255" src="<?php echo BASE_URL();?>assets/images/explore_img8.png" width="460"></div>
<div class="explore_rgt">
<span>
<p>
When Margaret Morrissette started Architectural Gardens in 2007,  I wanted to create an open platform where a customer could call without being “pre-qualified” over the phone by a stuffy receptionist. I wanted to create a company where a home-owner could call in and have a friendly chat about my project and a welcoming appointment scheduled with a hands on owner. 10 years later and growing, I still meet with customers on a friendly one on one basis. We work with all sorts of customers who have all sorts of goals and challenges. Most with budgets and don’t want to be charged a fortune but want something beautiful. At the jobsite, we are like family. Working hand in hand with crews to ensure a team approach that likes to have fun working with you in the best way possible. We are good listeners , we like to collaborate with the homeowner and take not a laid back approach but a genuine intention to make it a great experience for you. Everything we do is backed with one of the best warranties in the business.</p>
</div>
<div class="clear">&nbsp;</div>
</div>
</div>
</li>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img2" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img2.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span>
</figure>
<div class="about_hover" jhref="<?php echo BASE_URL();?>showroom">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>SHOWROOMS</h2>
<a class="link" href="<?php echo BASE_URL();?>showroom">Explore Now</a>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img3" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img3.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span>
</figure>
<div class="about_hover">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>PRODUCT DEVELOPMENT</h2>
<a>Explore Now</a>
</div>
</div>
</div>
</div>
<div class="explore">
<div class="container">
<div class="explore_lft"><img alt="explore_img1" height="255" src="<?php echo BASE_URL();?>assets/images/explore_img7.png" width="460"></div>
<div class="explore_rgt"><span>
This has been the hallmark for Treasure Garden &ndash; consistently designing, testing and bringing new innovations to the shade industry. This attribute ensures greater brand life-cycle management and maintains our market growth.</span></div>
<div class="clear">&nbsp;</div>
</div>
</div>
</li>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img4" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img4.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span>
</figure>
<div class="about_hover">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>CAPABILITIES</h2>
<a>Explore Now</a>
</div>
</div>
</div>
</div>
<div class="explore">
<div class="container">
<div class="explore_lft"><img alt="explore_img1" height="255" src="<?php echo BASE_URL();?>assets/images/explore_img9.png" width="460"></div>
<div class="explore_rgt"><span>
We have always offered innovation, quality, and value, but unlike other manufacturers, Treasure Garden is a Vertically Integrated Manufacturer, ensuring a more balanced work-flow. This enables Treasure Garden the flexibility to respond quickly to market demands.</span></div>
<div class="clear">&nbsp;</div>
</div>
</div>
</li>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img5" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img5.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span></figure>
<div class="about_hover">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>PASSION &amp; DEDICATION</h2>
<a>Explore Now</a>
</div>
</div>
</div>
</div>
<div class="explore">
<div class="container">
<div class="explore_lft"><img alt="explore_img1" height="255" src="<?php echo BASE_URL();?>assets/images/explore_img6.png" width="460"></div>
<div class="explore_rgt"><span>
The 3,500 employees who share in the same vision on a daily basis and their attention to detail enables Treasure Garden to continue to be &quot;The World&#39;s Favorite Shade&quot;.</span></div>
<div class="clear">&nbsp;</div>
</div>
</div>
</li>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img6" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img6.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span></figure>
<div class="about_hover">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>MANUFACTURING</h2>
<a>Explore Now</a>
</div>
</div>
</div>
</div>
<div class="explore">
<div class="container">
<div class="explore_lft"><img alt="explore_img1" height="255" src="<?php echo BASE_URL();?>assets/images/explore_img3.png" width="460"></div>
<div class="explore_rgt"><span>
We manufacture and produce over 90% of the components used in our products from their raw state. This allows Treasure Garden the advantage of monitoring every stage of the manufacturing process and maintain a consistent level of quality.</span></div>
<div class="clear">&nbsp;</div>
</div>
</div>
</li>
<li>
<div class="about_inner">
<figure data-height="250" data-stage-type="best-fit" data-width="1300">
<span style="overflow:hidden"><img alt="about_img6" class="scale" data-align="center" data-scale="best-fill" src="<?php echo BASE_URL();?>assets/images/about_img7.png" style="position:absolute;top:0;left:0;width:1300px;height:250px;max-width:none"></span>
</figure>
<div class="about_hover" jhref="<?php echo BASE_URL();?>">
<div class="about_hover_in">
<div class="about_hover_inner">
<h2>PRESS &amp; NEWS</h2>
<a class="link" href="<?php echo BASE_URL();?>blog">Explore Now</a></div>
</div>
</div>
</div>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
<div>&nbsp;</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="bottom">
<?php include "includes/footer.php";?>