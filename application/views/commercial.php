<html>
<head>
    <title> Architectural Gardens | Commercial</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.css">
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
<div class="banar_content" style="background:rgba(0,0,0,0) url(<?php echo BASE_URL();?>assets/images/category/coll_img11_lg-min.png) no-repeat scroll center top">
<div class="container">
<div class="banar_in">
<h2>COMMERCIAL</h2>
</div>
</div>
</div>
<div class="">
<div class="left">
<div class="left_top_bg"></div>
<div class="left_center_bg"></div>
<div class="left_bottom_bg"></div>
<div style="clear:both"></div>
</div>
<div>
<div class="center_content">
<div id="products_list_content">
<div class='page_nav_line'></div>
<div id="products_list_content" class="product_list_grid">
<div class="container">
<div class="product_nav" style="background:none;margin-bottom:5px">
<div id='top_breadcrumb'>
<span>
<div><span itemscope itemtype="#"><a href="<?php echo BASE_URL();?>home" class="headerNavigation" itemprop="url"><span itemprop="title">Home</span></a></span> > <span itemscope itemtype="#"><a href="<?php echo BASE_URL();?>collection" class="headerNavigation" itemprop="url"><span itemprop="title">Collection</span></a></span> > <span itemscope itemtype="#"><span style="color:#000;text-transform:uppercase;font-weight:bold" itemprop="title">COMMERCIAL</span></span></div>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div style="overflow:auto;position:relative;height:60px;line-height:60px;border-top:1px solid #ededed;border-bottom:1px solid #ededed">
<div class="container subcategory" style="">
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">Cantilevers</span>
</a>
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">SPECIALTY</span>
</a>
<a href="#" style="margin-right:45px">
<span style="text-transform:uppercase">MARKET</span>
</a>
<a href="<?php echo BASE_URL();?>commercial" style="margin-right:45px">
<span style="font-weight:bold;text-transform:uppercase">COMMERCIAL</span>
</a>
<a href="<?php echo BASE_URL();?>accessories" style="margin-right:45px">
<span style="text-transform:uppercase">ACCESSORIES</span>
</a>
<a href="#l" style="margin-right:45px">
<span style="text-transform:uppercase">Fabrics</span>
</a>
</div>
</div>
<div class="product">
<div class="container">
<div class="product_in">
<ul class="plist" style="width:100%">
<li>
<a href="<?php echo BASE_URL();?>products"><span><img src="<?php echo BASE_URL();?>assets/images/products/UCP409_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UCP409_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UCP409_17_thumb.png'"></span>
<small>Commercial</small><em>Octagon Series <br>9'</em>
</a>
</li>
<li>
<a href="<?php echo BASE_URL();?>products"><span><img src="<?php echo BASE_URL();?>assets/images/products/UCP407_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407_17_thumb.png'"></span>
<small>Commercial</small><em>Octagon Series <br>7.5'</em>
</a>
</li>
<li>
<a href="<?php echo BASE_URL();?>products"><span><img src="<?php echo BASE_URL();?>assets/images/products/UCP407SQ_17_thumb.png" width="260" height="204" alt="img" onmouseover="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407SQ_17_thumb.png'" onmouseout="this.src='<?php echo BASE_URL();?>assets/images/products/UCP407SQ_17_thumb.png'"></span>
<small>Commercial</small><em>Square Series <br>7'</em>
</a>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<div class='page_nav_line'></div>
</div>
</div>
</div>
<div class="right"></div>
</div>
<div class="bottom">
<?php include "includes/footer.php";?>