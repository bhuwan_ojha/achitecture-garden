<html>
<head>
    <title> Architectural Gardens | Showroom </title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="keywords" content="Architectural Gardens">
    <meta name="description" content="Architectural Gardens">
    <base>
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.css">

    <script>var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;</script>
    <script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include 'includes/nav.php';?>
<div id="homecenter">
<div id="contentleft">
<div class="postarea">
<style>.bx-wrapper{margin-bottom:0}.bx-wrapper .bx-pager,.bx-wrapper .bx-controls-auto{bottom:10px;text-align:right}.bx-controls-direction{display:none}.bx-wrapper .bx-pager.bx-default-pager a{margin:0 2px}</style>
<div class="press"></div>
<div class="container">
<div style="padding-left:7px;background:0;margin-bottom:5px" class="product_nav">
<div id="top_breadcrumb" style="display:none">
<span><div>
<span itemscope=""><a itemprop="url" class="headerNavigation" href="#"><span itemprop="title">Home</span></a></span> &gt;
<span itemscope=""><a itemprop="url" class="headerNavigation" href="#"><span itemprop="title">About Us</span></a></span> &gt; <span itemtype="#" itemscope=""><span itemprop="title" style="color:#000;text-transform:uppercase;font-weight:bold">Showrooms</span></span>
</div></span>
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>
<div class="showroom-content-holder">
<div style="text-align:center;padding-bottom:30px">
<div style="text-transform:uppercase;font-size:30px;color:#000;padding:20px 0">LAS VEGAS</div>
<div><div>
<span style="color:#006400">WORLD MARKET CENTER - C1215</span></div>
<div>
<span style="color:#006400"><span class="_Xbe">475 S. GRAND CENTRAL PKWY </span></span></div>
<div>
<span style="color:#006400">LAS VEGAS, NV <span class="_Xbe">89106</span></span></div>
<div>
&nbsp;</div>
<div>
<span style="color:#000000"><span style="font-size:16px"><strong>OPEN JANUARY 22 - 26, 2017</strong></span></span></div>
</div>
<div style="width:30px;height:30px;border-bottom:1px solid #000;display:inline-block"></div>
</div>
<div class="facility-box">
<div class="bxslider0">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_LV_04.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_LV_03.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_LV_01.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_LV_02.jpg">
</div>
</div>
<script>$('.bxslider0').bxSlider({auto:true,controls:false});</script>
<div class="facility-map">
<div>
<iframe src="http://showroom.gso360.com/embed/68L_TreasureGarden" width="525" height="340" allowFullScreen="true" frameborder="0" scrolling="no"></iframe><p style='text-align:center;margin-top:10px'><a href='http://showroom.gso360.com/tour/68L_TreasureGarden?mobile=true' target='_blank' rel='nofollow'><img src="<?php echo BASE_URL();?>assets/images/button_fullscreen.png"></a></p> </div>
</div>
<div style="padding:30px 0 10px 0"><img width="100%" src="<?php echo BASE_URL();?>assets/images/f_bg.png"></div>
<div style="text-align:center;padding-bottom:30px">
<div style="text-transform:uppercase;font-size:30px;color:#000;padding:20px 0">CHICAGO</div>
<div><div>
<span style="color:#006400">MERCHANDISE MART - 1655</span></div>
<div>
<span style="color:#006400">222 W. MERCHANDISE MART PLAZA</span></div>
<div>
<span style="color:#006400">CHICAGO, IL <span class="_Xbe">60654</span></span></div>
<div>
&nbsp;</div>
<div>
<span style="color:#000000"><span style="font-size:16px"><strong>OPEN JULY 11 - 13, 2017</strong></span></span></div>
</div>
<div style="width:30px;height:30px;border-bottom:1px solid #000;display:inline-block"></div>
</div>
<div class="facility-box">
<div class="bxslider1">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_CHI_07.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_CHI_03.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_CHI_06.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_CHI_08.jpg">
<img width="100%" src="<?php echo BASE_URL();?>assets/images/Showroom_CHI_09.jpg">
</div>
</div>
<script>$('.bxslider1').bxSlider({auto:true,controls:false,});</script>
<div class="facility-map">
<div>
<iframe src="http://showroom.gso360.com/embed/69C_TreasureGarden" width="525" height="340" allowFullScreen="true" frameborder="0" scrolling="no"></iframe><p style='text-align:center;margin-top:10px'><a href='http://showroom.gso360.com/tour/69C_TreasureGarden?mobile=true' target='_blank' rel='nofollow'><img src='<?php echo BASE_URL();?>assets/images/button_fullscreen.png'></a></p> </div>
</div>
<div style="padding:30px 0 10px 0"><img width="100%" src="<?php echo BASE_URL();?>assets/images/f_bg.png"></div>
</div>
<script>$(".showroom-content-holder").imagesLoaded(function(){setHeights();});var int;$(window).resize(function(){clearInterval(int);int=setTimeout(function(){setHeights();},300);});function setHeights(){$(".facility-map").each(function(){$(this).find("iframe").height($(this).prev().prev(".facility-box").height())})}</script>
</div>
</div>
</div>
</div>
</div>
<div class="bottom">
<?php include 'includes/footer.php';?>