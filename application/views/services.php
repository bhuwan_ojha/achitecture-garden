<html>
<head>
<title>Architectural Gardens | Services</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="keywords" content="Architectural Gardens">
<meta name="description" content="Architectural Gardens">
<base>
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
<script>
var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;
</script>
<script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include 'includes/nav.php';?>
<div class="container">
<div style="margin-top:35px">
<h4><u>GARDEN DESIGN & INSTALLATION</u></h4>
<ul class="arrow" style="margin-left:60px">
<li class="arrow">Fully rendered detailed scaled drawings</li>
<li class="arrow">Architectural Drawings</li>
<li class="arrow">3D Elevation drawings</li>
<li class="arrow">Landscape Plantings – Trees, shrubs, perennial flowers</li>
<li class="arrow">Brick and Bluestone</li>
<li class="arrow">Patios and Walkways</li>
<li class="arrow">Brick driveways</li>
<li class="arrow">Stoops, Stairs and Porches</li>
<li class="arrow">Bluestone Masonry</li>
<li class="arrow">Stone seatwalls and masonry work</li>
<li class="arrow">Outdoor kitchens</li>
<li class="arrow">Outdoor Fireplaces</li>
<li class="arrow">Outdoor Firepits</li>
<li class="arrow">Woodwork - Pergolas, Custom Gates and Outdoor Pavilions</li>
<li class="arrow">Drainage & PVC downspout extensions</li>
<li class="arrow">Retaining Walls</li>
<li class="arrow">Landscape Lighting</li>
<li class="arrow">Perennial and Flower Gardens</li>
<li class="arrow">Raised Vegetable Gardens</li>
<li class="arrow">Permit Drawings and applications</li>
</ul>
</div>
<br>
<div>
<h4><u>Garden Care and Maintenance</u></h4>
<p>We offer several optional services to include both weekly full service landscape maintenance and also provide monthly detailed garden care so your property is always looking your best.</p>
<br>
<ul class="arrow" style="margin-left:60px">
<li>Turf mowing and fertilizations
<li>Pruning of ornamental trees and shrubs</li>
<li>Installation of Premium Shredded hardwood mulch</li>
<li>Planting of summer flowering annuals and seasonal containers</li>
<li>Power washing of decks, patios and walkways</li>
</ul>
</div>
<br>
<div>
<h4><u>Custom Carpentry</u></h4>
<p>Visual elements can enhance an outdoor space. Custom designed gates, arbors and trellis add a unique dimension to the garden. A Pergola or Pavillion can also add shade and create an intimate space to gather. We have custom carpenters on staff to provide you with an exception quality and detailed built to last.</p>
<br>
<ul class="arrow" style="margin-left:60px">
<li>Pergola structures
<li>Custom Gates</li>
<li>Outdoor Pavilions</li>
<li>Trellis</li>
<li>Arbors</li>
<li>Decks – both cedar and composite available</li>
<li>Staircases</li>
<li>Storage sheds</li>
</ul>
</div>
<br>
<div>
<h4><u>Seasonal Rotation</u></h4>
<h5>Season long color loaded with texture and visual appeal.</h5>
<p>
We have a seasonal rotation of decorative floral displays with colorful annuals, interesting branching, pods and luscious trailing vines. All of our urns are custom designed to suit your taste and favorite color ranges.  We can do a one time stunning arrangement for a special party or Graduation or schedule a seasonal rotation. We can also plant summer annuals for a creative display by your patio or front entry way. Our holiday urns are loaded with fresh organic greens, pods, and colorful branching and berries. We also offer guidance or order and deliver containers of your choosing.
</p>
</div>
</div>
<style>ul.arrow{list-style-type:square}</style>
<div class="right"></div>
</div>
<div class="bottom">
<?php include 'includes/footer.php';?>