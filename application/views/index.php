<html>
<head>
<title> Architectural Gardens | Home </title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="keywords" content="Architectural Gardens">
<meta name="description" content="Architectural Gardens">
<base>
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/stylesheet.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.css">
<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.css">
<script>
var g_js_web_root_dir="";var g_js_web_language="";var g_js_string_spliter="";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;
</script>
<script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
</head>
<body>
<div class="wrapper">
<?php include "includes/nav.php"; ?>
<div class="center">
<div id="homepage">
<div class="main_slider">
<ul class="bxslider">
<li>
<div class="slider">
<img src="<?php echo BASE_URL();?>assets/images/jsbanner/Banner-ArchBench.jpg" width="1920" height="756">
<div class="slider_in middle">
<div class="slider_content middle">
<div class="slider_txt">
<div class="container12">
<div>
<p style="margin-bottom:12px">Its more than a design… <br> It’s a lifestyle<br></p>
<div class="d">
<a href="#" style="width:69px">ABOUT US</a>
<a href="#" class="aligning" style="margin-top:-11px">CONTACT US</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="slider">
<img src="<?php echo BASE_URL();?>assets/images/jsbanner/Banner-Queens.jpg" width="1920" height="756">
<div class="slider_in middle">
<div class="slider_content middle">
<div class="slider_txt">
<div class="container12">
<p style="margin-bottom:12px">Award winning <br> Garden Design & Installation</p>
<div class="d">
<a href="#">LEARN MORE</a>
<a href="#" class="aligning" style="margin-top:-11px">CONTACT US</a>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="slider">
<img src="<?php echo BASE_URL();?>assets/images/jsbanner/Banner-ILCA.jpg" width="1920" height="756">
<div class="slider_in middle">
<div class="slider_content middle">
<div class="slider_txt">
<div class="container12">
<p style="margin-bottom:12px">Keep it Spectacular <br>  Garden Care & Maintenance</p>
<div class="d">
<a href="#">LEARN MORE</a>
<a href="#" class="aligning" style="margin-top:-11px">CONTACT US</a>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="slider">
<img src="<?php echo BASE_URL();?>assets/images/jsbanner/Banner-Urns.jpg" width="1920" height="756">
<div class="slider_in middle">
<div class="slider_content middle">
<div class="slider_txt">
<div class="container12">
<p style="margin-bottom:12px">Color your Lifestyle <br> Seasonal Rotations</p>
<div class="d">
<a href="#">LEARN MORE</a>
<a href="#" class="aligning" style="margin-top:-11px">CONTACT US</a>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="slider">
<img src="<?php echo BASE_URL();?>assets/images/jsbanner/Banner-Alice.jpg" width="1920" height="756">
<div class="slider_in middle">
<div class="slider_content middle">
<div class="slider_txt">
<div class="container12">
<p style="margin-bottom:12px">Illuminate your Sanctuary <br> Custom Carpentry</p>
<div class="d">
<a href="#">LEARN MORE</a>
<a href="#" class="aligning" style="margin-top:-11px">CONTACT US</a>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="slider">
<img src="<?php echo BASE_URL();?>assets/images/jsbanner/Banner-ArchBench.jpg" width="1920" height="756">
<div class="slider_in middle">
<div class="slider_content middle">
<div class="slider_txt">
<div class="container12">
<p style="margin-bottom:12px">About Us & You <br> It's Really <u>About</u> You!</p>
<div class="d">
<a href="#">LEARN MORE</a>
<a href="#" class="aligning" style="margin-top:-11px">CONTACT US</a>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
</ul>
</div>
<script>/*<![CDATA[*/$(document).ready(function(){$(".bxslider").bxSlider({auto:true,pause:7000,randomStart:false,preloadImages:"all"});if($(window).width()<=1024){$("ul.bxslider li .slider_in").each(function(){$(this).css("margin-top","-"+($(this).outerHeight()/2)+"px")})}});/*]]>*/</script>
<div>
<p class="all-same-text"><blockquote class="arch-quote">Architectural Gardens is an award winning landscape Architect firm. We specialize in residential landscape design and installation for homeowners on Chicago’s Northshore. Our goal is to collaborate with you into turning your garden into something spectacular<cite style="color:black">Architectural Gardens</cite></blockquote></p>
</div>
<div class="main_content" style="background-color:darkgray">
<div class="container12">
<br>
<div>
<h2 style="font-size:30px;margin-top:25px;margin-bottom:12px;color:white;background-color:darkgrey">Portfolio</h2>
<br>
<p class="all-same-text">Inspiring/transformational/collaboration/sensitive to budgets/colorful</p>
<p class="all-same-text">Exceptional quality, impeccable warranties along with award winning designs is how we built our business. Let us help you with your next landscape project.</p>
</div>
<br>
<div class="left_content dark_block" style="margin-top:32px">
<ul>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/left_img1.png" width="439" height="589">
<span class="home_txt">
<small class="dark_hover">
<em>Outdoor Rug Collection <br>
<span style="width:45px;border-top:1px solid #fff;display:inline-block"></span>
<br>
<span style="padding-top:8px;font-size:11px">Explore More</span>
</em>
</small>
</span>
</a>
</li>
</ul>
<div class="oasis">
<h2 style="cursor:default">Timeless Technology & Iconic Styles</h2>
<p style="padding-bottom:0"></p>
<a href="#">EXPLORE MARKET COLLECTION</a>
</div>
</div>
<div class="right_content" style="margin-bottom:70px">
<div class="beauty">
<h2>Exceptional Quality <br> in Every Color</h2>
<a href="#">EXPLORE FABRIC COLLECTION</a></div>
<ul>
<li class="dark_block extra_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/right_img1.png" width="319" height="319">
<span class="home_txt">
<small class="dark_hover">
<em>Specialty Collection <br>
<span style="width:45px;border-top:1px solid #fff;display:inline-block"></span>
<br><span style="padding-top:8px;font-size:11px">EXPLORE More</span>
</em>
</small>
</span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/right_img2.png" width="319" height="319">
<span class="home_txt">
<small class="dark_hover">
<em>TG DESIGN STUDIO<br>
<span style="width:45px;border-top:1px solid #fff;display:inline-block"></span>
<br><span style="padding-top:8px;font-size:11px">EXPLORE More</span>
</em>
</small>
</span>
</a>
</li>
<li class="dark_block">
<a href="#">
<img src="<?php echo BASE_URL();?>assets/images/right_img3.png" width="640" height="299">
<span class="home_txt">
<small class="dark_hover">
<em>Cantilever Collection <br>
<span style="width:45px;border-top:1px solid #fff;display:inline-block"></span>
<br><span style="padding-top:8px;font-size:11px">EXPLORE More</span>
</em>
</small>
</span>
</a>
</li>
</ul>
<div class="clear"></div>
</div>
<br>
<div class="clear"></div>
</div>
<style>
.all-same-text{font-size:20px;text-align:left;word-wrap:break-word}
.container12{width:50%;padding:0 20px;box-sizing:border-box;margin:0 auto}
@media screen and (max-width: 992px){.container12{width:99%;box-sizing:border-box;margin:0 auto;padding:0 20px;text-align:center}}
</style>
</div>
</div>
</div>
<div class="bottom" style="background-color:white">
<div style="text-align:center">
<h2 style="font-size:30px;margin-bottom:15px;margin-top:45px">Quote</h2>
<br>
<blockquote class="quote-quote">
We don't have the words to describe how incredible our backyard looks especially at night, with the patio and gardens all lit up.  It feels less like a yard and more like an experience.  The ambiance is more than we could've asked for.  Thanks for everything! <cite style="color:black"> Pete & Austin T.</cite></blockquote>
</div>
<?php include "includes/footer.php";?>