<!DOCTYPE html>
<html>
<head>
<title> Architectural Gardens | Get A Meeting</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="keywords" content="Architectural Gardens">
<meta name="description" content="Architectural Gardens">
<base>
<link rel="stylesheet" href="<?php echo BASE_URL(); ?>assets/css/stylesheet.css">
<link rel="stylesheet" href="<?php echo BASE_URL(); ?>assets/css/validationEngine.jquery.css">
<script>
var g_js_web_root_dir = "";var g_js_web_language = "";var g_js_string_spliter = "";var g_js_d_d_f_a = "";var g_user_id = "0";var g_guest_flag = "0";var g_js_is_backoffice = false;
</script>
<script src="<?php echo BASE_URL(); ?>assets/js/1.7/jquery-1.7.1.min.js"></script>
<script src="<?php echo BASE_URL(); ?>assets/js/jquery_plugin/jquery.lazyload.js"></script>
<script src="<?php echo BASE_URL(); ?>assets/js/bxslider/jquery.bxslider.js"></script>
<script src="<?php echo BASE_URL(); ?>assets/js/fancybox/source/jquery.fancybox.js"></script>
<script src="<?php echo BASE_URL(); ?>assets/js/jquery.validationEngine-en.js"></script>
<script src="<?php echo BASE_URL(); ?>assets/js/jquery.validationEngine.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>
<body>
<div class="wrapper">
<?php include 'includes/nav.php'; ?>
<div class="" style="min-height:300px;background:url(<?php echo BASE_URL(); ?>assets/images/contact_map.png) no-repeat center top"></div>  <!-- empty class do we need it -->
<div id="homecenter">
<div id="contentleft">
<div class="postarea">
<div class="cs_i_center">
<div class="cs_i_pic"></div>
<div class="cs_i_content">
<div class="cs_i_c_content"></div>
</div>
</div>
<div class="cs_i_center">
<div style="clear:both;margin-top:45px"></div>
<div style="text-align:center;font-size:28px;color:#505050">BECOME AN AUTHORIZED DEALER</div>
<div style="height:2px;border-bottom:1px solid #000;width:20px;margin:20px auto"></div>
<div style="max-width:680px;margin:0 auto">
<form id="id_contact_us_form" action="<?php echo BASE_URL();?>send-mail" method="POST">
<div class="" style="width:100%">  <!-- empty class do we need it -->
<table style="width:100%" class="contact">
<tr>
<td>
<div class="regleft">
<div class="user_content">
<span class="require_field1">*</span>
<input id="name" class="contact_us_item validate[required]" type="text" name="name" placeholder="Full Name">
</div>
</div>
</td>
<td>
<div class="regleft">
<div class="user_content">
<span class="require_field1">*</span>
<input id="email" class="contact_us_item validate[required]" type="text" name="email" placeholder="Email">
</div>
</div>
</td>
</tr>
<tr>
<td>
<div class="regleft">
<div class="user_content">
<span class="require_field1">*</span>
<input id="companyName" class="contact_us_item validate[required]" type="text" name="companyName" placeholder="Company Name">
</div>
</div>
</td>
<td>
<div class="regleft">
<div class="user_content">
<span class="require_field1">*</span>
<input id="phone" class="contact_us_item validate[required]" type="text" name="phone" placeholder="Phone Number">
</div>
</div>
</td>
</tr>
<tr>
<td colspan="2" rowspan="5">
<div class="regleft contact-textarea" style="margin-bottom:10px">
<div class="user_content">
<span class="require_field1">*</span>
<textarea id="comment" class="contact_us_item validate[required]" placeholder="Your Comment" style="width:100%;height:90px" wrap="soft" name="comment"></textarea>
</div>
</div>
<div style="clear:both;display:none">*Required fields</div>
</td>
</tr>
</table>
</div>
<div style="text-align:center">
<a style="" class="btn_common" id="reset">RESET</a>
<div style="margin-left:210px;margin-top:-33px">
<button class="g-recaptcha btn_common" data-callback="onSubmit" data-sitekey="<?php echo RECAPTCHA_KEY; ?>" style="background-color:#669463;height:33px">
SEND
</button>
</div>
<div style="padding:20px 0 0 0;margin:0 20px">For general inquiries, contact us at <a href="#" style="color:#669463">info@architecturalgardens.com</a></div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="bottom">
<script>
function onSubmit() {
var name = $('#name').val();
var email = $('#email').val();
var companyName = $('#companyName').val();
var phone = $('#phone').val();
var comment = $('#comment').val();
if(name && email && companyName && phone && comment !=''){
document.getElementById("id_contact_us_form").submit();
}
}
</script>
<?php include 'includes/footer.php'; ?>