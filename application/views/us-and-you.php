<!DOCTYPE html>
<html>
<head>
<title>Architectural Gardens | Us & You!</title>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
<meta name="keywords" content="Us & You">
<meta name="description" content="Us & You">
<base>
<link rel="stylesheet" href="<?php echo BASE_URL(); ?>assets/css/stylesheet.css">
<script>/*<![CDATA[*/var g_js_web_root_dir="<?php echo BASE_URL();?>";var g_js_web_language="en";var g_js_string_spliter="________________________";var g_js_d_d_f_a="";var g_user_id="0";var g_guest_flag="0";var g_js_is_backoffice=false;function js_get_themes_root(){return g_js_web_root_dir+"assets/";}/*]]>*/</script><script src="<?php echo BASE_URL();?>assets/js/1.7/jquery-1.7.1.min.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/jquery_ex.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/fancybox/source/jquery.fancybox.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/bxslider/jquery.bxslider.js"></script>
<script src="<?php echo BASE_URL();?>assets/js/image-scale.js"></script>
<script>
$(document).ready(function(e){$(".about_hover a").click(function(event){if($(this).hasClass('link')){return;}
if(!$(this).parent().parent().parent().parent().parent().hasClass('active')){$('.explore').hide();$('.explore').parent().removeClass('active');}
$(this).parent().parent().parent().parent().parent().find(".explore").slideToggle();$(this).parent().parent().parent().parent().parent().toggleClass('active');var exp=$(this);});});$(window).resize(function(){$(function(){$("img.scale").imageScale();});});$(window).load(function(){$(function(){$("img.scale").imageScale();});});
</script>
<script src="<?php echo BASE_URL();?>assets/js/custom.js"></script>
<script>
document.createElement("header");document.createElement("nav");document.createElement("section");document.createElement("article");document.createElement("aside");document.createElement("footer");
</script>
</head>
<script>
function start_scroll(){var item=$("#tinyscrollbar");item.jscroll({W:"10px",BgUrl:"",Bg:"#F0752F",Bar:{Pos:"up",Bd:{Out:"#F0752F",Hover:"#F0752F"},Bg:{Out:"#F0752F",Hover:"#F0752F",Focus:"#F0752F"}},Btn:{btn:false,uBg:{Out:"#F0752F",Hover:"#F0752F",Focus:"#F0752F"},dBg:{Out:"#F0752F",Hover:"#F0752F",Focus:"#F0752F"}},Fn:function(){}});}
</script>
<body>
<div class="wrapper">
<?php include "includes/nav.php";?>
<div class="container" style="margin-top:60px">
<h4> US & YOU (But it's about You!)</h4>
<br>
<p>When Margaret Morrissette started Architectural Gardens in 2007,  I wanted to create an open platform where a customer could call without being “pre-qualified” over the phone by a stuffy receptionist. I wanted to create a company where a home-owner could call in and have a friendly chat about my project and a welcoming appointment scheduled with a hands on owner. 10 years later and growing, I still meet with customers on a friendly one on one basis. We work with all sorts of customers who have all sorts of goals and challenges. Most with budgets and don’t want to be charged a fortune but want something beautiful. At the jobsite, we are like family. Working hand in hand with crews to ensure a team approach that likes to have fun working with you in the best way possible. We are good listeners , we like to collaborate with the homeowner and take not a laid back approach but a genuine intention to make it a great experience for you. Everything we do is backed with one of the best warranties in the business.</p>
</div>
<div class="bottom">
<?php include "includes/footer.php";?>