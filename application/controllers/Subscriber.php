<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscriber extends CI_Controller{

    private $table;
    private $CommonModel;
    public function __construct(){
        parent::__construct();
        $this->CommonModel = new Common_Model();
        $this->table = "tbl_subscribers";
    }

    public function index(){
        $date = $this->CommonModel->getCurrentDateTime();

        $this->CommonModel->insert("tbl_subscribers",array('email'=>$_POST['email'],'subscribed_date'=> $date[0]['CURRENT_TIMESTAMP'] ));
    }
}