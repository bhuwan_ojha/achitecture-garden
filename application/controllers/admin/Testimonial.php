<?php

class Testimonial extends CI_Controller
{
    private $CommonModel;
    public function __construct()
    {
        parent::__construct();
        if(check_user('current_user')){

        }
        $this->CommonModel = new Common_Model();
    }

    public function index(){
        $data = $this->CommonModel->get_all("tbl_testimonials");
        $this->load->view('admin/testimonial-list',array('data' => $data));
    }

}