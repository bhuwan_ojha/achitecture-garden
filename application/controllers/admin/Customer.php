<?php

/**
 * Created by PhpStorm.
 * User: Nepsrock
 * Date: 2017-02-05
 * Time: 2:29 PM
 */
class Customer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->CommonModel = new Common_Model();

    }


    function index()
    {
        $data['data'] = $this->CommonModel->get_all('tbl_customers');
       $this->load->view('admin/index',$data);
    }

    function Delete(){
        $id = $_POST['id'];
        $this->CommonModel->delete_data('tbl_customers',array('id'=>$id));
    }

    function Add(){
        $data = $_POST;
        $this->CommonModel->insert('tbl_customers',$data);
        redirect('admin/Customer');
    }

    function Update(){
        $data = $_POST;
        $this->CommonModel->update('tbl_customers',$data,array('id'=>$data['id']));
    }


}