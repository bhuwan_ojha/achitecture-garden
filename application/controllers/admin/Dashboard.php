<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/15/2016
 * Time: 3:42 PM
 */

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!check_user('current_user')) {
        redirect('admin');
        }
    }

    public function index(){
        redirect('admin/Customer');
    }

} 