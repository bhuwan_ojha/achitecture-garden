<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Seo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if(!check_user('current_user')) {
            redirect('admin');
        }
        $this->CommonModel = new Common_Model();
    }

    function index()
    {
        $seo = $this->CommonModel->get_all('tbl_seo', '', 'id DESC');
        $this->load->view('admin/seo-manager', array('seo'=>$seo));
    }

    function form(){
        $this->load->view('admin/seo-form');
    }

    function add_update()
    {
        $seo_id = segment(3);
        if($_POST) {

            $post = array(
                'name'=> $_POST['name'],
                'link'=> $_POST['link'],
                'title'=> $_POST['title'],
                'description'=> $_POST['description'],
                'keywords'=> $_POST['keywords']
            );
            if(($_POST['id'])=='') {
                $this->CommonModel->insert('tbl_seo', $post);
                set_flash('msg', 'SEO Details Saved');
            } else {
                $this->CommonModel->update('tbl_seo', $post, array('id' => $_POST['id']));
            }
            set_flash('msg', 'SEO details Updated');
            redirect('admin/seo');
        } else{
                $data= $this->CommonModel->get_where('tbl_seo', array('id' => $seo_id));
                $this->load->view('admin/seo-form',array('seo'=>$data));
            }


    }

    function delete()
    {

        $this->CommonModel->delete_data('tbl_seo', array('id' => $_POST['id']));
        set_flash('msg', 'SEO Details Deleted');
        redirect('admin/seo');
    }

    function common(){

        if($_POST) {
            $post = $_POST;
            $this->CommonModel->update('tbl_seo_common', $post, array('id' => 1));
            set_flash('msg', 'Common SEO Details Updated');
        }

        $data = $this->CommonModel->get_where('tbl_seo_common', array('id' => 1));
        $this->load->view('admin/meta-tag', array('data'=> $data));

    }
}