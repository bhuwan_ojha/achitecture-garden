<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/14/2016
 * Time: 12:42 PM
 */


class Login extends CI_Controller {

    private $CommonModel;

    public function __construct(){
        parent::__construct();
        if(check_user('current_user')) {

        }
        $this->CommonModel = new Common_Model();

    }

    public function Index(){
        $data = 'admin/login';
        $this->load->view($data);
    }

    public function Authenticate(){
        $user = $_POST;
        $user = $this->CommonModel->get_where('tbl_user', array('username' => $user['username'], 'password' => $user['password']));

        if($user) {
            $this->session->set_userdata(array(
                'current_user' => $user[0]['id'],
                'username' => $user[0]['username']
            ));
            redirect('admin');
        } else {
            set_flash('msg', 'Incorrect username or password');
           redirect('admin');
        }
    }

    public function UpdateUser(){
       $user = $_POST;
        $user['username'] = $_POST['username'];
        $user['email'] = $_POST['email'];
        $user['password'] = $_POST['password'];
        $user['id'] = $_POST['id'];
        $result = $this->CommonModel->update('tbl_user',$user,array('id' => $user['id']));
        if($result==TRUE){
           set_flash('msg','Successfully Updated User');
        }else{
            set_flash('msg','Failed Updating User');
        }
    }

    public function user(){
        $data = $this->CommonModel->get_all($table='tbl_user');
            $this->load->view('admin/user-manager', array("data" => $data) );
        }

    function logout()
    {
        $this->session->unset_userdata(array('current_user' => '', 'username' => ''));
        session_destroy();
        redirect('admin');
    }
    function forgotPassword()
    {
        $this->load->view('admin/forgot-password');
    }

    function checkEmailExist()
    {
        $email = $_POST;
        $result = $this->CommonModel->get_where('tbl_user', array('email' => $email['email']));
        if ($result) {
            $newPassword = $this->random_str(8);
            $this->db->trans_begin();
            $changePassword = $this->CommonModel->update('tbl_user', array('password'=>$newPassword), array('email' => $email['email']));
            if ($changePassword) {
                include_once APPPATH . 'third_party/autoload.php';
                $mail = new PHPMailer;

                $mail->SMTPDebug = 0;
                $mail->isSMTP();
                $mail->Host = "smtp.gmail.com";
                $mail->SMTPAuth = true;
                $mail->Username = PHPMAILER_USERNAME;
                $mail->Password = PHPMAILER_PASSWORD;
                $mail->SMTPSecure = "tls";
                $mail->Port = 587;

                $mail->From = NO_REPLY_EMAIL;
                $mail->FromName = SITE_NAME;

                $mail->addAddress(ADMIN_EMAIL, SITE_NAME);

                $contact_emailer = $this->load->view('admin/email-template', array('password' => $newPassword), true);
                $mail->isHTML(true);
                $mail->Subject = "Subject Text";
                $mail->Body = $contact_emailer;
                if (!$mail->send()) {
                    $this->session->set_flashdata("Error Sending Message");
                    $this->db->trans_rollback();
                } else {
                    $this->session->set_flashdata('message', "We've sent you email with password reset code!");
                    $this->db->trans_commit();
                }
                redirect('admin');
            } else {
                set_flash('msg', 'Invalid Email Address!');
                redirect('forgot-password');
            }


        }
    }

    function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[rand(0, $max)];
        }
        return $str;
    }



}
