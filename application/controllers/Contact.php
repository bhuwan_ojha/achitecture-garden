<?php

/**
 * Created by PhpStorm.
 * User: Nepsrock
 * Date: 2017-01-31
 * Time: 10:30 PM
 */
class Contact extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function sendMail()
    {
        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'companyName' => $_POST['companyName'],
            'phone' => $_POST['phone'],
            'comment' => $_POST['comment']
        );

        $mail = new PHPMailer;

        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = PHPMAILER_USERNAME;
        $mail->Password = PHPMAILER_PASSWORD;
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->From = NO_REPLY_EMAIL;
        $mail->FromName = SITE_NAME;

        $mail->addAddress(ADMIN_EMAIL, SITE_NAME);

        $contact_emailer = $this->load->view('contact_email',array('data' => $data),true);
        $mail->isHTML(true);
        $mail->Subject = "Subject Text";
        $mail->Body = $contact_emailer;
        if (!$mail->send()) {
            $this->session->set_flashdata("Error Sending Message");
        } else {
            $this->session->set_flashdata('message', "Your query sent successfully. We'll get back to you soon!");
        }

        redirect('get-a-meeting');
    }
}

