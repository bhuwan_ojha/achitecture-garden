<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 1/6/2017
 * Time: 11:33 AM
 */

class Pages extends CI_Controller{


    public function index()
    {
        $this->load->view('index');
    }
    public function collection()
    {
        $this->load->view('collection');
    }
    public function about()
    {
        $this->load->view('about');
    }
    public function blog()
    {
        $this->load->view('blog');
    }
    public function policy()
    {
        $this->load->view('policy');
    }
    public function showroom()
    {
        $this->load->view('showroom');
    }
    public function contact()
    {
        $this->load->view('get-a-meeting');
    }

    public function howToBuy()
    {
        $this->load->view('how-to-buy');
    }

    public function accessories(){
        $this->load->view('accessories');
    }
    public function commercial(){
        $this->load->view('commercial');
    }
    public function productDetails(){
        $this->load->view('product-details');
    }

    public function service(){
        $this->load->view('services');
    }

    public function usAndYou()
    {
        $this->load->view('us-and-you');
    }

    public function sitemap(){
        $this->load->view('sitemap');
    }

    public function ArchBlog(){
        $this->load->view('arch-blog');
    }

} 