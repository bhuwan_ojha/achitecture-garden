var cart_trigger;
var js_cart = {
	cart_right:362,
	add_to_cart:function(combo_id,products_id,qty){
		g_js.js.ajax.fg.get(3,1,'combo_id='+combo_id+'&products_id='+products_id+'&qty='+qty+'&wish_id=0',function(v) {
			if(v < 0){
				$.jq_alert(js_language.cst_product_qty_not_enough);
			}
			else {
				window.location.href = 'process_cart.html';
				//js_cart.add_to_cart_effect(products_id);
				//js_cart.show_cart_box();
			}
		});
	},
	add_to_cart2:function(products_id,product_sku_code,qty){
		g_js.js.ajax.fg.get(3,1,'product_sku_code='+product_sku_code+'&products_id='+products_id+'&qty='+qty+'&wish_id=0',function(v) {
			if(v < 0){
				$.jq_alert(js_language.cst_product_qty_not_enough);
			}
			else {
				//window.location.href = 'process_cart.php';
				//js_cart.add_to_cart_effect(products_id);
				js_cart.show_cart_box();
			}
		});
	},
	add_to_cart_effect:function(products_id){
		//$("#batc_"+products_id).effect("transfer",{ to: "#id_show_cart_box", className: 'button_add_to_cart_effect' },2000,function(){});
	},
	change_country:function(country_obj, state_obj) {
		var country = country_obj.val();
		g_js.js.ajax.fg.get(5,0,'address_type='+0+'&country='+country,function(v) {
			state_obj.html(v);
			var oDropdown = state_obj.msDropdown().data("dd");
			oDropdown.destroy();
			state_obj.msDropdown()
		});
		
	},
	add_to_cart4:function(){
		var products_id = '';
		var error = 0;
		var error_message = '';
		$(".nile_item_no").each(function(idx,obj){
			if(!js_is_null($(this).val())) {
				var item_no = $(this).val();
				var color = '';
				var qty = $("input[name='qty']:eq("+idx+")").val();
				if(qty == null || qty == "" || qty == "0") {
					error = 1;
					error_message += "Please input the QTY for item '"+item_no+"'.\r\n";
					qty = 1;
				}
				products_id += item_no+":"+color+":"+qty+",";
			}
		});
		if(products_id && error == 0) {
			g_js.js.ajax.fg.get(3,25,'products_id='+products_id,function(v) {
				if (v != 1){
					alert(v);
				}
				else {
					js_cart.goto_address_info_page();
				}
			});
		}
		else if(error == 1) {
			alert(error_message);
		}
		else {
			alert("Sorry, you don't input any product.");
		}
		return false;
	}
	,
	add_to_cart5:function(){
		var products_id = '';
		var param = $("#item_input").val();
		var item_no = '';
		var color = '';
		var qty = '';
		var error = 0;
		var error_message = '';

		if(!js_is_null(param)) {
			var params = param.split("\n");
			for(var pi=0;pi<params.length;pi++) {
				if(params[pi]) {
					param = params[pi];
					tem = param.split("	");
					item_no = tem[0];
					qty = tem[1];
					if(qty == " " || qty == "" || qty == "0" || qty == undefined) {
						qty = 1;
					}
					products_id += item_no+":"+color+":"+qty+",";
				}
			}
		}
		if(products_id) {
			g_js.js.ajax.fg.get(3,25,'products_id='+products_id,function(v) {
				if (v != 1){
					alert(v);
				}
				else {
					js_cart.goto_address_info_page();
				}
			});
		}
		else if(error == 1) {
			alert(error_message);
		}
		else {
			alert("Sorry, you don't input any product.");
		}
		return false;
	},
	goto_address_info_page:function(){
		location.href = 'index-2.html';
	}
	,
	select_color:function(item){		
		var num = $(item).attr("name");
		var id_item = "#id_nile_item_no_" + num;
		var color_option = "#color_option_" + num;
		g_js.js.ajax.fg.get_color(3,24,id_item,function(v) {
			var arr = v.split(",");
			 var output = [];
			 output.push('<option>Select Product Color</option>');
			 $.each(arr, function(key, value){
				 if(value) {
					 output.push('<option>'+ value +'</option>');
				 }
			 });
			 $(color_option).html(output.join(''));
		});		
	},
	add_wish_to_cart:function(products_id,wishlist_id,qty){
		g_js.js.ajax.fg.get(3,13,'products_id='+products_id+'&qty='+qty+'&wish_id='+wishlist_id,function(v) {
			if (v < 0){
				$.jq_alert(js_language.cst_product_qty_not_enough);
			} else {
				js_cart.show_cart_box();
			}
		});		
	},
	add_customed_to_cart:function(products_id,customed_param,qty){
		g_js.js.ajax.fg.get(3,18,'products_id='+products_id+'&qty='+qty+'&customed_param='+customed_param,function(v) {
			if (v < 0){
				$.jq_alert(js_language.cst_customed_info_required);
			} else {
				js_cart.show_cart_box();
			}
		});
	},
	ini_car_box:function(){
		if(($('#id_show_cart_box').length > 0)){
			$("#id_show_cart_box").click(function(){
				js_cart.show_cart_box(false);
			});
		}
		
		$(".sc").hover(
				function(){
					clearTimeout(cart_trigger);
				},
				function(){
					cart_trigger = setTimeout(function(){
						$(".sc").slideUp("fast");
				    },3000);
				}
		);
	},
	show_cart_box:function(add_to_cart) {
		/*
		if($("#a_view_cart").length>0){
			g_js.js.ajax.fg.get(3,5,'cart_title=BOX_HEADING_VIEW_CART',function(v) {
				$('#a_view_cart').html(v);
			});
		}
		if(($('#id_show_cart_box').get().length > 0)){
			g_js.js.ajax.fg.get(3,5,'cart_title=TEXT_TOP_MENU_SHOPPING_BAG',function(v) {
				$('#id_show_cart_box').html(v);
			});
			g_js.js.ajax.fg.get(3,4,'',function(v) {
				$('#id_cart_box').html(v);
				js_cart.slide_cart_box(add_to_cart);
			});				
		}
		*/
		var html_body = '<div class="landing"><div style="width:100%;margin:66px auto;text-align:center;">Your item has been added successfully to cart, please click on <a href="ex_checkout.php" style="color:#f07b36;font-weight:bold;">Checkout</a> to continue or complete your order.</div></div>';
		g_js.windows.html.create_popup_landing(html_body,function(){},450,180,true);
	},
	slide_cart_box:function(add_to_cart){
		if($(".sc:visible").length>0){
			if(add_to_cart!=undefined){
				$(".sc").slideUp("fast");
			}
		}else{
			$(".sc").slideDown("fast");
			
			clearTimeout(cart_trigger);
			cart_trigger = setTimeout(function(){
				$(".sc").slideUp("fast");
		    },3000);
			
		}	
	},
	close_cart_box:function() {
		$(".sc").slideUp("fast");
	},
	edit_gift_wrap:function(pid) {
		g_js.js.ajax.fg.get(3,8,'pid='+pid,function(v) {
			g_js.windows.html.create_popup(v,function(){},700,560,true);
		});
	},
	save_gift_wrap:function() {
		g_js.js.ajax.fg.post(3,9,"#id_form_gift_wrap","",function(v){
			window.location.href = window.location.href;
		});
		
	},
	close_popup:function() {
		g_js.windows.html.close_popup();
	},
	save_for_later:function(pid){
		g_js.js.ajax.fg.get(3,10,'pid='+pid,function(v) {
								window.location.href = window.location.href;
							    });	
	},
	move_to_cart:function(pid){
		g_js.js.ajax.fg.get(3,3,'pid='+pid,function(v) {
								window.location.href = window.location.href;
							    });	
	},
	delete_item2:function(pid){
		g_js.js.ajax.fg.get(3,6,'pid='+pid,function(v) {
			window.location.href = window.location.href;
			//"process_shipping.php";
		});
	},
	delete_item:function(pid){
		g_js.js.ajax.fg.get(3,6,'pid='+pid,function(v) {
			window.location.href = window.location.href;
		});	
			
	},
	drop_item:function(pid){
		g_js.js.ajax.fg.get(3,11,'pid='+pid,function(v) {
								window.location.href = window.location.href;
							    });
	},
	update_cart:function(){
		g_js.js.ajax.fg.post(3,7,"#id_shopping_cart","",function(v){
			if (v < 0){
				$.jq_alert(js_language.cst_product_qty_not_enough);
			}
			window.location.href = window.location.href;
		});
	},
	check_out:function(){
		if( js_get_user_id()==0){
			g_js.js.ajax.fg.get(3,15,'',function(v) {

				var str_url = 'index-2.html';
				if (v == '0'){
					str_url = 'process_cart.html';
				}
				js_user.login(str_url,0);
			});

			
		} else {
			g_js.js.ajax.fg.get(3,15,'',function(v) {
				if (v == '1'){
					g_js.js.ajax.fg.get(3,16,'',function(v) {window.location.href = v;});
				} else {
					g_js.js.ajax.fg.get(3,17,'',function(v) {
						g_js.windows.html.create_popup(v,function(){},790,435,true);
					});
				}
			});
		}
	},
	confirm_checkout:function() {
		g_js.js.ajax.fg.get(3,16,'',function(v) {window.location.href = v;});
	},
	continue_shopping:function() {
		g_js.js.ajax.fg.get(3,14,'',function(v) {window.location.href = v;});
	},
	recalculate:function() {
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(3,12,"#id_shipping_calculate_form","",function(v){
			$("#id_shipping_calculate").html(v);
			g_js.windows.mask.busy.close();
		});
	},
	
	//coupon
	apply_coupon_cart_page:function(){
		var couponcode = $("#id_couponcode").val();
		var couponcode_x ='';
		if (couponcode.replace(/ /g,'') != '' ){
			couponcode_x = couponcode;
		}
		if (couponcode_x.replace(/ /g,'') != ''){
			//g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.get(5,8,'couponcode='+couponcode_x,function(v) {
				//g_js.windows.mask.busy.close();
				var o = g_js.json.get(v); 
				if (o.error != ''){
					$.jq_alert(o.error);
				}else{
					js_cart.recalculate();
				}
				//$("#id_order_total").html(o.result);
			});
		}
	},
	ini:function(){
		js_cart.ini_car_box();
	}
};
(function(){js_cart.ini();})();