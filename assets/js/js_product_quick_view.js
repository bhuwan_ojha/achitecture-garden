var js_product_quick_view={
	add_to_wishlist:function(wid,obj){
		if($(obj).hasClass('selected')) return;

		/* is login ?
		 *
		 */
		if(0==js_get_user_id()){
			//$.jq_alert(js_language.cst_login_first	);
			js_user.login(window.location.href);
			return;
		}

		/* check product type
		 *
		 */
		var v_n_product_type		= $('#id_qv_p_type').val()				;	/* product type */
		var v_n_parent_product_id	= this.optionsx.m_n_parent_product_id	;	/* parent id	*/
		var v_n_child_product_id	= this.optionsx.m_n_child_product_id	;	/* childe id	*/
		var v_p_id					= 0										;
		if( 0==v_n_product_type			){	v_p_id	= v_n_parent_product_id	;}	/* single prodcut */
		else if (1==v_n_product_type	){	v_p_id	= v_n_child_product_id	;}	/* parent product */
		else{}

		/* validate product id
		 *
		 */
		if( v_p_id>0 ){}else{$.jq_alert(js_language.cst_please_choose_product_otions);return;}

		/* add product to wishlist
		 *
		 */
		//js_user.wish_list.wl_add_product_to_wishlist(v_p_id);
		js_user.wish_list.wl_save(v_p_id,wid);
		$(obj).addClass('selected');
	}
	,
	cart:null
	,
	add_to_cart:function(){this.cart.add1($('#id_qv_p_qty').val(),$('#id_qv_p_type').val(),this.optionsx.m_n_parent_product_id,this.optionsx.m_n_child_product_id)}
	,
	optionsx:null
	,
	show:function(v_product_id){g_js.js.ajax.fg.get(1,3,'pid='+v_product_id,function(v){g_js.windows.html.create_popup(v,function(){
		$('.wish_heart').hover(function(){$(this).addClass('hover').parent().find('.wish_tips').fadeIn();},function(){$(this).removeClass('hover').parent().find('.wish_tips').slideUp();});

		js_product_quick_view.optionsx	= new c_product_options_x();
		js_product_quick_view.optionsx.ini(v_product_id,$('#id_options_max_index').html(),function(o){
			
			$('#id_qv_p_name')						.html(o.products_name)					;	/* product name										*/
			$('#id_qv_p_short_description')			.html(o.products_shortdescription)		;	/* product seo and short description				*/
			$('#id_qv_p_sku')						.html(o.products_sku)					;	/* product sku										*/
			$('#id_qv_p_stock')						.html(o.child_product_stock)			;	/* product stock status								*/
			$('#id_qv_p_sale_price')				.html(o.child_product_price)			;	/* product child product price						*/
			$('#id_qv_p_list_price_and_you_save')	.html(o.child_product_price_list)		;	/* product child product price for list and save	*/

		});
		js_product_quick_view.pic		= new c_product_options_pic();
		js_product_quick_view.pic.ini('product_thumbnail_for_qv',js_product_quick_view.optionsx,'id_product_picture_quick_view');
		js_product_quick_view.cart		= new c_cart();js_product_quick_view.cart.ini();
	},790,650,true,null,null,null,true,0.7);});}
	,
	ini:function(){}
	,
	pic:null	
};$().ready( function (){js_product_quick_view.ini();});