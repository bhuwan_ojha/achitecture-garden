var menu_showtime = 500;
var trigger2;
function checkfield(value,r,msg_id){
    var res = new Array();
    res['email'] = /^(?:[a-z0-9]+[_\-+.]?)*[a-z0-9]+@(?:([a-z0-9]+-?)*[a-z0-9]+\.)+([a-z]{2,})+$/i;
    res['password'] = /^[0-9a-zA-Z]{1,16}$/;
    res['require'] = /^[\s\S]+$/;


    var rule = res[r];

    if(r=='pwdmatch'){
        if($('#password').val()!=value){
            setmsg(msg_id,'error');
            return false;
        }else{
            setmsg(msg_id,'success');
            return true;
        }
    }else{
        var Expression=rule;
        var objExp=new RegExp(Expression);
        if(objExp.test(value)==true){
            setmsg(msg_id,'success');
            return true;
        }else{
            setmsg(msg_id,'error');
            return false;
        }
    }
}


function setmsg(msg_id,type){
    if(type=='error'){
        $('#'+msg_id).removeClass('confirmed');
        $('#'+msg_id).addClass('error');
    }else if(type=='success'){
        $('#'+msg_id).removeClass('error');
        $('#'+msg_id).addClass('confirmed');
    }

}


var js_header={
	add_row:function(){
		var item_length = $(".nile_item_no").length;
		$("#item_order_table_new").append('<tr><td valign="top"><input type="text" name="item_no" class="nile_item_no"  placeholder ="Item Number" id="id_nile_item_no_'+(item_length+1)+'"></td>'
		+'<td valign="top"><div align="justify"><input name="qty" type="text" id="id_nile_item_qty" class="nile_qty" size="3" placeholder ="Qty"></div></td>'
		+'<td valign="top"><div class="nile_item_des" id="id_item_des_'+(item_length+1)+'"></div></td>'
		+'<td>&nbsp;</td><td valign="top"><input type="text" name="item_no" class="nile_item_no"  placeholder ="Item Number" id="id_nile_item_no_'+(item_length+2)+'"></td>'
		+'<td valign="top"><div align="justify"><input name="qty" type="text" id="id_nile_item_qty" class="nile_qty" size="3" placeholder ="Qty"></div></td>'
		+'<td valign="top"><div class="nile_item_des" id="id_item_des_'+(item_length+2)+'"></div></td></tr>');
	},
	change_currency:function(currency){
		g_js.js.ajax.fg.get(4,0,'currency='+currency,function(v){
			window.location.href = window.location.href;
		});
	},
	change_language:function(language){
		g_js.js.ajax.fg.get(4,0,'language='+language,function(v){
			window.location.href = window.location.href;
		});
	},
	send_contactus:function(id){
		
		var is_error = 0;
		$.jq_remove_error();
		
		if($('input[name="name"]').val()==''){
			$.jq_field_error($('input[name="name"]'), 'This is a required field');
			is_error = 1;
		}
		if($('input[name="email"]').val()==''){
			$.jq_field_error($('input[name="email"]'), 'This is a required field');
			is_error = 1;
		}
		if($('input[name="conpany"]').val()==''){
			$.jq_field_error($('input[name="conpany"]'), 'This is a required field');
			is_error = 1;
		}
		if($('input[name="phone"]').val()==''){
			$.jq_field_error($('input[name="phone"]'), 'This is a required field');
			is_error = 1;
		}
		if($('textarea[name="enquiry"]').val()==''){
			$.jq_field_error($('textarea[name="enquiry"]'), 'This is a required field');
			is_error = 1;
		}
		
		if(is_error==1){
			return false;
		}
		
		//g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(11,2,"#id_contact_us_form","",function(v){
			var o = g_js.json.get(v); 
			//g_js.windows.mask.busy.close();
			if (o.error == 1){
				$.jq_field_error($(o.type+'[name="'+o.field+'"]'), o.result);
				
				return false;
			} else {
				$.jq_remove_error();
				document.getElementById('id_contact_us_form').reset();
				alert('Your message was sent successfully. Thank you!');
				//alert(o.result,'js_header.refresh');				
			}
		});		
	},
	send_rma:function(id){
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(11,3,"#id_rma_form","",function(v){
			var o = g_js.json.get(v); 
			g_js.windows.mask.busy.close();
			if (o.error == 1){
				$.jq_alert(o.result);
				return false;
			} else {
				$.jq_alert(o.result,'js_header.refresh');				
			}
		});		
	},
	send_contactus_bak:function(id){
		if(id==2){
			var firstname = $("#id_contact_us_firstname").val();
			var lastname = $("#id_contact_us_lastname").val();
			var email = $("#id_contact_us_email").val();
			var enquire = $("#id_contact_us_value").val();
			if (firstname.replace(/ /g,'')==''){
				$.jq_alert(js_language.cst_contact_us_firstname);
				return false;
			}
			if (lastname.replace(/ /g,'')==''){
				$.jq_alert(js_language.cst_contact_us_lastname);
				return false;
			}
		}else{
			var name = $("#id_contact_us_name").val();
			var email = $("#id_contact_us_email").val();
			var enquire = $("#id_contact_us_value").val();
			if (name.replace(/ /g,'')==''){
				$.jq_alert(js_language.cst_contact_us_name);
				return false;
			}
		}

		if (email.replace(/ /g,'')==''){
			$.jq_alert(js_language.cst_contact_us_email);
			return false;
		}
		
		if (enquire.replace(/ /g,'')==''){
			$.jq_alert(js_language.cst_contact_us_enquire);
			return false;
		}
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(11,2,"#id_contact_us_form","",function(v){
			var o = g_js.json.get(v); 
			g_js.windows.mask.busy.close();
			if (o.error == 1){
				$.jq_alert(o.result);
				return false;
			} else {
				$.jq_alert(o.result,'js_header.refresh');				
			}
		});		
	},
	refresh:function(){
		window.location.href = window.location.href;
	},
	deals_alert_ini:function(){
		$(".deals_alert").hover(
			function(){			
				$(".deals_alert_popup:not(:animated)").slideDown("fast");
			},
			function(){
				$(".deals_alert_popup").slideUp("fast");
			}
		);		
	},
	main_menu_ini:function(){
		
		$('.hover_menu').hover(
				function(){
					setTimeout(function(){
						menu_showtime = 1;
				    },500);
				},
				function(){
					menu_showtime = 500;
				}
		);

		$('.menu .item').hover(
			function(){
                if($(window).width()<1000){
                    return;
                }
				$(this).addClass('hover');
				var _this = $(this); 
				trigger = setTimeout(function(){
			        _this.find('.cate_dropdown:not(:animated)').slideDown('fast');
			    },menu_showtime);
				
				if(trigger2!=undefined){
					clearTimeout(trigger2);
				}
				
			},
			function(){
				clearTimeout(trigger);
				$(this).removeClass('hover');
				$(this).find('.cate_dropdown').slideUp('fast');
				trigger2 = setTimeout(function(){
					menu_showtime = 500;
			    },500);
			}
		);
	},
	subscribe_check_email:function(param){
		$.jq_remove_error();
		var sub_email = $("#sub_email").val();
		$("#sub_delemail").val(param);
		var sub_delemail = $("#sub_delemail").val();
		if ( $.trim(sub_email).length==0 ){
			alert(js_language.cst_please_enter_email_address) ;
			return false ;
		}
		g_js.js.ajax.fg.post(4,0,"#form_sub","",function(v){
			
			var o = g_js.json.get(v);
			if (o.error == 1){
				$.jq_field_error($('input[name="'+o.field+'"]'), o.result);
			}else{
				$("#sub_email").val('Enter Your Email Address');
				alert('Thank you for subscribing to Treasure Garden’s Newsletter!');
			}
		});
	},
	subscribe_check_email_landing:function(param){
		var sub_email = $("#sub_email_landing").val();
		$("#sub_delemail_landing").val(param);
		var sub_delemail = $("#sub_delemail_landing").val();
		if ( $.trim(sub_email).length==0 ){			
			$.jq_alert(js_language.cst_please_enter_email_address) ;
			return false ;
		}
		g_js.js.ajax.fg.post(4,10,"#form_sub_landing","",function(v){
			$.jq_alert(v);
			if(v=="Thank you for subscribing to our newsletter!"){
				g_js.windows.html.close_popup();
			}
		});
	},
	recently_viewed_ini:function(){
		$(".recently_viewed_t,.recently_viewed_b").click(function(){
			var h = $(".recently_viewed_e").height()+2;
			if($(".recently_viewed_b.collapse").length>0){
				$(".recently_viewed_t").next(":not(:animated)").animate({top: '+='+h}, "normal",function(){
					$(".recently_viewed_b").removeClass("collapse");
				});
			}else{				
				$(".recently_viewed_t").next(":not(:animated)").animate({top: '-='+h}, "normal",function(){
					$(".recently_viewed_b").addClass("collapse");
				});
			}
		});
	},
	track_order_ini:function(){
		$(".submit_sub").click(function(){
			js_header.subscribe_check_email(0);
		});
		$("#sub_email").focus(function(){
			var v = $('#sub_email').val();
			if(v=='Enter Email Address'){
				$('#sub_email').val('');
			}
		});
		$("#sub_email").blur(function(){
			var v = $('#sub_email').val();
			if($.trim(v)==''){
				$('#sub_email').val('Enter Email Address');
			}
		});

//		$(".submit_sub_landing").click(function(){
//			js_header.subscribe_check_email_landing(0);
//		});
//		$("#sub_email_landing").focus(function(){
//			var v = $('#sub_email_landing').val();
//			if(v=='Enter Email Address'){
//				$('#sub_email_landing').val('');
//			}
//		});
//		$("#sub_email_landing").blur(function(){
//			var v = $('#sub_email_landing').val();
//			if($.trim(v)==''){
//				$('#sub_email_landing').val('Enter Email Address');
//			}
//		});

		/*
		$(".submit_tyo").click(function(){
			$('#tyo_form').submit();
		});
		$(".tyo_close").click(function(){
			$(".tyo").fadeOut();
			$(".top_menu_tyo").removeClass("clicked");
		});
		$(".top_menu_tyo").click(function(){
			$(this).blur();
			$(".tyo").fadeIn();
			$(this).addClass("clicked");
		});*/
	},
	scroll_cart_box:function(v){
		var abslt_top = 0;//弹出框的top
		var top_link_cart = -35;//弹出框的容器与浏览器顶部的距离
		if(v<abslt_top){
			var h=abslt_top;
		}else{
			var h=v-top_link_cart;
		}
		$('.sc').animate({
			top: h
		}, 0);
		return false;
	},
	scroll_header_menu:function(c_top, top){
		if(c_top>top){
			$('.scroll_menu').addClass('scroll');
			$('.content').css({'margin-top':'85px'});
		}else{
			$('.scroll_menu').removeClass('scroll');
			$('.content').css({'margin-top':'0px'});
		}
	},
	category_slider_ini:function(){
		var scroll_width = 890;
		$(".prev_page_bt").click(function(){
			var also_like_pro = $(this).parent().find(".also_like_pro");
			if(also_like_pro.attr("rvi") == "1"){	return false;}
			also_like_pro.find(".also_like_pro_content:not(:animated)").animate({marginLeft: '+='+scroll_width}, "slow",function(){
				also_like_pro.attr("rvi",parseInt(also_like_pro.attr("rvi")) - 1);
			});
		});
		$(".next_page_bt").click(function(){
			var also_like_pro = $(this).parent().find(".also_like_pro");
			var rvi_content = parseInt(also_like_pro.attr("rvi"));
			var rvc_content = parseInt(also_like_pro.attr("rvc"));
			if( rvi_content == rvc_content){return false;}
			also_like_pro.find(".also_like_pro_content:not(:animated)").animate({marginLeft: '-='+scroll_width}, "slow",function(){also_like_pro.attr("rvi",rvi_content+1);});
		});
//		$(".prev_page_bt").click(function(){if($(".also_like_pro").attr("rvi") == "1"){	return false;}
//		$(".also_like_pro_content:not(:animated)").animate({marginLeft: '+='+scroll_width}, "slow",function(){
//			$(".also_like_pro").attr("rvi",parseInt($(".also_like_pro").attr("rvi")) - 1);
//		});});
//		$(".next_page_bt").click(function(){
//
//			var rvi_content = parseInt($(".also_like_pro").attr("rvi"));
//			var rvc_content = parseInt($(".also_like_pro").attr("rvc"));
//			if( rvi_content == rvc_content){return false;}
//			$(".also_like_pro_content:not(:animated)").animate({marginLeft: '-='+scroll_width}, "slow",function(){$(".also_like_pro").attr("rvi",rvi_content+1);});
//		});
	},
	clear:function(){
		$('#id_contact_us_form input,#id_contact_us_form textarea').val('');
	},
	teamss_get_by_cud:function(cid,subcid,sort_by,per_page,page){
		var keywords = $("#teamss_keywords").val();
		var lsearch = $("#teamss_articles_list").attr("lsearch");
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.get(11,3,"&cid="+cid+"&subcid="+subcid+"&sort_by="+sort_by+"&page="+page+"&per_page="+per_page+"&keywords="+keywords+"&lsearch="+lsearch,function(v) {
			$("#teamss_articles_list").html(v);
			g_js.windows.mask.busy.close();
		});
		
	},
	change_page:function(page){
		var page = $(page).attr("p");
		var pvid = $("#teamss_articles_list").attr("pvid");
		var lvid = $("#teamss_articles_list").attr("lvid");
		var sort_by = $("#sort_by").val();
		var per_page = $("#per_page").val();
		js_header.teamss_get_by_cud(pvid,lvid,sort_by,per_page,page);
	},
	ini:function(){
		$(".teamss_sub_title").click(function(index){
			var clvid = $(this).attr("lvid");
			$('.teamss_sub_title').removeClass("selected");
			
			$(this).addClass("selected");
			
			if($(this).hasClass('has_sub')){
				$(this).parent().toggleClass('ext');
				return false;
			}
			
			$("#teamss_articles_list").attr("pvid",$(this).attr("pvid"));
			$("#teamss_articles_list").attr("lvid",$(this).attr("lvid"))
			var sort_by = $("#sort_by").val();
			var per_page = $("#per_page").val();
			js_header.teamss_get_by_cud($(this).attr("pvid"),$(this).attr("lvid"),sort_by,per_page,0);
			
			$(".1teamss_sub_title").each(function(index2,elm2){
				if($(this).attr("lvid") == clvid){
					$(this).addClass("selected");
					
					if($(this).hasClass('has_sub')){
						$(this).parent().toggleClass('ext');
						return false;
					}
					
					$("#teamss_articles_list").attr("pvid",$(this).attr("pvid"));
					$("#teamss_articles_list").attr("lvid",$(this).attr("lvid"))
					var sort_by = $("#sort_by").val();
					var per_page = $("#per_page").val();
					js_header.teamss_get_by_cud($(this).attr("pvid"),$(this).attr("lvid"),sort_by,per_page,0);
				}
				else{
					$(this).removeClass("selected");
				}
			});
		});
		$(".cis_select_options").live("change",function(){
			var pvid = $("#teamss_articles_list").attr("pvid");
			var lvid = $("#teamss_articles_list").attr("lvid");
			var sort_by = $("#sort_by").val();
			var per_page = $("#per_page").val();
			js_header.teamss_get_by_cud(pvid,lvid,sort_by,per_page,0);
		});
		$('.header_menu_item.first').hover(function(){
			$('.header_menu_item_dropdown').show();
		},function(){
			$('.header_menu_item_dropdown').hide();
		});
		$('.header_menu_item_dropdown').hover(function(){
			$(this).show();
			$('.header_menu_item.first').addClass('selected');
		},function(){
			$(this).hide();
			$('.header_menu_item.first').removeClass('selected');
		});
		$('.left1 .item').hover(function(){
			var id=$(this).attr('id');
			$('#des_'+id).show();
			$(this).addClass("selected");
		},function(){
			var id=$(this).attr('id');
			$('#des_'+id).hide();
			$(this).removeClass("selected");
		});
		$('.left2 .item').hover(function(){
			var id=$(this).attr('id');
			id=id.replace('des_','');
			$(this).show();
			$('#'+id).addClass("selected");
		},function(){
			var id=$(this).attr('id');
			id=id.replace('des_','');
			$(this).hide();
			$('#'+id).removeClass("selected");
		});

		if($("#recently_viewed").length>0){
			var right_offset = $("div.content").offset().left+$("div.content").width()+57;
			$("#recently_viewed").css({"top":0,"left":right_offset});
			js_header.recently_viewed_ini();
		}
		$("#dp_category").change(function(){
			$('#cpath').val($(this).val());
			$('#ctitle').val($.trim($("#dp_category option:selected").text()));
		});
		$(".top_search_keyword input").focus(function(){
			if($.trim($(this).val())==$(this).attr("rv")){
				$(this).val("");
			}
		});
		$(".top_search_keyword input").blur(function(){
			if($.trim($(this).val())==""){
				$(this).val($(this).attr("rv"));
			}
		});
		$(".top_shopping_bag").hover(
			function(){$(this).addClass("hover");},
			function(){$(this).removeClass("hover");}
		);
		js_header.track_order_ini();
		js_header.main_menu_ini();
		//js_header.deals_alert_ini();
		js_header.category_slider_ini();


		/* by breezer,2012.01.18
		 *
		 */
        $(window.document).ready(function () {
            $('#recently_viewed').show('fast');

            setTimeout(function(){
            	$('.msdropdown').each(function(){
            		$(this).msDropdown();
            	});
            	
            }, 200);

            $('.res_icon').click(function(){
                if($('.menu_res').hasClass('res_show')){
                    $('.menu_res').removeClass('res_show');
                }else{
                    $('.menu_res').addClass('res_show');
                }
            });
            
            

//            $('.search_link').click(function(){
//                $('.menu li a').css('padding','0 12px');
//                $(this).hide();
//
//                setTimeout(function(){$('.search').fadeIn();}, 200);
//
//            });

//            $('body').bind('click',function(e) {
//                if(!$(e.target).is('.search_link')&& !$(e.target).is('#keyword') && !$(e.target).is('#id_search_go')){
//                    $('.search').hide();
//                    $('.search_link').fadeIn();
//                    $('.m_link').css('padding','0 20px');
//                }
//            });

            $(function() {
                $(".lazyimg").lazyload({
                    effect:"fadeIn",
                });
            })

            $('.logo_item').hover(
                function(){
                    $(this).css('z-index','100');
                },
                function(){
                    $(this).css('z-index','0');
                }
            );
            
            /*
            $('.product_items_grid').hover(
    	            function(){
    	                $(this).css('z-index','100');
    	                
    	                $(this).find('.product_option_button').addClass('shadow');
    	                $(this).find('.product_option_button').stop().animate({
    	                    bottom:'-39px',
    	                    opacity:'1'
    	                  },500,function(){
    	                	  //$(this).css('z-index','100');
    	                  });
    	                
    	            },
    	            function(){
    	                $(this).css('z-index','0');
    	                $(this).find('.product_option_button').removeClass('shadow');
    	                $(this).find('.product_option_button').stop().animate({
    	                    bottom:'10px',
    	                    opacity:'1'
    	                  },500,function(){
    	                	  //$(this).css('z-index','100');
    	                  });
    	            }
    	   );
		   */
	        $('.product_items_grid').hover(
    	            function(){
    	                //$(this).find('.more_description').show();
    	                
    	            },
    	            function(){
    	                //$(this).find('.more_description').hide();
    	            }
    		);


        });
        
        $(document).ready(function(){

            $('.pholder').click(function(){
                $(this).hide();
                $(this).next().focus();
            });

            $('input, textarea').each(function(){
                pholder = $(this).prev();
                if($(this).val()==''&&pholder.hasClass('pholder')){
                    pholder.show();
                }
            });


            $('input, textarea').focusin(function() {
                pholder = $(this).prev();
                if(pholder.hasClass('pholder')){
                    pholder.hide();
                }
            });
            $('input, textarea').focusout(function() {
                data_rule = $(this).attr('data-rule');
                msg_id = $(this).attr('msg_id');
                if(data_rule!=''){
                    checkfield($(this).val(),data_rule,msg_id);
                }
                if($(this).val()!=''){
                    return;
                }
                pholder = $(this).prev();
                if(pholder.hasClass('pholder')){
                    pholder.show();
                }
            });

            $(".social_box").hover(
            	function(){
            		var _this = $(this); 
            		trigger = setTimeout(function(){
//            			_this.css({"z-index":999});
    			        _this.animate({
    	            	    right:'0px',
    	            	  });
    			    },500);
            	  
            	},
            	function(){
            		clearTimeout(trigger);
//            		$(this).css({"z-index":999});
              	  	$(this).animate({
              	  		right:'-240px',
              	  	});
              	}
            ); 
            
    		
//    		$(window).resize(function(){
//    			js_category.setlastgriditem();
//    		});
//    		
//    		setTimeout(function(){
//    			js_category.setlastgriditem();
//		    },500);

        });
        
		$(window).scroll(function () {
			//js_header.scroll_cart_box($(this).scrollTop());
		});	
		
		$('#login_password').keydown(function(e){
			if(e.keyCode==13){
				$('.btn_login').trigger('click');
			}
		});
		
		
		$('#id_new_user_account_reppwd').keydown(function(e){
			if(e.keyCode==13){
				$('.btn_reg').trigger('click');
			}
		});
		
		$('#sub_email').keydown(function(e){
			if(e.keyCode==13){
				$('.submit_sub').trigger('click');
			}
		});
		
		$('#id_create_anccount_for_guest').keydown(function(e){
			if(e.keyCode==13){
				$('.btn_guest').trigger('click');
				return;
			}
		});
		
		
		
	}// end of function int()	
};(function(){js_header.ini();})();


//var scrolltotop={setting:{startline:10,scrollto:0,scrollduration:1000,fadeduration:[500,100]},controlHTML:'<span style="color:#fff;" id="takeMeUp">Back to top</span>',controlattrs:{offsetx:3,offsety:25},anchorkeyword:"#top",state:{isvisible:false,shouldvisible:false},scrollup:function(){if(!this.cssfixedsupport){this.$control.css({opacity:0})}var a=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);if(typeof a=="string"&&jQuery("#"+a).length==1){a=jQuery("#"+a).offset().top}else{a=0}this.$body.animate({scrollTop:a},this.setting.scrollduration)},keepfixed:function(){var c=jQuery(window);var b=c.scrollLeft()+c.width()-this.$control.width()-this.controlattrs.offsetx;var a=c.scrollTop()+c.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:b+"px",top:a+"px"})},togglecontrol:function(){var a=jQuery(window).scrollTop();if(!this.cssfixedsupport){this.keepfixed()}this.state.shouldvisible=(a>=this.setting.startline)?true:false;if(this.state.shouldvisible&&!this.state.isvisible){this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]);this.state.isvisible=true}else{if(this.state.shouldvisible==false&&this.state.isvisible){this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]);this.state.isvisible=false}}},init:function(){jQuery(document).ready(function(c){var a=scrolltotop;var b=document.all;a.cssfixedsupport=!b||b&&document.compatMode=="CSS1Compat"&&window.XMLHttpRequest;a.$body=(window.opera)?(document.compatMode=="CSS1Compat"?c("html"):c("body")):c("html,body");a.$control=c('<div id="topcontrol">'+a.controlHTML+"</div>").css({position:a.cssfixedsupport?"fixed":"absolute",bottom:a.controlattrs.offsety,right:a.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll Back to Top"}).click(function(){a.scrollup();return false}).appendTo("body");if(document.all&&!window.XMLHttpRequest&&a.$control.text()!=""){a.$control.css({width:a.$control.width()})}a.togglecontrol();c('a[href="'+a.anchorkeyword+'"]').click(function(){a.scrollup();return false});c(window).bind("scroll resize",function(d){a.togglecontrol()})})}};scrolltotop.init();
