/* js core code be processed for jQuery extension , written by Breezer,2011.12.08
 * 
 * All rights reserved Ruiit.com Co., Ltd. Chengdu,Sichuan
 * 
 */

$.extend({
	jq_alert:function(msg,callback,width,height) {
		if(js_is_null(width)) width = 400;
		if(js_is_null(height)) height = 100;
		if(js_is_null(callback)) callback = '';
		g_js.windows.html.create_popup_small('<div style="text-align:center;font-size:14px;min-height:50px;">'+msg+'</div>',function(){
		},width,height,true,null,null,null,true,0.7,callback);
		
	},
	jq_confirm:function(msg,width,height,yes_callback,no_callback){
		if(js_is_null(width)) width = 400;
		if(js_is_null(height)) height = 140;
		if(js_is_null(yes_callback)) yes_callback = '';
		if(js_is_null(no_callback)) no_callback = '';
		g_js.windows.html.create_popup_small_confirm('<div style="text-align:center;min-height:50px;">'+msg+'</div>',function(){
		},width,height,true,null,null,null,false,0.7,yes_callback,no_callback);
	},
	jq_remove_error:function(){
		$('.field_error').removeClass('field_error');
		$('.error_msg').remove();
	},
	jq_field_error:function(obj, msg){
		obj.addClass('field_error');
		obj.parent().find(".error_msg").remove();
		obj.after('<span class="error_msg">'+msg+'</span>');
		$('.field_error:first').focus();
        $('.error_msg').fadeOut(5000);
	}
});
$.fn.extend ({
				fe_get_object_size	: function (){

					/* all width and height properties give 0 on elements with display none,so enable the element temporarily
 					 *
					 */
					var element				= this.get(0)			;
					var els					= element.style			;
					var originalVisibility	= els.visibility		;
					var originalPosition	= els.position			;
					var originalDisplay		= els.display			;
					els.visibility			= 'hidden'				;
					els.position			= 'absolute'			;
					els.display				= 'block'				;
					var originalWidth		= element.clientWidth	; 
					var originalHeight		= element.clientHeight	;  
					els.display				= originalDisplay		;
					els.position			= originalPosition		;
					els.visibility			= originalVisibility	;	
					return{width:originalWidth,height:originalHeight};		 
				}
				,
				fe_get_object_absolute_position:function(){

					/* get dom element absolute position relative browser left corner	
					 *
					 */
					var v_o=this.get(0);var pt={x:0,y:0,left:0,top:0};do{pt.x+=v_o.offsetLeft;pt.y += v_o.offsetTop;v_o=v_o.offsetParent;}while(v_o);pt.left=pt.x;pt.top=pt.y;return pt;
				}
				,
				js_ex:function(){var v_this=this;return{
						anx:{
							hide:{
								scroll:{
									up:function(){$(v_this).slideUp('fast');return $(v_this);}
								}
								,
								ex:function(){return this.scroll.up();}
							}
							,
							show:{							
								scroll:{
									down	:function(){$(v_this).slideDown('fast');return $(v_this);}
									,
									left	:function(){$(v_this).show('slide',{ direction: 'left' }, 300);return $(v_this);}
									,
									right	:function(){$(v_this).show('slide',{ direction: 'right' }, 300);return $(v_this);}
								}
								,
								to_left		:function(v_pos){$(v_this).animate({'margin-left'	:v_pos+'px'});return $(v_this);}
								,
								to_right	:function(v_pos){$(v_this).animate({'margin-right'	:v_pos+'px'});return $(v_this);}
								,
								ex			:function(){return this.scroll.right();}
							}
						}
					}
				}
			});

function js_get_object_ex(v_id,o_window)	{return o_window.document.getElementById(v_id)	;}
function js_get_object(v_id)				{return js_get_object_ex(v_id,window)			;} 
function js_show_div(v_o)					{v_o.style.display = 'block'					;}
function js_hide_div(v_o)					{v_o.style.display ='none'						;}
function js_url_encoder(v)					{return encodeURIComponent(v)					;}
function ajax_ex(v_url,o_callback,v_timeout){if(js_is_null(v_timeout)){v_timeout = 120000;};$.ajax({
			url		: v_url
		,	cache	: false
		,	success	: function( v_html ){o_callback($.trim(v_html));}
		,	timeout : v_timeout
		,	complete: function( v_xml_http_request,v_text_status){}
	});
}
function ajax_ex_get_json (v_url,o_callback){
	$.ajax	(
				{
						url			: v_url
		            ,   beforeSend	: function(x) {x.setRequestHeader("Content-Type", "application/json; charset=utf-8"); }
					,   dataType	: 'json'
					,	cache		: false
					,	success		: function( v_html ){o_callback($.trim(v_html));}
					,	complete	: function( v_xml_http_request,v_text_status){}
				}
			);
}
function ajax_ex_get (v_url,o_callback,v_timeout){return ajax_ex(v_url,o_callback,v_timeout);}
function ajax_ex_post(v_url,v_data,o_callback){

	var v_data_x='';for(var i=0;i<v_data.length;i++){v_data_x += v_data[i][0] + '=' +js_url_encoder(v_data[i][1])+ '&';}
  	$.ajax	(
				{
						url		: v_url
					,	type	: 'POST'
					,	data	: v_data_x
					,	cache	: false
					,	success	: function( v_html ){o_callback($.trim(v_html));}
					,	complete: function( v_xml_http_request,v_text_status){if( 'error'==v_text_status ){$.jq_alert('Ajax post call ,error');}}
				}
			);
}
function js_get_val(o)				{return o.value						;}
function js_get_html(o)				{return o.innerHTML					;}
function js_get_string_spliter()	{return g_js_string_spliter			;}
function js_is_null(v)				{return ((undefined==v)||(null==v)||(''==v))?true:false;}
function js_validate_mail(v)		{

	if (v ==''){$.jq_alert(js_language.cst_email_validate_error_1);return false;}	
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
	for (i=0; i<invalidChars.length; i++) {
		if (v.indexOf(invalidChars.charAt(i),0) > -1) {
			$.jq_alert(js_language.cst_email_validate_error_2);
			return false;
		}
	}
	
	for (i=0; i<v.length; i++) {
		if (v.charCodeAt(i)>127) {
			$.jq_alert(js_language.cst_email_validate_error_3);
			return false;
		}
	}

	var atPos = v.indexOf('@',0);
	if (atPos == -1) {
		$.jq_alert(js_language.cst_email_validate_error_4);
		return false;
	}
	if (atPos == 0) {
		$.jq_alert(js_language.cst_email_validate_error_5);
		return false;
	}
	
	if (v.indexOf('@', atPos + 1) > - 1) {
		$.jq_alert(js_language.cst_email_validate_error_6);
		return false;
	}
	if (v.indexOf('.', atPos) == -1) {
		$.jq_alert(js_language.cst_email_validate_error_7);
		return false;
	}
	if (v.indexOf('@.',0) != -1) {
		$.jq_alert(js_language.cst_email_validate_error_8);
		return false;
	}
	if (v.indexOf('.@',0) != -1){
		$.jq_alert(js_language.cst_email_validate_error_9);
		return false;
	}
	if (v.indexOf('..',0) != -1) {
		$.jq_alert(js_language.cst_email_validate_error_10);
		return false;
	}
	var suffix = v.substring(v.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
		$.jq_alert(js_language.cst_email_validate_error_11);
		return false;
	}
	return true;
}
function js_div_class(o,v_class)	{$(o).get(0).className	= v_class	;}
function js_div_align_center(o)		{
	$(o).css({	
				'left'		:	((window.document.body.clientWidth/2)-($(o).fe_get_object_size().width/2))+'px'
				,
				'top'		:	((window.screen.availHeight/2) - ($(o).fe_get_object_size().height/2))+'px'
			});
}
function js_div_move(o,v_l,v_t,v_w,v_h){
	if(	v_l>0)$(o).get(0).style.left	= v_l+'px';
	if( v_t>0)$(o).get(0).style.top		= v_t+'px';
	if( v_w>0)$(o).get(0).style.width	= v_w+'px';
	if( v_h>0)$(o).get(0).style.height	= v_h+'px';
}
function js_time_countdown(v_time_start,v_time_end){

	if(v_time_start==''||v_time_end=='')return '';
	
	var o_time_start	= new Date(v_time_start);		/* start time		*/
	var o_time_end		= new Date(v_time_end);			/* end time			*/
	var o_time_diff		= o_time_end - o_time_start		/* time diff span	*/

	/* total seconds	
	 *
	 */
	var v_seconds	= o_time_diff/1000;

	/* day
	 *
	 */
	var v_ar_d		= (v_seconds/(60*60*24)).toString().split('.');
	var v_day		= '<span>'+v_ar_d[0]+'</span>Day';

	/* hour
	 *
	 */
	v_ar_d			= (('0.'+v_ar_d[1])*24).toString().split('.');
	var v_hour		= '<span>'+v_ar_d[0]+'</span>Hour';

	/* minute
	 *
	 */
	v_ar_d			= (('0.'+v_ar_d[1])*60).toString().split('.');
	var v_minute	= '<span>'+v_ar_d[0]+'</span>Minute';
 
	/* second
	 *
	 */
	v_ar_d			= (('0.'+v_ar_d[1])*60).toString();
	var v_second	= '<span>'+Math.round (v_ar_d.toString())+'</span>Second';
	
	return '<div class=time_countdown_format>'+v_day+v_hour+v_minute+v_second+'</div>';
}

/* get or set ckeditor value
 *
 */
function js_get_ckeditor_data(v_id	){	$('#'+v_id).val( eval('CKEDITOR.instances.'+v_id+'.getData()') ); return $('#'+v_id).val()	;}
function js_set_ckeditor_data(v_id,v){ 	var o = $( eval('CKEDITOR.instances.'+v_id))					; o.get(0).setData(v)		;}

/* get user id
 *
 */
function js_get_user_id()	{ return g_user_id			;	}
function js_go_to(v)		{ window.location.href = v	;	}

/* website directory information
 *
 */
function js_get_dir_for_product(){	return g_js_web_root_dir+'images/products/';	}

/* golbal function and variables
 *
 */
var g_js={

	json:{
		get:function(v,v_debug){
			
			try{return (!(js_is_null(v_debug)?false:v_debug))?($.parseJSON(v)):(v);}catch(ex){$.jq_alert(v);}
		}
	}
	,
	page:{
		is_eq:function(v_page_name){
			
			var v_ar			= window.location.pathname.split('index.html')	;
			var v_path_name		= v_ar [ v_ar.length -1 ]				;			
			return (v_path_name==v_page_name )?true:false				;
		}
	}
	,
	form:{
		select:{attr:function(o_s,v_attr_name){return $(o_s).find("option:selected").attr(v_attr_name);}}
		,
		val_by_name_for_input	:function(v_name,v)	{	this.val_by_name(v_name,v);	}
		,
		val_by_name_for_select	:function(v_name,v)	{	$('select[name="'+v_name+'"]').attr('value',v);}
		,
		val_by_name				:function(v_name,v)	{	$('input[name="'+v_name+'"]').attr('value',v);}
		,
		serialize				:function(v_form_id){	return $(v_form_id).serialize();}
		,
		val_by_name_for_radio	:function(v_name,v)	{	$('input[name="'+v_name+'"]').each(function(i,d){if($(d).val()==v){d.checked = true;}});}
	}
	,
	string:{
		convert_str_2_ar:function(v){	return v.split( g_js.string.spliter() );}
		,
		spliter:function(){	return js_get_string_spliter();	}
	}
	,
	js:{
		d_d_f_a:g_js_web_root_dir+g_js_d_d_f_a+''		
		,		
		debug			:function(v){$(window.document.body).append(v);}
		,
		dispatch		:function(v_class/* class */,v_cmd/* command */){return	this.d_d_f_a+'js_dispatch_for_admin.php?v_class='+v_class+'&v_cmd='+v_cmd;}
		,
		dispatch_ex		:function(v_class/* class */,v_cmd/* command */,v_parameter_extention/* extention paramter */){
			
			return	this.d_d_f_a+'js_dispatch_for_admin.php?v_class='+v_class+'&v_cmd='+v_cmd+'&'+v_parameter_extention;
		}
		,
		dispatch_bg		:function(v_class,v_cmd){ return	this.dispatch(v_class,v_cmd); }
		,
		dispatch_ex_bg	:function(v_class,v_cmd,v_parameter_extention){ return this.dispatch_ex(v_class,v_cmd,v_parameter_extention); }
		,
		dispatch_fg		:function(v_class/* class */,v_cmd/* command */){return	g_js_web_root_dir+'js_dispatch.php?v_class='+v_class+'&v_cmd='+v_cmd;}
		,
		dispatch_ex_fg	:function(v_class/* class */,v_cmd/* command */,v_parameter_extention/* extention paramter */){
			
			return	g_js_web_root_dir+'js_dispatch.php?v_class='+v_class+'&v_cmd='+v_cmd+'&'+v_parameter_extention;
		}
		,
		ajax:{
			fg:{
				get :function(v_class,v_cmd,v_parameter,v_callback_for_get_ok){

					var v_url		= js_is_null( v_parameter)?( g_js.js.dispatch_fg(v_class,v_cmd) ):( g_js.js.dispatch_ex_fg(v_class,v_cmd,v_parameter) );
					g_js.js.ajax.get(v_url,function(v){v_callback_for_get_ok(v);});
				}
				,
				post:function(v_class,v_cmd,v_id_form,v_parameter,v_callback_for_post_ok){
					
					var v_url		= js_is_null( v_parameter)?( g_js.js.dispatch_fg(v_class,v_cmd) ):( g_js.js.dispatch_ex_fg(v_class,v_cmd,v_parameter) );
					var v_form_data = js_is_null( v_id_form  )?( '' ):( $(v_id_form).serialize() );
					g_js.js.ajax.post(v_url,v_form_data,function(v){v_callback_for_post_ok(v);});
				}
			}
			,
			bg:{
				get :function(v_class,v_cmd,v_parameter,v_callback_for_get_ok){
					var v_url = js_is_null( v_parameter)?( g_js.js.dispatch_bg(v_class,v_cmd) ):( g_js.js.dispatch_ex_bg(v_class,v_cmd,v_parameter) );
					g_js.js.ajax.get(v_url,function(v){v_callback_for_get_ok(v);});
				}
				,
				post:function(v_class,v_cmd,v_id_form,v_parameter,v_callback_for_post_ok){
					
					var v_url		= js_is_null( v_parameter)?( g_js.js.dispatch_bg(v_class,v_cmd) ):( g_js.js.dispatch_ex_bg(v_class,v_cmd,v_parameter) );
					var v_form_data = js_is_null( v_id_form  )?( '' ):( $(v_id_form).serialize() );
					g_js.js.ajax.post(v_url,v_form_data,function(v){v_callback_for_post_ok(v);});
				}
			}
			,
			load_js_ex:function(v_js_url){ $.getScript(js_get_web_root_dir()+v_js_url);	}
			,
			load_js	:function(v_ar_file_name,v_js_url){
				
				var v_ar_path_name	= window.location.pathname.split('index.html');
				var v_file_name		= v_ar_path_name[v_ar_path_name.length-1];
				for(var i=0;i<v_ar_file_name.length;i++){if( v_ar_file_name[i] == v_file_name ){$.getScript(js_get_web_root_dir()+v_js_url);}}
			}
			,			
			get:function(v_url,v_callback,v_timeout){ 	ajax_ex_get (v_url,v_callback,v_timeout);	}
			,
			post	:function(v_url,v_data_x,v_callback,v_timeout){
				if(js_is_null(v_timeout)){
					v_timeout = 120000;
				}

				$.ajax	(
							{
									url		: v_url
								,	type	: 'POST'
								,	data	: v_data_x
								,	cache	: false
								,	success	: function( v ){ v_callback($.trim(v)); }
								,	timeout : v_timeout
								,	complete: function( v_xml_http_request,v_text_status){if( 'error'==v_text_status ){/*$.jq_alert('Ajax,error');*/}}
							}
						);
			}
		}
	}
	,
	keyboard:{
		esc		:function(v_o,callback_esc){v_o.keyup	( function (e){if( e.keyCode == 27){callback_esc()	;}})	;}
		,
		enter	:function(v_o,callback_ok){v_o.keypress	( function (e){if( e.keyCode == 13){callback_ok()	;}})	;}
	}
	,
	windows:{		
		mask:{
			busy:{
				close:function(){$('#id_windows_mask_busy_container').hide();}
				,
				show:function(){					
					
						var v_mask_ui	  = 'z-index:99999;width:300px;height:90px;clear:both;margin:0 auto;position:absolute;background:white;text-align:center;padding:11px 0 0 0;';
						var v_mask_style  = 'z-index:9999;position:absolute;left:0;top:0; background:black; filter:alpha(opacity=50); -moz-opacity:0.5; opacity:0.5;';	
						var v_html	 = '	<div id=id_windows_mask_busy_container>';
						v_html		+= '		<div id=id_windows_mask_busy	style="'+v_mask_style+'"	></div>'	;
						v_html		+= '		<div id=id_windows_mask_busy_ui style="'+v_mask_ui+'" class="radius_5 shadow ">';
						v_html		+= '			<img src='+g_js_web_root_dir+'assets/images/loading.gif  border=0><br><br>';
						v_html		+= '			<span style="">Loading, Please wait.</span>';
						v_html		+= '		</div>';
						v_html		+= '	</div>';

						/* append ui
						 *
						 */
						if($('#id_windows_mask_busy').get().length==0){$(window.document.body).append(v_html);}						

						/* adjust mask window size and position 
						 *
						 */
						js_div_move('#id_windows_mask_busy',0,0,$(window.document.body).get(0).offsetWidth,$(document).height());

						/* adjust content ui size and position 
						 * 
						 */
						var v_view_height	= window.document.documentElement.clientHeight==0?window.document.body.clientHeight:window.document.documentElement.clientHeight;
						var v_div_height	= $('#id_windows_mask_busy_ui').fe_get_object_size().height;
						var v_top			= ((v_view_height - v_div_height) / 2) + $(window.document).scrollTop();
						js_div_move	(	
										'#id_windows_mask_busy_ui',
										( $('#id_windows_mask_busy').fe_get_object_size().width - $('#id_windows_mask_busy_ui').fe_get_object_size().width)/2,
										v_top,
										0,
										0
									);
					 /* show it
					  *
					  */
					$('#id_windows_mask_busy_container').show();				 
				}
			}
		}
		,
		html:{
			close_popup:function(v_callback_for_close){$('#id_create_popup').hide('fast');if(!js_is_null(v_callback_for_close)){eval('('+v_callback_for_close+')();');}}
			,
			close_popup_ext:function(v_callback_for_close){$('#id_create_popup').hide('fast');if(!js_is_null(v_callback_for_close)){eval('('+v_callback_for_close+');');}}
			,
			create_popup_small:function(v/* ui */,callback_ini,v_w,v_h,v_force_ini,v_callback_for_close,top,left,show_close_button,opacity,ok_function){
				if($('#id_create_popup').length>0){$('#id_create_popup').remove();}
				if(show_close_button==null){
					show_close_button = true;
				}
				if(js_is_null(opacity)){
					opacity=0.7;
				}
				/* process default value
				 *
				 */
				v_w			= (js_is_null(v_w))?'700px':(v_w+'px');
				v_h			= (js_is_null(v_h))?'400px':(v_h+'px');
				v_force_ini = (js_is_null(v_force_ini))?false:true;
				var v_vm_view_style  = 'z-index:99999;width:'+v_w+';clear:both;margin:0 auto;position:absolute;background:transparent;text-align:left;';
				var v_vm_mask_style  = 'z-index:99998;position:absolute;left:0;top:0; background:#000; filter:alpha(opacity=10); -moz-opacity:'+opacity+'; opacity:'+opacity+';';
				var v_vm_close_style = 'background:url('+g_js_web_root_dir+'images/popup_close.png) no-repeat scroll 0 0 transparent;width:53px;height:9px;position:absolute;cursor:pointer;right:0;top:-13px;';
				if( ($('#id_create_popup').get().length==0)){
					
					/* ini 
					 *
					 */
					var v_html	 = '	<div class="hide_div " id=id_create_popup>';
					v_html		+=			'	<div id=id_create_popup_view_mask style="'+v_vm_mask_style+'"></div>';					
					v_html		+=			'	<div style="'+v_vm_view_style+'" id=id_create_popup_vm_view>';
					if(show_close_button){
						v_html		+=			'<a href="javascript:;" onclick="g_js.windows.html.close_popup('+v_callback_for_close+');" class="fancybox-item fancybox-close" title="Close"></a>			';
					}
					v_html		+=			'<div class="popup_t1"></div><div id="id_create_popup_ex" class="popup_c1" style="height:'+v_h+'"></div><div class="popup_b1"></div>';
					v_html		+=			'	</div>';
					v_html		+=		'</div>';
					$(window.document.body).append( v_html );					
					v_force_ini = true;
				}
				
				/* force ini
				 *
				 */
				if(v_force_ini){				
					
					/* adjust widht and height
					 *
					 */
					$('#id_create_popup_vm_view').css({'width':v_w});

					/* update ui 
					 *
					 */		
					v = v+'<div class="msg_button"><a class="btn_common" style="color:#fff;" href="javascript:g_js.windows.html.close_popup(\''+ok_function+'\');">OK</a></div>';
					
					$('#id_create_popup_ex').html(v).css({'height':v_h});
					 

					/* callback 
					 *
					 */
					(js_is_null(callback_ini))?'':callback_ini();

					/* event for close ui 
					 *
					 */
					g_js.keyboard.esc($(window.document),function(){g_js.windows.html.close_popup(v_callback_for_close);});
				}			

				/* adjust mask window size and position 
				 *
				 */
				js_div_move(
								'#id_create_popup_view_mask',
								0,
								0,
								$(window.document.body).get(0).offsetWidth			/* width */,
								$(document).height()								
							);
				
				/* show ui
				 *
				 */
				$('#id_create_popup').show();

				/* adjust content ui size and position 
				 * 
				 */
				var v_view_height	= window.document.documentElement.clientHeight == 0 ? window.document.body.clientHeight : window.document.documentElement.clientHeight;
				var v_div_height	= $('#id_create_popup_vm_view').fe_get_object_size().height;
				var v_top	= js_is_null(top)?(((v_view_height - v_div_height) / 2) + $(window.document).scrollTop()):(top+$(window.document).scrollTop());
				var v_left = js_is_null(left)?(( $('#id_create_popup_view_mask').fe_get_object_size().width - $('#id_create_popup_vm_view').fe_get_object_size().width)/2):left;

				js_div_move	(	
								'#id_create_popup_vm_view',
								v_left,
								v_top,
								0,
								0
							);
				
			},
			create_popup_small_confirm:function(v/* ui */,callback_ini,v_w,v_h,v_force_ini,v_callback_for_close,top,left,show_close_button,opacity,ok_function,no_function){
				if($('#id_create_popup').length>0){$('#id_create_popup').remove();}
				if(show_close_button==null){
					show_close_button = true;
				}
				if(js_is_null(opacity)){
					opacity=0.7;
				}
				/* process default value
				 *
				 */
				v_w			= (js_is_null(v_w))?'700px':(v_w+'px');
				v_h			= (js_is_null(v_h))?'400px':(v_h+'px');
				v_force_ini = (js_is_null(v_force_ini))?false:true;
				var v_vm_view_style  = 'z-index:100;width:'+v_w+';clear:both;margin:0 auto;position:absolute;text-align:left;';
				var v_vm_mask_style  = 'z-index:99;position:absolute;left:0;top:0; background:#fff; filter:alpha(opacity=10); -moz-opacity:'+opacity+'; opacity:'+opacity+';';
				var v_vm_close_style = 'background:url('+g_js_web_root_dir+'images/popup_close.png) no-repeat scroll 0 0 transparent;width:53px;height:9px;position:absolute;cursor:pointer;right:0;top:-13px;';
				if( ($('#id_create_popup').get().length==0)){
					
					/* ini 
					 *
					 */
					var v_html	 = '	<div class="hide_div " id=id_create_popup>';
					v_html		+=			'	<div id=id_create_popup_view_mask style="'+v_vm_mask_style+'"></div>';					
					v_html		+=			'	<div style="'+v_vm_view_style+'" id=id_create_popup_vm_view>';
					if(show_close_button){
						v_html		+=			'			<div style="'+v_vm_close_style+'" onclick="g_js.windows.html.close_popup('+v_callback_for_close+');"></div>';
					}
					v_html		+=			'<div class="popup_t1"></div><div id="id_create_popup_ex" class="popup_c1" style="height:'+v_h+'"></div><div class="popup_b1"></div>';
					v_html		+=			'	</div>';
					v_html		+=		'</div>';
					$(window.document.body).append( v_html );					
					v_force_ini = true;
				}
				
				/* force ini
				 *
				 */
				if(v_force_ini){				
					
					/* adjust widht and height
					 *
					 */
					$('#id_create_popup_vm_view').css({'width':v_w});

					/* update ui 
					 *
					 */		
					v = v+'<div class="msg_button"><a href="javascript:g_js.windows.html.close_popup_ext(\''+ok_function+'\');"><img src="images/buttons/button_yes.png"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:g_js.windows.html.close_popup_ext(\''+no_function+'\');"><img src="images/buttons/button_no.png"></a></div>';
					
					$('#id_create_popup_ex').html(v).css({'height':v_h});
					 

					/* callback 
					 *
					 */
					(js_is_null(callback_ini))?'':callback_ini();

					/* event for close ui 
					 *
					 */
					g_js.keyboard.esc($(window.document),function(){g_js.windows.html.close_popup(v_callback_for_close);});
				}			

				/* adjust mask window size and position 
				 *
				 */
				js_div_move(
								'#id_create_popup_view_mask',
								0,
								0,
								$(window.document.body).get(0).offsetWidth			/* width */,
								$(document).height()								
							);
				
				/* show ui
				 *
				 */
				$('#id_create_popup').show();

				/* adjust content ui size and position 
				 * 
				 */
				var v_view_height	= window.document.documentElement.clientHeight == 0 ? window.document.body.clientHeight : window.document.documentElement.clientHeight;
				var v_div_height	= $('#id_create_popup_vm_view').fe_get_object_size().height;
				var v_top	= js_is_null(top)?(((v_view_height - v_div_height) / 2) + $(window.document).scrollTop()):(top+$(window.document).scrollTop());
				var v_left = js_is_null(left)?(( $('#id_create_popup_view_mask').fe_get_object_size().width - $('#id_create_popup_vm_view').fe_get_object_size().width)/2):left;

				js_div_move	(	
								'#id_create_popup_vm_view',
								v_left,
								v_top,
								0,
								0
							);
				
			},
            create_popup:function(v/* ui */,callback_ini,v_w,v_h,v_force_ini,v_callback_for_close,top,left,show_close_button,opacity,v_title){

                /* process default value
                 *
                 */
                v_w			= (js_is_null(v_w))?'700px':(v_w+'px');
                v_h			= (js_is_null(v_h))?'400px':(v_h+'px');
                v_force_ini = (js_is_null(v_force_ini))?false:true;
//                var v_vm_view_style  = 'z-index:10000;clear:both;margin:0 auto;position:absolute;background:white;text-align:left;';
                var v_vm_view_style  = 'z-index:10000;width:'+v_w+';height:'+v_h+';clear:both;margin:0 auto;position:absolute;background:white;text-align:left;';
                var v_vm_mask_style  = 'z-index:9999;position:absolute;left:0;top:0; background:black; filter:alpha(opacity=10); -moz-opacity:0.1; opacity:0.1;';
                var v_vm_close_style = 'background:url('+g_js_web_root_dir+'images/popup_close.png);width:25px;height:25px;float:right;margin:13px 11px;cursor:pointer;';
                if( ($('#id_create_popup').get().length==0)){

                    /* ini
                     *
                     */
                    var v_html	 = '	<div class="hide_div " id=id_create_popup>';
                    v_html		+=			'	<div id=id_create_popup_view_mask style="'+v_vm_mask_style+'"></div>';
                    v_html		+=			'	<div class="shadow " style="'+v_vm_view_style+'" id=id_create_popup_vm_view>';
                    v_html		+=			'		<div class="popup_header"><div class="popup_title">';
                    if(v_title!=undefined){
                        v_html		+=			v_title;
                    }
                    v_html		+=			'		</div>';
                    v_html		+=			'		<div style="float:right;">';
                    if(true){
                        v_html		+=			'			<div style="'+v_vm_close_style+'" onclick="g_js.windows.html.close_popup('+v_callback_for_close+');"></div>';
                    }
                    v_html		+=			'		</div></div>';
                    v_html		+=			'		<div class=clear_both></div>';
                    v_html		+=			'		<div id=id_create_popup_ex></div>';
                    v_html		+=			'	</div>';
                    v_html		+=		'</div>';
                    $(window.document.body).append( v_html );
                    v_force_ini = true;
                }

                /* force ini
                 *
                 */
                if(v_force_ini){

                    /* adjust widht and height
                     *
                     */
                    $('#id_create_popup_vm_view').css({'width':v_w,'height':v_h});

                    /* update ui
                     *
                     */
                    $('#id_create_popup_ex').html(v);

                    /* callback
                     *
                     */
                    (js_is_null(callback_ini))?'':callback_ini();

                    /* event for close ui
                     *
                     */
                    g_js.keyboard.esc($(window.document),function(){g_js.windows.html.close_popup(v_callback_for_close);});
                }

                /* adjust mask window size and position
                 *
                 */
                js_div_move(
                    '#id_create_popup_view_mask',
                    0,
                    0,
                    $(window.document.body).get(0).offsetWidth			/* width */,
                    $(document).height()
                );

                /* show ui
                 *
                 */
                $('#id_create_popup').show();

                /* adjust content ui size and position
                 *
                 */
                var v_view_height	= window.document.documentElement.clientHeight == 0 ? window.document.body.clientHeight : window.document.documentElement.clientHeight;
                var v_div_height	= $('#id_create_popup_vm_view').fe_get_object_size().height;
                var v_top	= js_is_null(top)?(((v_view_height - v_div_height) / 2) + $(window.document).scrollTop()):(top+$(window.document).scrollTop());
                var v_left = js_is_null(left)?(( $('#id_create_popup_view_mask').fe_get_object_size().width - $('#id_create_popup_vm_view').fe_get_object_size().width)/2):left;

                js_div_move	(
                    '#id_create_popup_vm_view',
                    v_left,
                    v_top,
                    0,
                    0
                );
            }
			,
			create_popup_big:function(v/* ui */,callback_ini,v_w,v_h,v_force_ini,v_callback_for_close,top,left,show_close_button,opacity){
				if($('#id_create_popup').length>0){$('#id_create_popup').remove();}
				if(show_close_button==null){
					show_close_button = true;
				}
				if(js_is_null(opacity)){
					opacity=0.7;
				}
				/* process default value
				 *
				 */
				v_w			= (js_is_null(v_w))?'700px':(v_w+'px');
				v_h			= (js_is_null(v_h))?'400px':(v_h+'px');
				v_force_ini = (js_is_null(v_force_ini))?false:true;
                var v_vm_view_style  = 'z-index:100;width:'+v_w+';clear:both;margin:0 auto;position:absolute;background:transparent;text-align:left;';
                var v_vm_mask_style  = 'z-index:99;position:absolute;left:0;top:0; background:#fff; filter:alpha(opacity=10); -moz-opacity:'+opacity+'; opacity:'+opacity+';';
				var v_vm_close_style = 'background:url('+g_js_web_root_dir+'images/popup_close.png) no-repeat scroll 0 0 transparent;width:53px;height:9px;position:absolute;cursor:pointer;right:0;top:-13px;';
				if( ($('#id_create_popup').get().length==0)){
					
					/* ini 
					 *
					 */
					var v_html	 = '	<div class="hide_div " id=id_create_popup>';
					v_html		+=			'	<div id=id_create_popup_view_mask style="'+v_vm_mask_style+'"></div>';					
					v_html		+=			'	<div style="'+v_vm_view_style+'" id=id_create_popup_vm_view>';
					if(show_close_button){
						v_html		+=			'			<div style="'+v_vm_close_style+'" onclick="g_js.windows.html.close_popup('+v_callback_for_close+');"></div>';
					}
					v_html		+=			'<div class="popup_t big"></div><div id="id_create_popup_ex1" style="height:'+v_h+'"></div><div class="popup_b big"></div>';
					v_html		+=			'	</div>';
					v_html		+=		'</div>';
					$(window.document.body).append( v_html );					
					v_force_ini = true;
				}
				
				/* force ini
				 *
				 */
				if(v_force_ini){				
					
					/* adjust widht and height
					 *
					 */
					$('#id_create_popup_vm_view').css({'width':v_w});

					/* update ui 
					 *
					 */
					$('#id_create_popup_ex1').html(v).css({'height':v_h});

					/* callback 
					 *
					 */
					(js_is_null(callback_ini))?'':callback_ini();

					/* event for close ui 
					 *
					 */
					g_js.keyboard.esc($(window.document),function(){g_js.windows.html.close_popup(v_callback_for_close);});
				}			

				/* adjust mask window size and position 
				 *
				 */
				js_div_move(
								'#id_create_popup_view_mask',
								0,
								0,
								$(window.document.body).get(0).offsetWidth			/* width */,
								$(document).height()								
							);
				
				/* show ui
				 *
				 */
				$('#id_create_popup').show();

				/* adjust content ui size and position 
				 * 
				 */
				var v_view_height	= window.document.documentElement.clientHeight == 0 ? window.document.body.clientHeight : window.document.documentElement.clientHeight;
				var v_div_height	= $('#id_create_popup_vm_view').fe_get_object_size().height;
				var v_top	= js_is_null(top)?(((v_view_height - v_div_height) / 2) + $(window.document).scrollTop()):(top+$(window.document).scrollTop());
				var v_left = js_is_null(left)?(( $('#id_create_popup_view_mask').fe_get_object_size().width - $('#id_create_popup_vm_view').fe_get_object_size().width)/2):left;

				js_div_move	(	
								'#id_create_popup_vm_view',
								v_left,
								v_top,
								0,
								0
							);
				
			}
			,
			create_popup_landing:function(v/* ui */,callback_ini,v_w,v_h,v_force_ini,v_callback_for_close,top,left,show_close_button,opacity){
			
				if(show_close_button==null){
					show_close_button = true;
				}
				if(js_is_null(opacity)){
					opacity=0.7;
				}
				/* process default value
				 *
				 */
				v_w			= (js_is_null(v_w))?'700px':(v_w+'px');
				v_h			= (js_is_null(v_h))?'400px':(v_h+'px');
				v_force_ini = (js_is_null(v_force_ini))?false:true;
				var v_vm_view_style  = 'z-index:10000;width:'+v_w+';clear:both;margin:0 auto;position:absolute;background:transparent;background-color:#fff;text-align:left;';
				var v_vm_mask_style  = 'z-index:9999;position:absolute;left:0;top:0; background:#fff; filter:alpha(opacity=10); -moz-opacity:'+opacity+'; opacity:'+opacity+';';
				var v_vm_close_style = 'background:url('+g_js_web_root_dir+'images/close-button.png) no-repeat scroll 0 0 transparent;width:32px;height:32px;position:absolute;cursor:pointer;right:-16px;top:-16px;z-index:999;';
				if( ($('#id_create_popup').get().length==0)){
					
					/* ini 
					 *
					 */
					var v_html	 = '	<div class="hide_div " id=id_create_popup>';
					v_html		+=			'	<div id=id_create_popup_view_mask style="'+v_vm_mask_style+'"></div>';					
					v_html		+=			'	<div style="'+v_vm_view_style+'" id=id_create_popup_vm_view>';
					if(show_close_button){
						v_html		+=			'			<div style="'+v_vm_close_style+'" onclick="g_js.windows.html.close_popup('+v_callback_for_close+');"></div>';
					}
					v_html		+=			'<div id="id_create_popup_ex" style="background:none no-repeat scroll 0 0 transparent;padding:0;height:'+v_h+'"></div>';
					v_html		+=			'	</div>';
					v_html		+=		'</div>';
					$(window.document.body).append( v_html );					
					v_force_ini = true;
				}
				
				/* force ini
				 *
				 */
				if(v_force_ini){				
					
					/* adjust widht and height
					 *
					 */
					$('#id_create_popup_vm_view').css({'width':v_w});

					/* update ui 
					 *
					 */				 
					$('#id_create_popup_ex').html(v);

					/* callback 
					 *
					 */
					(js_is_null(callback_ini))?'':callback_ini();

					/* event for close ui 
					 *
					 */
					g_js.keyboard.esc($(window.document),function(){g_js.windows.html.close_popup(v_callback_for_close);});
				}			

				/* adjust mask window size and position 
				 *
				 */
				js_div_move(
								'#id_create_popup_view_mask',
								0,
								0,
								$(window.document.body).get(0).offsetWidth			/* width */,
								$(document).height()								
							);
				
				/* show ui
				 *
				 */
				$('#id_create_popup').show();

				/* adjust content ui size and position 
				 * 
				 */
				var v_view_height	= window.document.documentElement.clientHeight == 0 ? window.document.body.clientHeight : window.document.documentElement.clientHeight;
				var v_div_height	= $('#id_create_popup_vm_view').fe_get_object_size().height;
				var v_top	= js_is_null(top)?(((v_view_height - v_div_height) / 2) + $(window.document).scrollTop()):(top+$(window.document).scrollTop());
				var v_left = js_is_null(left)?(( $('#id_create_popup_view_mask').fe_get_object_size().width - $('#id_create_popup_vm_view').fe_get_object_size().width)/2):left;

				js_div_move	(	
								'#id_create_popup_vm_view',
								v_left,
								v_top,
								0,
								0
							);
				
				$('#id_create_popup_view_mask').click(function(){
					g_js.windows.html.close_popup();
				});

			}
			,
			create_view:function(v_container_id,v_url,v_width,v_height){
								
				v_width		= (undefined==v_width)	?'100%'	:v_width	;
				v_height	= (undefined==v_height)	?'600px':v_height	;				
				if( $(v_container_id).get().length>0 ){
					
					$(v_container_id).html ( '<iframe border=0 FRAMEBORDER=no src='+v_url+'></iframe>').children().width(v_width).height(v_height);
				}
			}
		}
		,facebook:{
			close_popup:function(v_callback_for_close){$('#id_create_popup').hide('fast');if(!js_is_null(v_callback_for_close)){eval('('+v_callback_for_close+')();');}}
			,
			create_popup:function(v/* ui */,callback_ini,v_w,v_h,v_force_ini,v_callback_for_close){

				/* process default value
				 *
				 */
				v_w			= (js_is_null(v_w))?'700px':(v_w+'px');
				v_h			= (js_is_null(v_h))?'400px':(v_h+'px');
				v_force_ini = (js_is_null(v_force_ini))?false:true;
				var v_vm_view_style  = 'z-index:100;border:solid 1px #888;width:'+v_w+';height:'+v_h+';clear:both;margin:0 auto;position:absolute;background:white;text-align:left;';
				var v_vm_mask_style  = 'z-index:99;position:absolute;left:0;top:0; background:black; filter:alpha(opacity=10); -moz-opacity:0.1; opacity:0.1;';
				var v_vm_close_style = 'background:url('+g_js_web_root_dir+'images/popup_close.png);width:16px;height:16px;float:right;margin:8px 11px;cursor:pointer;';
				if( ($('#id_create_popup').get().length==0)){
					
					/* ini 
					 *
					 */
					var v_html	 = '	<div class="hide_div " id=id_create_popup>';
					v_html		+=			'	<div id=id_create_popup_view_mask style="'+v_vm_mask_style+'"></div>';					
					v_html		+=			'	<div class="radius_5 shadow " style="'+v_vm_view_style+'" id=id_create_popup_vm_view>';
					v_html		+=			'		<div style="float:left;width:50%;">';
					v_html		+=			'			';					
					v_html		+=			'		</div>';
					v_html		+=			'		<div style="float:right;width:50%;">';
					v_html		+=			'			<div style="'+v_vm_close_style+'" onclick="g_js.windows.html.close_popup('+v_callback_for_close+');"></div>';
					v_html		+=			'		</div>';
					v_html		+=			'		<div class=clear_both></div>';
					v_html		+=			'		<div id=id_create_popup_ex></div>';
					v_html		+=			'	</div>';
					v_html		+=		'</div>';
					$(window.document.body).append( v_html );					
					v_force_ini = true;
				}
				
				/* force ini
				 *
				 */
				if(v_force_ini){				
					
					/* adjust widht and height
					 *
					 */
					$('#id_create_popup_vm_view').css({'width':v_w,'height':v_h});

					/* update ui 
					 *
					 */				 
					$('#id_create_popup_ex').html(v);

					/* callback 
					 *
					 */
					(js_is_null(callback_ini))?'':callback_ini();

					/* event for close ui 
					 *
					 */
					g_js.keyboard.esc($(window.document),function(){g_js.windows.html.close_popup(v_callback_for_close);});
				}			

				/* adjust mask window size and position 
				 *
				 */
				js_div_move(
								'#id_create_popup_view_mask',
								0,
								0,
								$(window.document.body).get(0).offsetWidth			/* width */,
								$(document).height()								
							);
				
				/* show ui
				 *
				 */
				$('#id_create_popup').show();

				/* adjust content ui size and position 
				 * 
				 */
			    var v_view_height	= window.document.documentElement.clientHeight == 0 ? window.document.body.clientHeight : window.document.documentElement.clientHeight;
				var v_div_height	= $('#id_create_popup_vm_view').fe_get_object_size().height;
				var v_top			= 100;
				
 				js_div_move	(	
								'#id_create_popup_vm_view',
								( $('#id_create_popup_view_mask').fe_get_object_size().width - $('#id_create_popup_vm_view').fe_get_object_size().width)/2,
								v_top,
								0,
								0
							);
			}
		}
		,
		dialog:{
			close_ok:function(){this.m_b_cancel_ok=true;this.m_b_close_ok=true;}
			,
			close	:function(v_dlg_id){this.close_ok();$(v_dlg_id).dialog('close');}
			,
			show_ok :function(){this.m_b_cancel_ok=false;this.m_b_close_ok=false;}
			,			
			show	:function(v_dlg_id){this.show_ok();$(v_dlg_id).dialog('open');}
			,
			create:function(){switch( arguments.length ){

					case 0:{																										;	break;}
					case 1:{	this.create_ex(arguments[0],null,null,true,null,true)												;	break;}
					case 2:{	this.create_ex(arguments[0],arguments[1],null,true,null,true)										;	break;}
					case 3:{	this.create_ex(arguments[0],arguments[1],arguments[2],true,null,true)								;	break;}
					case 4:{	this.create_ex(arguments[0],arguments[1],arguments[2],arguments[3],null,true)						;	break;}
					case 5:{	this.create_ex(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],true)				;	break;}
					case 6:{	this.create_ex(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])		;	break;}
 				}
			}
			,
			create_ex:function	(	v_dlg_id				/* dialog id			*/,
									v_callback_for_ok		/* ok event				*/,
									v_callback_for_cancel	/* cancel event			*/,
									v_modal					/* modal or modeless	*/,
									v_width					/* width				*/,
									v_auto_close			/* auoto close dialog	*/
								){
				/* dialog id validate
				 *
				 */
				var v_this=this;if($(v_dlg_id).get().length==0){return;}

				/* create a dialog
				 *
				 */				 
				$(v_dlg_id).dialog({

					autoOpen:	false							
					,
					width	:	(null==v_width)?900:v_width		
					,					
					modal	:	(!v_modal)						
					,
					buttons	:	{
									"Ok"	:function(){if(null==v_callback_for_ok){$(this).dialog('close');}else{
											
											v_callback_for_ok();if(v_auto_close){v_this.close_ok();$(this).dialog('close');}
										}
									}
									,
									"Cancel":function(){if(null==v_callback_for_cancel){$(this).dialog('close');}else{
											
											v_callback_for_cancel();if(v_auto_close){v_this.close_ok();$(this).dialog('close');}
										}
									}
								}
					,
					beforeClose	:	function(e,ui){if(v_this.m_b_cancel_ok){}else{
							
							(null==v_callback_for_cancel)?({}):v_callback_for_cancel();if(v_auto_close){v_this.close_ok();$(this).dialog('close');}
						}
						return v_this.m_b_close_ok;
					}
				});
			}
			,
			m_b_cancel_ok	:false
			,
			m_b_close_ok	:false
		}
		,
		confirm:{

			info:function(v_info)	{	return window.confirm(v_info);	}
			,
			deletex:function()		{	var v_r = window.confirm('Are you sure delete this item ?')	;return(v_r);
			}
		}
	}
};
$(document).ready(function(){
	$("img").bind("contextmenu",function(){
		return false;
	});
});