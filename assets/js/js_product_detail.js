$(document).ready(function() {
	if ($(window).width() < 1025) $(".product_in li img").attr({'onmouseover':'','onmouseout':''})
});


var js_product_detail={
click_ext_img:function(item){
		var item=$(item);
		var ov=item.attr("ov");
		var value=item.attr("value");
		$("#id_option_for_dropdown_sel_" + ov).val(value);
		$("#id_option_for_dropdown_sel_" + ov).val(value);
		$("#id_option_for_dropdown_sel_" + ov).change();
		js_product_detail.change_ext_img_style(value);
		g_js.js.ajax.fg.get(1,9,'ov='+ov+'&option_value_id='+value,function(v) {
			$('#button_add_to_cart').html(v);
		});
	},
	popup_video:function(url){
		
		var content = '<div style="width:100%;text-align:center;"><iframe width="640" height="480" frameborder="0" src="'+url+'"></iframe></div>';
		g_js.windows.html.create_popup(content,function(){},700,550,true,null,null,null,true,null,'');
	}
	,
	click_first_img_thumbnail:function(sid){
		//detail page
		$('.product_img_regional .thumbnail img').each(function(){
			var attr_options_value_id=$(this).attr("attr_options_value_id").split("_");
			//if($(this).css('display')=='inline'){
			if(attr_options_value_id[1]==sid){
				$(this).click();
				return false;
			}
		});
		//quick view
		$('.popup_quickview_l .popup_quickview_img_small_qv img').each(function(){
			var attr_options_value_id=$(this).attr("attr_options_value_id").split("_");
			//if($(this).css('display')=='inline'){
			if(attr_options_value_id[1]==sid){
				$(this).click();
				return false;
			}
		});
	},
	change_ext_img_style:function(id){
		$(".ext_img_added").removeClass('ext_img_added_selected');
		$("#ext_" + id).addClass('ext_img_added_selected');
	},
	option_for_dropdown:{click:function(o,v_p_id){var v_option_value_id = $(o).val();$('#id_option_for_dropdown_img_'+v_p_id+'_'+v_option_value_id).trigger('click');}}
	,
	custom:{
		m_n_p_id_for_custom			:0		/* current product id				*/
		,
		m_b_custom_product_option	:false	/* true => custom,false => normal	*/
		,
		show:function(v_pid){
			
			/* reset var
			 *
			 */
			this.m_n_p_id_for_custom=v_pid;this.m_b_custom_product_option=true;var v_this=this;js_product_detail.pic.option_picture_reset();

			/* show popup view
			 *
			 */
			g_js.windows.html.create_popup(this.get_custom_ui(v_pid),function(){ v_this.custom_ui_ini();},400,550,true);
		}
		,
		custom_ui_ini:function(){

			/* ini your custom html ui ,client event
			 *
			 */		
			var v_this = this;$('#id_custom_close').click(function(){g_js.js.ajax.fg.get(1,1,'v_product_id='+v_this.m_n_p_id_for_custom,function(v){
					 
					js_product_detail.update_product_info_for_child(g_js.json.get(v));g_js.windows.html.close_popup();
				 });				 
			});
		}
		,
		get_custom_ui:function(v_pid){
			
			/* custom your product html ui
			 *
			 */
			return '<div id=id_custom_x>Please input your custom html here.</div><input type=button id="id_custom_close" value="ok">';
		}
		,
		get_custom_ok:function()		{
			
			/* get user custom product information => callback function,u do not need call ,remember ,my boy. it auto called by framework
			 *
			 */
			return $('#id_custom_x').html();
		}
	}
	,
	preview:function(v_id){var v_url = 'product88dd.html?v_pid='+v_id;window.open(v_url);}
	,
	price_alert:{
		save:function(){
			
			/* validate
			 *
			 */
			var v_p_id = js_product_detail.get_current_selected_product_id();if( v_p_id>0 ){}else{$.jq_alert(js_language.cst_please_choose_product_otions);return;}
			if( (''==$('input:[name=name_for_pa_your_price]').val()) ||	isNaN($('input:[name=name_for_pa_your_price]').val()) ){

				$.jq_alert(js_language.cst_price_alert_for_u_price);	return;
			}
			var your_name =$('input:[name=name_for_pa_your_name]').val();if(your_name.replace(/ /g,'')==''){$.jq_alert(js_language.cst_price_alert_for_u_name)	;return;}
			var your_email=$('input:[name=name_for_pa_your_email]').val();if(your_email.replace(/ /g,'')==''){$.jq_alert(js_language.cst_price_alert_for_u_email);return;}
			if (js_validate_mail(your_email)){

				/* save 
				 *
				 */
				g_js.js.ajax.fg.post(1,5,'#id_form_for_price_alert','v_p_id='+v_p_id,function(v){$.jq_alert(js_language.cst_price_drop_alert);});
			}
		}
		,
		ini:function(){	}
	}
	,
	article_detail:function(article_id) {g_js.js.ajax.fg.get(1,4,'article_id='+article_id,function(v){g_js.windows.html.create_popup(v,function(){},400,550,true);});},
	write_comment:function(){
		
		/* select comment tab
		 *
		 */
		var o = $('#id_tabs_for_product_detail');o.tabs('select',2);

		/* scroll view
		 *
		 */
		js_go_to('#id_customer_reviews_vs');
		
		/* focus comment 
		 *
		 */
		$('#id_for_name_for_comment_content').focus();
	}
	,
	get_current_selected_product_id:function(){
		
		if(this.onlyonechild_id>0){
			return this.onlyonechild_id;
		}
		
		
		/* check product type and get select product id
		 *
		 */
		var v_n_product_type = this.m_n_product_type;
		var v_p_id			 = 0;
		if( 0==v_n_product_type			){	v_p_id	= this.m_n_parent_product_id	;/* single prodcut */}
		else if (1==v_n_product_type	){	v_p_id	= this.m_n_child_product_id		;/* parent product */}
		else{}

		return v_p_id;
	}
	,
	add_to_wishlist_for_baby:function(){

		/* get current selected product id
		 *
		 */
		var v_p_id = this.get_current_selected_product_id();

		/* validate
		 *
		 */
		if( v_p_id>0 ){}else{$.jq_alert(js_language.cst_please_choose_product_otions);return;}

		/* is login ?
		 *
		 */
		if(js_get_user_id()==0){js_user.login(window.location.href.replace('#','&'),1);}else{
		
			/* have i already created baby wishlist ?
			 *
			 */
			g_js.js.ajax.fg.get(0,18,null,function(v){
				
				if( v==''){$.jq_alert(js_language.cst_baby_information_is_required);js_go_to(js_get_web_root()+'user.php?v_view_id=7');}else{
				
				/* add product to baby wishlist
				 *
				 */
				js_user.wish_list.wl_add_product_to_wishlist_for_baby(v_p_id/* product id */,v/* wish list id */);
			}});
		}
	}
	,
	add_to_wishlist_for_wedding:function(){

		/* get current selected product id
		 *
		 */
		var v_p_id = this.get_current_selected_product_id();

		/* validate
		 *
		 */
		if( v_p_id>0 ){}else{$.jq_alert(js_language.cst_please_choose_product_otions);return;}
				
		/* is login ?
		 *
		 */
		if(js_get_user_id()==0){js_user.login(window.location.href.replace('#','&'),1);}else{

			/* have i already created wedding wishlist ?
			 *
			 */
			g_js.js.ajax.fg.get(0,17,null,function(v){
				
				if( v==''){$.jq_alert(js_language.cst_wedding_information_is_required);js_go_to(js_get_web_root()+'user.php?v_view_id=5');}else{
				
				/* add product to wedding wishlist
				 *
				 */
				js_user.wish_list.wl_add_product_to_wishlist_for_wedding(v_p_id/* product id */,v/* wish list id */);
			}});
		}
	}
	,
	add_to_wishlist:function(wid,obj){

		if($(obj).hasClass('selected')) return;
		
		/* is login ?
		 *
		 */
		if(js_get_user_id()==0){js_user.login(window.location.href.replace('#','&'),1);}else{
			/* get current selected product id
			 *
			 */
			var v_p_id = this.get_current_selected_product_id();
			/* validate
			 *
			 */
			if( v_p_id>0 ){}else{$.jq_alert(js_language.cst_please_choose_product_otions);return;}

			/* add product to wishlist
			 *
			 */
			//js_user.wish_list.wl_add_product_to_wishlist(v_p_id);
			js_user.wish_list.wl_save(v_p_id,wid);
			$(obj).addClass('selected');
		}
	}
	,
	add_to_wishlist_c:function(obj,wid){
		if($(obj).hasClass('selected')) return;
		if(js_get_user_id()==0){js_user.login(window.location.href.replace('#','&'),2);}else{
			var v_p_id = $(obj).attr('rv');
			if(v_p_id==0){
//				g_js.windows.html.create_popup_small('<div style="text-align:center;min-height:50px;">'+js_language.cst_please_choose_product_otions+'</div>',function(){
//				},400,100,true,null,null,null,false,0.7,'');
				//$.jq_alert(js_language.cst_please_choose_product_otions);return;
				$.jq_alert(js_language.cst_please_choose_product_otions);
			}else{
				js_user.wish_list.wl_save(v_p_id,wid,obj);
			}
		}
	}
	,
	pic_click_to_view_big_for_popup:function(o){$('#id_popup_largeimage_img').html('<img src="'+$(o).attr('attr_url_for_image')+'" border=0>');}
	,
	pic_click_to_enlarge_view_big:function(v_b_fb){if(!js_is_null(v_b_fb)){

			/* fb
			 *
			 */
			 g_js.js.ajax.fg.get(13,0,'v_product_id='+js_product_detail.m_n_parent_product_id,function(v){
				
				g_js.windows.facebook.create_popup(v,function(){},460,340,true);
			 });
		}else{

			/* normal product detail page
			 *
			 */
			var attr_url_for_image_large = $('#id_product_picture').attr('attr_url_for_image_large');
			if(attr_url_for_image_large == undefined){
				attr_url_for_image_large = '';
			}
			 g_js.js.ajax.fg.get(1,2,'v_product_title='+js_url_encoder($('#id_product_name').html())+'&v_product_id='+js_product_detail.m_n_parent_product_id+'&attr_url_for_image_large='+attr_url_for_image_large,function(v){

				 g_js.windows.html.create_popup(v,function(){},790,550,true);
			 });
		}	
	}
	,
	cart:null
	,
	add_to_cart:function(v_add_to_cart_by_fb){
		if(!js_is_null(v_add_to_cart_by_fb)){
			js_product_detail.cart.add(
				$('#id_product_qty').val(),
				js_product_detail.m_n_product_type,
				js_product_detail.m_n_parent_product_id,
				js_product_detail.m_n_child_product_id,
				js_product_detail.onlyonechild_id,
				1
			);
		}
		else{
			/* is custom ?
			 *
			*/
			if(this.custom.m_b_custom_product_option){
				/* ok		=> custom
				 *
				*/
				js_cart.add_customed_to_cart(this.custom.m_n_p_id_for_custom,this.custom.get_custom_ok(),$('#id_product_qty').val());
			}
			else{
				/* normal	=> product option selected
				 *
				 */
				//
				var pid = 0;
				var product_sku_code = "";
				var need_option = false;
				$(".select_ui_ex select").each(function(){
					var ops = $(this).attr("id").replace("id_option_for_dropdown_sel_","").split("_");
					pid = ops[0];
					if($(this).val() > 0){
						if(product_sku_code == ""){
							product_sku_code = ops[0]+"{"+ops[1]+"}"+$(this).val();
						}
						else{
							product_sku_code += "{"+ops[1]+"}"+$(this).val();
						}
					}
					else{
						 need_option = true;
					}
				});
				if(need_option == true){
					alert("Please choose options");
					return;
				}
				js_cart.add_to_cart2(pid,product_sku_code,$('#id_product_qty').val());
				/*
				js_product_detail.cart.add(
					$('#id_product_qty').val(),
					js_product_detail.m_n_product_type,
					js_product_detail.m_n_parent_product_id,
					js_product_detail.m_n_child_product_id,
					js_product_detail.onlyonechild_id
				);
				*/
			}
		}
	}
	,
	optionsx:null
	,
	pic:null
	,
	update_product_info_for_child:function(o){

		/* update ui
		 *
		 */
		$('#id_product_name')					.html(o.products_name)					;	/* product name										*/
		$('#id_seo_and_short_description')		.html(o.seo_short_description)			;	/* product seo and short description				*/
		$('#id_customer_type_price')			.html(o.product_customer_type_price)	;	/* product customer type price						*/
		$('#tabs-2')							.html(o.child_product_description)		;	/* product description								*/
		$('#id_product_sku')					.html(o.products_sku)					;	/* product sku										*/
		$('#id_product_merchant_sku')			.html(o.merchant_sku)					;	/* product merchant sku								*/
		$('#id_product_upc')					.html(o.products_upc)					;	/* product upc										*/
		$('#id_product_isbn')					.html(o.products_isbn)					;	/* product isbn										*/
		$('#id_product_ean')					.html(o.products_ean)					;	/* product ean										*/
		$('#id_product_stock')					.html(o.child_product_stock)			;	/* product stock status								*/
		$('#id_product_sale_price')				.html(o.child_product_price)			;	/* product child product price						*/
		$('#id_product_list_price_and_you_save').html(o.child_product_price_list)		;	/* product child product price for list and save	*/
		if(o.promotion_enabled){$('#id_product_promotion').html(o.products_promotion);}	;	/* product child product promotion					*/
	},	
	tab_nav_products_ini:function(){
		$(".tab_nav.products ul li a").click(function(){
			var si = $(".tab_nav.products ul li a").index(this);
			$(".tab_nav.products ul li.selected").removeClass("selected");
			$(this).parent().addClass("selected");
			$(".items_list_content.selected").removeClass("selected");
			$(".items_list_content").eq(si).addClass("selected");
		});
	},
	show_sizechart:function(){
		g_js.js.ajax.fg.get(1,6,null,function(v){					 
			g_js.windows.html.create_popup(v,function(){},790,697,true);
		 });	
		
	},
	tell_friend:function(pid){
		g_js.js.ajax.fg.get(1,7,'pid='+pid,function(v){					 
			g_js.windows.html.create_popup(v,function(){},600,397,true);
		});	
	},
	send_tell_friend:function(){
		var name = $("#id_contact_us_name").val();
		var email = $("#id_contact_us_email").val();
		var enquire = $("#enquiry").val();
		var id_your_friends_name = $("#id_your_friends_name").val();
		var id_your_friends_email = $("#id_your_friends_email").val();

		if (name.replace(/ /g,'')==''){
			$.jq_alert(js_language.cst_contact_us_name);
			return false;
		}

		if (email.replace(/ /g,'')==''){
			$.jq_alert(js_language.cst_contact_us_email);
			return false;
		}

		if (id_your_friends_name.replace(/ /g,'')==''){
			$.jq_alert('Please input your friend\'s full name.');
			return false;
		}

		if (id_your_friends_email.replace(/ /g,'')==''){
			$.jq_alert('Please input your friend\'s email.');
			return false;
		}
				
		if (enquire.replace(/ /g,'')==''){
			$.jq_alert(js_language.cst_contact_us_enquire);
			return false;
		}		
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(1,8,"#tell_friend_form","",function(v){
			var o = g_js.json.get(v); 
			g_js.windows.mask.busy.close();
			if (o.error == 1){
				$.jq_alert(o.result);
				return false;
			} else {
				window.location.href = window.location.href;
			}
		});
		
	},
	clear:function(){
		$('#tell_friend_form input,#tell_friend_form textarea').val('');
	},
	ini:function(){
		$('.options2 .item .l > img').each(function(){
			$(this).hover(function(){
				var b_img = '<img src="'+$(this).attr("bimg")+'">';
				if($("#options2_popimg").length == 1){
					$("#options2_popimg").remove();
				}
				var pophtml = '<div id="options2_popimg" style="position:absolute;left:0px;top:0px;border:0px solid red;padding:0px;">'+b_img+'</div>';
				$(this).parent().parent().append(pophtml);
				var pophtml_h = $("#options2_popimg").height();
				$("#options2_popimg").css("top",-1*(pophtml_h+3));
				$("#options2_popimg").show();
			},
			function(){
				$("#options2_popimg").hide();
			});
		});

		$('.options1 .item').click(function(){
			$(this).addClass('selected').siblings().removeClass('selected');
			var pid = $(".options1 .item.selected").attr("lpid");
			var options1_vid = $(".options1 .item.selected").attr("vid");
			g_js.js.ajax.fg.get(1,11,'pid='+pid+'&options1_vid='+options1_vid,function(v){
				$(".options2").html(v);
				$('.options2 .item .l > img').each(function(){
					$(this).hover(function(){
						var b_img = '<img src="'+$(this).attr("bimg")+'">';
						if($("#options2_popimg").length == 1){
							$("#options2_popimg").remove();
						}
						var pophtml = '<div id="options2_popimg" style="position:absolute;left:0px;top:0px;border:0px solid red;padding:0px;">'+b_img+'</div>';
						$(this).parent().parent().append(pophtml);
						var pophtml_h = $("#options2_popimg").height();
						$("#options2_popimg").css("top",-1*(pophtml_h+3));
						$("#options2_popimg").show();
					},
					function(){
						$("#options2_popimg").hide();
					});
				});
			});
		});
		/*
		$('.options2_title').click(function(){
			if($(".options1 .item.selected").length == 0){
				alert("Please select Fabric.");
				return;
			}
			var obj = $(this);
			var pid = $(".options1 .item.selected").attr("lpid");
			var options1_vid = $(".options1 .item.selected").attr("vid");
			g_js.js.ajax.fg.get(1,11,'pid='+pid+'&options1_vid='+options1_vid,function(v){
				$("#tinyscrollbar_content").html(v);
				if(obj.hasClass('selected')){
					$('.options2').hide();
					obj.removeClass('selected');
				}
				else{
					$('.options2').show();
					obj.addClass('selected');
					if($('#tinyscrollbar_content').height() > $('#tinyscrollbar').height()){
						start_scroll();
					}
				}
				//
				$('.options2 .item .l > img').each(function(){
					$(this).hover(function(){
						var b_img = '<img src="'+$(this).attr("bimg")+'">';
						//var b_img = '<img src="images/sysicons/Jockeyred.png">';
						if($("#options2_popimg").length == 1){
							$("#options2_popimg").remove();
						}
						var pophtml = '<div id="options2_popimg" style="position:absolute;left:0px;top:0px;border:0px solid red;padding:0px;">'+b_img+'</div>';
						$(this).parent().parent().append(pophtml);
						var pophtml_h = $("#options2_popimg").height();
						$("#options2_popimg").css("top",-1*(pophtml_h+3));
						$("#options2_popimg").show();
					},
					function(){
						$("#options2_popimg").hide();
					});
				});
			});
		});
		*/
		$('.ext_icon').click(function(){
			if($(this).html() == '+'){
				$(this).html('-');
				$(this).next().show();
			}else{
				$(this).html('+');
				$(this).next().hide();
			}
		});
		if($('#id_tabs_for_product_detail').get().length>0){
			
			/* ini tabs
			 * 
			 */
			$('#id_tabs_for_product_detail').tabs({selected: 0});

			/*
			 *
			 */
			$(".prev_page_bt").click(function(){if($(".also_like_pro").attr("rvi") == "1"){	return false;}
			$(".also_like_pro_content:not(:animated)").animate({marginLeft: '+=1160'}, "slow",function(){

				$(".also_like_pro").attr("rvi",parseInt($(".also_like_pro").attr("rvi")) - 1);
			});});
			$(".next_page_bt").click(function(){

				var rvi_content = parseInt($(".also_like_pro").attr("rvi"));
				var rvc_content = parseInt($(".also_like_pro").attr("rvc"));
				if( rvi_content == rvc_content){return false;}
				$(".also_like_pro_content:not(:animated)").animate({marginLeft: '-=1160'}, "slow",function(){$(".also_like_pro").attr("rvi",rvi_content+1);});
			});
		}
		this.m_n_parent_product_id	= $('#id_n_parent_product_id').html()	;	/* product id					*/	
		this.m_n_product_type		= $('#id_n_product_type').html()		;	/* product type					*/	
		this.onlyonechild_id	= $('#id_onlyonechild_id').html()	;
		/* ini product thumbnail , options , cart
		 *
		*/
		this.optionsx	= new c_product_options_x();
		this.optionsx.ini(this.m_n_parent_product_id,$('#id_options_max_index').html(),function(o){
			/* update child product id
			 *
			*/
			js_product_detail.m_n_child_product_id = js_product_detail.optionsx.m_n_child_product_id;
			js_product_detail.update_product_info_for_child(o);
		});
		this.pic		= new c_product_options_pic();
		this.pic.ini('product_thumbnail_list',this.optionsx,'id_product_picture');
		this.cart		= new c_cart();this.cart.ini();
		this.tab_nav_products_ini();
		if(this.onlyonechild_id>0){
			$('.product_options_class_for_image_ex').addClass('selected');
		}
		$('.default_selected_ovid').each(function(){
			var ovid = $(this).html();
			$('#ovid_'+ovid).trigger('click');
		});
	}
	,
	m_n_parent_product_id:0
	,
	m_n_child_product_id:0
	,
	m_n_product_type:0
};
(function(){
	js_product_detail.ini();
	$(".options3_select").each(function(index,elm){
		if($(this).find("option").length == 1){
			$('#'+$(this).attr("lid")).val($(this).val());
		}
	});
})();