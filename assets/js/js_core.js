/*
 *
 */
function js_c_scroll_thumbnail(){

	this.m_n_current_pos	= 0;
	this.m_n_current_index	= 0;
}	
js_c_scroll_thumbnail.prototype.ini =function (v_id/* container */,v_max_index/* max index */,v_one_item_width/* one item width */,v_uuid,v_n_view_num/* view number */){
	
	var v_this=this;var o=$(v_id);if(o.get().length>0){
				
			this.m_ui					= o.html()			;
			this.m_n_max_index			= v_max_index		;
			this.m_n_one_item_width		= v_one_item_width	;

			/* create scroll ui
			 *
			 */
			var v_html  = '<div id=id_left_'+v_uuid+' class="st_left '+((v_n_view_num<v_max_index)?'':'hide_div')+'"  >';
				v_html += '		<img  src='+js_get_themes_root()+'images/product_detail_thumbnail_scroll_left.png border=0>';
				v_html += '</div>';
				v_html += '<div class=st_content>';
				v_html += '		<div class=st_content_ex id=id_scroll_uuid_'+v_uuid+'>';
				v_html += '			<table><tr><td valign=middle>'+this.m_ui+'</td></tr></table>';
				v_html += '		</div>';
				v_html += '</div>';
				v_html += '<div id=id_right_'+v_uuid+' class="st_right '+((v_n_view_num<v_max_index)?'':'hide_div')+'" >';
				v_html += '		<img   src='+js_get_themes_root()+'images/product_detail_thumbnail_scroll_right.png border=0>';
				v_html += '</div>';
				v_html += '<div class=clear_both></div>';		

			/* show it
			 *
			 */
			o.html(v_html).js_ex().anx.show.ex(); 

			/* bind left,right
			 *
			 */
			$('#id_left_'+v_uuid	).bind('click',function(){v_this.left(v_uuid);});
			$('#id_right_'+v_uuid	).bind('click',function(){v_this.right(v_uuid);});
	}
}
js_c_scroll_thumbnail.prototype.get_scroll_width=function(){return this.m_n_one_item_width;}
js_c_scroll_thumbnail.prototype.right=function(v_uuid){
	
	this.m_n_current_pos  += this.get_scroll_width();this.m_n_current_index--;
	if(this.m_n_current_pos>=0){this.m_n_current_pos=0;this.m_n_current_index=0;};$('#id_scroll_uuid_'+v_uuid).js_ex().anx.show.to_left(this.m_n_current_pos);				
}
js_c_scroll_thumbnail.prototype.left=function(v_uuid){

	if(this.m_n_current_index>=this.m_n_max_index-1){}else{
		
		this.m_n_current_pos  -= this.get_scroll_width();this.m_n_current_index++;$('#id_scroll_uuid_'+v_uuid).js_ex().anx.show.to_left(this.m_n_current_pos);$('#id_right_'+v_uuid).show();	
	}		
}

var js_core={	
	qa:{		
		ok:function(v_p_id){
			
			if(js_get_user_id()==0){alert('Please log in first.');return;}else{
				
				g_js.js.ajax.fg.post(8,4,'#id_form_for_qa','v_p_id='+v_p_id,function(v){alert(js_language.cst_product_detail_qa_ok);});
			}
		}
	}
	,
	menu:{
		header:{

			ini:function(){
				
				$('#main_menu1').hover(function(){						

					$('#id_menu_header_container_0').css	({
															'left':$('#main_menu1').fe_get_object_absolute_position().left+'px'
															,
															'top':$('#main_menu1').fe_get_object_absolute_position().top+38+'px'
														}).js_ex().anx.show.scroll.down();
				},function(){});
				$('#id_menu_header_container_0').hover(function(){},function(){$('#id_menu_header_container_0').js_ex().anx.hide.scroll.up();});

				/* 
				 *
				 */
				$('.mhc_one_item').hover(function(){

					var v_this	= this;var v_id=($(this).attr('attr_id'));var v_pid=($(this).attr('attr_pid'));

					/* reset
					 *
					 */
					$('.mhc_one_item').each(function(i,d){if($(this).attr('attr_pid')==v_pid){
						
							$('#id_menu_header_container_'+$(this).attr('attr_id')).hide();
							$('#id_menu_item_one_item_'+$(this).attr('attr_id')).removeClass('mhc_one_item_selected');
						}
					});
					

					/* show sub category
					 *
					 */		
					$(this).addClass('mhc_one_item_selected');
					$('#id_menu_header_container_'+v_pid).show();
					$('#id_menu_item_one_item_'+v_pid).addClass('mhc_one_item_selected');
 					$('#id_menu_header_container_'+v_id).css	({
																	'left'	:$(v_this).fe_get_object_size().width-0+'px'
																	,
																	'top':$(v_this).fe_get_object_absolute_position().top-$('#id_menu_header_container_'+$(v_this).attr('attr_pid')).fe_get_object_absolute_position().top+'px'
																}).js_ex().anx.show.scroll.left();

				},function(){

					var v_this	= this;var v_id=($(this).attr('attr_id'));var v_pid=($(this).attr('attr_pid'));$(this).removeClass('mhc_one_item_selected');
					$('#id_menu_header_container_'+v_id).hide();					
				});
 			}
		}
		,
		ini:function(){

			this.header.ini();
		}
	}
	,
	othx:{
		funx:{
			product_short_desc_2_long:function(v_tab_id){js_core.tabx.select_tab_by_index(v_tab_id,0);window.location.hash='#'+v_tab_id;}
			,
			register_go:function(){js_go_to('user.html');}
			,
			ini:function(){}
		}
		,
		ini:function(){

			this.funx.ini();			
		}
	}
	,
	tabx:{
		select_tab_by_index:function(v_tab_id,v_tab_index){

			var v_uuid_for_group = $('#'+v_tab_id).attr('attr_tab_container_id');
			$('.tab_c_one_item').each(function(i,o){if($(o).attr('attr_group_id')==v_uuid_for_group && $(o).attr('attr_index')==v_tab_index){$(this).trigger('click');}});
 		}
		,
		ini_ex:function(){

			$('.tab_c_one_item').click(function(){
				
				/* get group uuid
				 *
				 */
				var v_uuid_for_group = $(this).attr('attr_group_id');				 

				/* reset
				 *
				 */
				$('.tab_c_one_item').each(function(i,o){if($(o).attr('attr_group_id')==v_uuid_for_group){$(o).css({'background':'url('+js_get_themes_root()+'images/header_featured_tab_bk.png)'});}});
				
				/* selected
				 *
				 */
				$(this).css({'background':'url('+js_get_themes_root()+'images/header_featured_tab_bk_sel.png)'});

				/* switch
				 *
				 */
 				$('.tab_c_content_c_ex').each(function(i,o){if($(o).attr('attr_group_id')==v_uuid_for_group){$(o).hide();}});
				$('#id_tab_content_'+$(this).attr('attr_id')).js_ex().anx.show.scroll.left();
			});
		}
		,
		ini:function(){
		
			$('.hfir_tab').click(function(){
				
				/* reset
				 *
				 */
				$('.hfir_tab').css({'background':'url('+js_get_themes_root()+'images/header_featured_tab_bk.png)'});
				
				/* selected
				 *
				 */
				$(this).css({'background':'url('+js_get_themes_root()+'images/header_featured_tab_bk_sel.png)'});

				/* switch
				 *
				 */
 				$('.hfi_one_item').hide();$('#id_hfi_content_container_'+$(this).attr('attr_id')).js_ex().anx.show.scroll.left();
			});

			/* default selected => 0
			 *
			 */
			$('#id_hfi_content_container_'+$('#id_hfir_tab_default').attr('attr_id')).show('fast');
		}
	}
	,	
	ini:function(){
		
		this.tabx.ini();
		this.tabx.ini_ex();
		this.othx.ini();
		this.menu.ini();
 	}
};(function(){$(window.document).ready(function(){js_core.ini();});})();