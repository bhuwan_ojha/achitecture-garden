var js_paging={
	bg:{
		jp_get_one_page:function(v_class_id,v_cmd_id,v_form_id,v_view_container_id,v_page_index){

 			g_js.js.ajax.post(g_js.js.dispatch_ex_bg(v_class_id,v_cmd_id,'v_page_index='+v_page_index),((v_form_id!='')?$('#'+v_form_id).serialize():''),function(v){

				$('#'+v_view_container_id).html(v);
				if (typeof JT_init == "undefined"){
				} else {
					$(document).ready(JT_init);
				}
			});
		}
	}
	,
	fg:{
		jp_get_one_page:function(v_class_id,v_cmd_id,v_form_id,v_view_container_id,v_page_index){

			g_js.js.ajax.fg.post(v_class_id,v_cmd_id,'#'+v_form_id,'v_page_index='+v_page_index,function(v){$('#'+v_view_container_id).html(v);});
		}
	}
	,
	ini:function(){
	}
};(function(){js_paging.ini();})();