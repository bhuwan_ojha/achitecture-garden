var js_process_shipping = {
	edit_address:function(address_type,address_id){
		if (address_type == '0'){
			g_js.js.ajax.fg.get(5,2,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_shipping_address").html(v);
							});
		} else {
			
			g_js.js.ajax.fg.get(5,2,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_billing_address").html(v);
							});
			
			
		}
	},
	update_cart:function(){
		g_js.js.ajax.fg.post(3,7,"#id_shopping_cart","",function(v){
			if (v < 0){
				$.jq_alert(js_language.cst_product_qty_not_enough);
			}
			js_process_shipping.update_total_price();
			//window.location.href = window.location.href;
		});
	},
	apply_address:function(address_type) {
		if (address_type == '0'){
			var pre = 'shipping';
			var telephone = $('#id_'+pre+'_telephone').val(); 
		} else {
			var pre = 'billing';
		}
		var first_name = $('#id_'+pre+'_first_name').val();
		var last_name = $('#id_'+pre+'_last_name').val();
		var street_address = $('#id_'+pre+'_street_address').val();
		var city = $('#id_'+pre+'_city').val();
		var zone_id = $('#id_'+pre+'_zones_id').val();
		var zip = $('#id_'+pre+'_zipcode').val();
		var country = $('#id_'+pre+'_countries_id').val();
		

		if (first_name.replace(/ /g,'') =='') {
			$.jq_alert(js_language.cst_first_name_required);
			return false;
		}

		if (last_name.replace(/ /g,'') == ''){
			$.jq_alert(js_language.cst_last_name_required);
			return false;
		}

		if (street_address.replace(/ /g,'') == ''){
			$.jq_alert(js_language.cst_street_address_require);
			return false;
		}

		if (city.replace(/ /g,'') == ''){
			$.jq_alert(js_language.cst_city_require);
			return false;
		}

		if (zone_id.replace(/ /g,'') == ''){
			$.jq_alert(js_language.cst_state_require);
			return false;
		}

		if (zip.replace(/ /g,'') == ''){
			$.jq_alert(js_language.cst_zip_require);
			return false;
		}

		if (country.replace(/ /g,'') == ''){
			$.jq_alert(js_language.cst_country_require);
			return false;
		}
		
		if (address_type == '0'){
			if (telephone.replace(/ /g,'') == ''){
				$.jq_alert(js_language.cst_telephone_require);
				return false;
			}
		}
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(5,1,"#id_"+pre+"_address_form","",function(v){
			try{var o = $.parseJSON(v); }catch(ex){}
			if(o!=undefined && o.error!=''){
				g_js.windows.mask.busy.close();
				alert(o.error);
				return;
			}
			var selected = $("#id_same_address").attr("select");
			if (selected == '1'){
				g_js.js.ajax.fg.get(5,16,'',function(v) {
					$("#id_address_info").html(v);
					js_process_shipping.bind_same_address();
//					if (address_type == '0'){
						js_process_shipping.reset_shipping();
//					}
					js_process_shipping.update_total_price();
					g_js.windows.mask.busy.close();
				});
			} else {
				$("#id_"+pre+"_address").html(v);
//				if (address_type == '0'){
					js_process_shipping.reset_shipping();
//				}
				js_process_shipping.update_total_price();
				g_js.windows.mask.busy.close();
			}		
		});
	},
	same_as_shipping:function() {
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.get(5,4,'',function(v) {
						if (v == '-1'){
							$.jq_alert(js_language.cst_save_shipping_info);
						} else if (v =='-2'){
							$.jq_alert(js_language.cst_cant_use_wish_address);
						} else {
							$("#id_shipping_address").html(v);
						}
						if (v < 0){
						} else {
                            $("#id_same_address").addClass('same');
							$("#id_same_address").attr("select","1");
						}
						g_js.windows.mask.busy.close();
					});
	},
	diff_shipping:function() {
		g_js.js.ajax.fg.get(5,2,'address_type=0&address_id=0',function(v) {
                        $("#id_same_address").removeClass('same');
						$("#id_same_address").attr("select","0");
						$("#id_shipping_address").html(v);
							});
	},
	choose_address:function(address_type) {
		if (address_type == '0'){
			var address_id = $('#id_shipping_address_id').val();
			
			if (address_id == '0')	{
				g_js.js.ajax.fg.get(5,2,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_shipping_address").html(v);
							});
			} else {
				g_js.js.ajax.fg.get(5,3,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_shipping_address").html(v);
						js_process_shipping.reset_shipping();
							});
				
			}
		} else {
			var address_id = $('#id_billing_address_id').val();
			if (address_id == '0')	{
				g_js.js.ajax.fg.get(5,2,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_billing_address").html(v);
							});
				
			} else {
				g_js.js.ajax.fg.get(5,3,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_billing_address").html(v);
							});
				
			}
		}
		
	},
	change_country:function(address_type) {
		if (address_type == '0'){
			var country = $("#id_shipping_countries_id").val();
			g_js.js.ajax.fg.get(5,0,'address_type='+address_type+'&country='+country,function(v) {
						$("#id_shipping_change_zones").html(v);
							});
		} else {
			var country = $("#id_billing_countries_id").val();
			g_js.js.ajax.fg.get(5,0,'address_type='+address_type+'&country='+country,function(v) {
						$("#id_billing_change_zones").html(v);
							});
			
			
		}
		
	},
	changeShippingMethod:function(shipping_method){
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.get(5,6,'shipping_method='+shipping_method,function(v) {
						$("#id_shipping_method").html(v);
						js_process_shipping.update_total_price();
						g_js.windows.mask.busy.close();
							});
	},
	reset_shipping:function(){
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.get(5,7,'',function(v) {
						$("#id_shipping_method").html(v);
						g_js.windows.mask.busy.close();
							});
	},
	apply_coupon:function(){
		var couponcode = $("#id_couponcode").val();
//		var couponcode1 = $("#id_couponcode1").val();
//		var couponcode2 = $("#id_couponcode2").val();
		var couponcode_x ='';
		if (couponcode.replace(/ /g,'') != '' ){
			couponcode_x = couponcode;
		}

//		if (couponcode1.replace(/ /g,'') != '' ){
//			couponcode_x = couponcode_x+','+couponcode1;
//		}
//
//		if (couponcode2.replace(/ /g,'') != '' ){
//			couponcode_x = couponcode_x+','+couponcode2;
//		}

		if (couponcode_x.replace(/ /g,'') != ''){
			g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.get(5,8,'couponcode='+couponcode_x,function(v) {
				g_js.windows.mask.busy.close();
				var o = g_js.json.get(v); 
				if (o.error != ''){
					$.jq_alert(o.error);
				}
				$(".id_order_total").html(o.result);
				
			});
		}
	},
	update_total_price:function(){
		g_js.js.ajax.fg.get(5,20,'',function(v) {
			$(".id_order_total").html(v);
			var oID = $("#id_orders_id_paypal").val();
			var address_id = $("#id_address_id").val();
			var action = $("#id_action").val();
			g_js.js.ajax.fg.get(5,22,'oID='+oID+'&address_id='+address_id+'&action='+action,function(v) {
				$("#id_payment_method").html(v);
			});
		});
		
	},
	apply_gift:function(){
		var giftcode = $("#id_giftcode").val();
		var giftpass = $("#id_giftpass").val();

		if (giftcode.replace(/ /g,'') != '' && giftpass.replace(/ /g,'') != '' && giftpass.replace(/ /g,'*') !=''){
			g_js.js.ajax.fg.get(5,9,'giftcode='+giftcode+'&giftpass='+giftpass,function(v) {
						if (v == '-1'){
							$.jq_alert(js_language.cst_gift_error);
						} else {
							$(".id_order_total").html(v);
						}
					});
		}
	},
	apply_reward:function(){
		var reward_points = $("#id_reward_points").val();
		if (!isNaN(reward_points) && reward_points > 0){
			g_js.js.ajax.fg.get(5,10,'reward_points='+reward_points,function(v) {
						if (v == '-1'){
							$.jq_alert(js_language.cst_reward_error);
						} else {
							$(".id_order_total").html(v);
						}
					});
		}
	},
	radio_checked:function () {
		$("#id_payment_method input:radio").each(function(){
		  var i = $(this).attr("rv");
		  if(this.checked){
			  var payment = $(this).val();
			  g_js.js.ajax.fg.get(5,11,'payment='+payment,function(v) {
						if ($('#div_payment_'+i).html().length > 0){
							$('#div_payment_'+i).css("display","block");
						}
					});
		  } else {
			  $('#div_payment_'+i).css("display","none");
		  }
		}); 
	},
	preview_order:function(over5000) {
//		var order_sub_total=$("#order_sub_total").val();
//		var CFG_MINIMUM_CHECKOUT_TOTAL_AMOUNT=$("#CFG_MINIMUM_CHECKOUT_TOTAL_AMOUNT").val();
//		if(CFG_MINIMUM_CHECKOUT_TOTAL_AMOUNT*1 > 0 && order_sub_total*1 < CFG_MINIMUM_CHECKOUT_TOTAL_AMOUNT*1){
//			alert("Wholesale users must have minimum $"+CFG_MINIMUM_CHECKOUT_TOTAL_AMOUNT+" order");
//			return false;
//		}
//		
//		if(over5000==1){
//			if(!window.confirm('Your order has exceeded our average expected demand, you can proceed to place your order, but it may not be processed right away until our engineering team has confirmed its accuracy and lead time')){
//				return false;
//			}
//		}
		return js_process_shipping.check_order_validate();
	},
	check_order_validate:function() {
		var oID='';
		
		if ($('#po_no').val().replace(/ /g,'') =='') {
			alert("PO Number is required.");
			return false;
		}
		
		if ($('#order_type').val().replace(/ /g,'') =='') {
			alert("Order Type is required.");
			return false;
		}
		
		if ($('#first_name').val().replace(/ /g,'') =='') {
			alert("First Name is required.");
			return false;
		}
		
		if ($('#last_name').val().replace(/ /g,'') =='') {
			alert("Last Name is required.");
			return false;
		}
		
		if ($('#email').val().replace(/ /g,'') =='') {
			alert("Email is required.");
			return false;
		}
		
		if($('#accept_agreement').attr('checked')!='checked'){
			alert("You need check the agreement button");
			return ;
		}
		
		g_js.js.ajax.fg.post(5,15,'#checkout_form','',function(v) {
			var o = g_js.json.get(v);
			if (o.type == 0){
				window.location.href = o.result;
				return false;
			}
			else if (o.type == 1){
				var error = eval('js_language.cst_order_error_'+o.result);
				$.jq_alert(error);
			}
			else if (o.type == 2){
				var error = o.result;
				$.jq_alert(error);
			}
			else if(o.type == 3){
				return v_this.pay_go_to_x(o.v_go_to_url);
			}
			else if(o.type == 4){
				return v_this.pay_go_to_x(o.v_go_to_url);
			}
			else {
				$.jq_alert(js_language.cst_order_timeout);
			}
			$("#id_button_confirm").show();
			$("#id_order_loading").hide();
		});
		return ;
		var check = check_form();
		if (check) {
			g_js.js.ajax.fg.post(5,12,"#id_process_payment_form","",function(v){
				if (v > 0){
					var error = eval('js_language.cst_order_error_'+v);
					$.jq_alert(error);
					if (v == 1)	{
						window.location.href = 'process_cart.html';
					} else {
						return false;
					}
				}
				else {
					g_js.js.ajax.fg.get(5,13,'',function(v) {
						var order_preview = v;
						if (v > 0){
							g_js.js.ajax.fg.get(5,14,'',function(v) {
								g_js.windows.html.create_popup(v,function(){},800,600,true);
							});
						}
						else {
							js_process_shipping.confirm_order();
						}
					});
				}
			});
		} else {
			return check;
		}
	},
	pay_go_to_x:function(v){
		window.setTimeout(function(){
			window.location.href=v;
		},1000*1);
		return false;
	}
	,
	confirm_order:function() {
		/* by breezer
		 *
		*/
		var v_this = this;
		$("#id_button_confirm").hide();
		$("#id_order_loading").show();
		var oID = $("#id_orders_id_paypal").val();
		g_js.js.ajax.fg.get(5,15,'old_oID='+oID,function(v) {
			var o = g_js.json.get(v);
			if (o.type == 0){
				window.location.href = o.result;
				return false;
			}
			else if (o.type == 1){
				var error = eval('js_language.cst_order_error_'+o.result);
				$.jq_alert(error);
			}
			else if (o.type == 2){
				var error = o.result;
				$.jq_alert(error);
			}
			else if(o.type == 3){
				return v_this.pay_go_to_x(o.v_go_to_url);
			}
			else if(o.type == 4){
				return v_this.pay_go_to_x(o.v_go_to_url);
			}
			else {
				$.jq_alert(js_language.cst_order_timeout);
			}
			$("#id_button_confirm").show();
			$("#id_order_loading").hide();
		});
	},
	confirm_paypal_order:function(over5000){
		
		if(over5000==1){
			if(!window.confirm('Your order has exceeded our average expected demand, you can proceed to place your order, but it may not be processed right away until our engineering team has confirmed its accuracy and lead time')){
				return false;
			}
		}
		
		$("#id_button_confirm_paypal").hide();
		$("#id_order_loading").show();
		var orders_id = $("#id_orders_id_paypal").val();
		g_js.js.ajax.fg.get(5,21,'orders_id='+orders_id,function(v) {
			var o = g_js.json.get(v);
			if (o.type == 0){
				window.location.href = o.result;
				return false;
			}
			else if(o.type == 1){
				var error = eval('js_language.cst_order_error_'+o.result);
				$.jq_alert(error);
			}
			else if(o.type == 2){
				$.jq_alert(o.result);
			}
			$("#id_button_confirm_paypal").show();
			$("#id_order_loading").hide();
		});
	},
	bind_same_address:function() {
		$("#id_same_address").bind("click",function(){
			var selected = $("#id_same_address").attr("select");
			if (selected != '1'){
				js_process_shipping.same_as_shipping();
			} else {
				js_process_shipping.diff_shipping();
			}
			
		});
	},
	ini:function(){
		js_process_shipping.bind_same_address();
		$("#id_giftpass").focus(function(){
			if($.trim($(this).val())=='******'){
				$(this).val("");
			}
		});
		$("#id_giftpass").blur(function(){
			if($.trim($(this).val())==""){
				$(this).val('******');
			}
		});
	}
};
(function(){js_process_shipping.ini();})();