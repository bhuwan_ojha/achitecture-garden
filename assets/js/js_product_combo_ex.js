var c_combo_product_qucik_view=function(){	
	var pic;
	var optionsx;
}
c_combo_product_qucik_view.prototype.ini=function(v_p_id,v_max_options_value,v_css_class_for_product_thumbnail,v_id_product_img){

	var v_this		= this;
	this.optionsx	= new c_product_options_x()		;this.optionsx	.ini(v_p_id,v_max_options_value,function(o){
		
		/* update ui
		 *
		 */
		$('#id_combo_quick_view_product_name_'+v_this.optionsx.m_n_parent_product_id)	.html(o.products_name);
		$('#id_combo_quick_view_product_price_'+v_this.optionsx.m_n_parent_product_id)	.html(o.child_product_price);

		/* update total combo price
		 *
		 */		 
		js_product_combo_ex.pce_get_total_price();

		/* get current selected child product
		 *
		 */
		v_this.optionsx.m_n_child_product_id;
	});
	this.pic = new c_product_options_pic()	;
	this.pic.ini(v_css_class_for_product_thumbnail,this.optionsx,v_id_product_img);
}
var js_product_combo_ex={
	pce_get_total_price:function(){

		var v_p = 0;
		for(var i=0;i<v_ar_for_combo_quick_view.length;i++){

			var v_parent_product_id = (v_ar_for_combo_quick_view[i].optionsx.m_n_parent_product_id);
			var v_p_ex				= $('#id_combo_quick_view_product_price_'+v_parent_product_id).html();
			v_p_ex					= parseFloat(v_p_ex.replace(/[^0-9,.]/ig,''));
			v_p					   += v_p_ex;
		}
		v_save = $("#id_combo_save_money").val();
		v_p = v_p - parseFloat(v_save);
		v_p = v_p.toFixed(2);
		/* get current currencies
		 *
		 */
		var v_current_currencies = $('#id_combo_product_total_price_ex').html().substring(0,1);
		$('#id_combo_product_total_price_ex').html(v_current_currencies+v_p);
	}
	,
	add_to_cart:function(){

		var ar_ids = [];
		for(var i=0;i<v_ar_for_combo_quick_view.length;i++){

			var o				= v_ar_for_combo_quick_view[i]															;
			var v_product_type	= $('#id_product_type_ex_'+o.optionsx.m_n_parent_product_id).html()						;
			var v_p_id			= (v_product_type==0)?o.optionsx.m_n_parent_product_id:o.optionsx.m_n_child_product_id	;
			
			/* product options is chose ok ?
			 *
			 */
			if( js_is_null(v_p_id)){alert(js_language.cst_please_choose_product_otions);return;}

			/* push to array
			 *
			 */
			ar_ids.push(v_p_id);
		}

		js_cart.add_to_cart($('#id_current_combo_id').html(),ar_ids.join(','),1); 		
	}
	,
	pce_create:function(){
		
		/* is current page in the combo view ?
		 *
		 */
		var v_max_index = 0;if( 0==$('#id_combo_quick_view_max_index').get().length ){return;}else{v_max_index = $('#id_combo_quick_view_max_index').html();}
		
		/* create quick view object
		 *
		 */		 
		for(var i=0 ;i<v_max_index;i++){var o = eval('combo_quick_view_'+i);o();}		
	}
	,
	pce_go_to_combo:function(v_product_id,v_combo_id){

		/* check combo product status 
		 *
		 */
		var v_id_x=$('#id_all_product_id_list_'+v_combo_id).html();v_id_x = v_id_x.replace(/ /g,'');
		if(v_id_x.replace(/ /g,'') !=''){	
			js_cart.add_to_cart(v_combo_id,v_id_x,1);
		}else{
			//alert(v_id_x);
			window.location.href=js_get_web_root()+'product_combo_ex.php?v_product_id='+v_product_id+'&v_combo_id='+v_combo_id;
		}
	}
	,
	ini:function(){this.pce_create();}
};$().ready(function (){js_product_combo_ex.ini();})