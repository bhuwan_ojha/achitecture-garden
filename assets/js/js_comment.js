var js_comment={
	is_login:function(){if(js_get_user_id()==0){js_user.login(window.location.href.replace('#','&'),1);}else{return true;}return false;}
	,
	admin:{
		m_n_current_id:0
		,
		reply:function(v_id){this.m_n_current_id = v_id;g_js.windows.dialog.show('#id_dialog_for_comment_reply');$('#id_comment_reply_title').val('').get(0).focus();}
		,
		deletex:function(v_id){g_js.js.ajax.get(g_js.js.dispatch_ex_bg(33,1,'v_id='+v_id),function(v){$('#id_table_tr_'+v_id).hide('fast');});}
		,
		change_status:function(v_id){
			g_js.js.ajax.get(g_js.js.dispatch_ex_bg(33,3,'v_id='+v_id),function(v){$('#id_table_td_'+v_id+'_3').html(v);});
		},
		ini:function(){
			
			g_js.windows.dialog.create	(
										'#id_dialog_for_comment_reply',
										function() { 
											
											g_js.js.ajax.post(g_js.js.dispatch_ex_bg(33,2,'v_id='+js_comment.admin.m_n_current_id),$('#id_form').serialize(),function(v){

												$('#id_table_td_'+js_comment.admin.m_n_current_id+'_2').html(v);												
											});
										});
		}
	}
	,
	reply_show:function(v_id){
		
		var o = $('#id_reply_container_'+v_id);
		if( o.attr('attr_view_status') == 0 ) {o.show('fast');o.attr('attr_view_status',1);}else{o.hide('fast');o.attr('attr_view_status',0);}
	}
	,
	reply:function(v_id){
		
		var vc = $('#id_form_for_comment_reply_textarea_'+v_id).val();if(''==vc){alert(js_language.cst_reply_store_content_required);return;}else{
			
			g_js.js.ajax.fg.post(8,1,'#id_form_for_comment_reply_'+v_id,'v_id='+v_id,function(v){$('#id_comment_reply_container_'+v_id).html(v);});
		}
	}
	,
	save:function(approval){

		if(js_get_user_id()==0){js_user.login(window.location.href.replace('#','&'),1);}else{
			
			/* comment content is required
			 *
			 */
			if( this.is_login() ){

				/* valid code
				 *
				 */
				//var valid_code = $("#id_valid_code").val();if (valid_code ==''){return false;}
				g_js.windows.mask.busy.show();g_js.js.ajax.fg.post(8,0,'#id_comment_for_product',null,function(v){
					
					g_js.windows.mask.busy.close();var o=g_js.json.get(v);
					if(o.error == 1){
						
						alert(o.result);
					}else{$('#tabs-3').html(o.result);if(!approval){g_js.windows.html.close_popup();
					
					alert(js_language.cst_submit_reviews_sucess);}}
				});
			}
		}
	}
	,
	popup_submit_feedback_ui:function(pid){
		g_js.js.ajax.fg.get(8,6,'pid='+pid,function(v) {
			g_js.windows.html.create_popup(v,function(){},930,550,true,null,null,null,true,null,'Submit Feedback');
		});
	}
	,
	ini:function(){this.admin.ini();}
};$().ready(function(){js_comment.ini();});