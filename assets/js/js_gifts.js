var js_gifts = {
	add_new:function() {
		g_js.js.ajax.get(g_js.js.dispatch_ex_bg(27,2,'gifts_id=0'),function(v){
			g_js.windows.html.create_popup(v,function(){
														$("#gifts_start,#gifts_expire").datepicker({dateFormat: "yy-mm-dd"});
														$("#ui-datepicker-div").css({display:"none"});
														},600,400,true);
		});
	},
	do_edit:function(gifts_id) {
		g_js.js.ajax.get(g_js.js.dispatch_ex_bg(27,2,'gifts_id='+gifts_id),function(v){
			g_js.windows.html.create_popup(v,function(){
														$("#gifts_start,#gifts_expire").datepicker({dateFormat: "yy-mm-dd"});
														$("#ui-datepicker-div").css({display:"none"});
														},600,400,true);
		});
	},
	do_delete:function(gifts_id) {
		if(g_js.windows.confirm.deletex()){
			g_js.js.ajax.get(g_js.js.dispatch_ex_bg(27,3,'gifts_id='+gifts_id),function(v){
									if (v < 0){
										alert(js_language.cst_gifts_remove_error);
									} else {
										window.location.href = window.location.href;
									}
								});
		}
	},
	save_gifts:function() {
		var email_address = $("#email_address").val();
		if (email_address.replace(/ /g,'')==''){
			alert(js_language.cst_gifts_email_address);
			return false;
		}

		if (!(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email_address))) {
			alert(js_language.cst_gifts_email_address_1);
			return false;
		}

		var gifts_values = $("#gifts_values").val();
		if (!js_gifts.checkFloat(gifts_values)){
			alert(js_language.cst_gift_value);
			return false;
		}

		g_js.js.ajax.post(g_js.js.dispatch_bg(27,4),$('#id_gifts_form').serialize(),function(v){
			if (v < 0){
				alert(js_language.cst_gift_code_exists);
				return false;
			}
			window.location.href = window.location.href;
		});
	},
	do_cancel:function() {
		g_js.windows.html.close_popup();
	},
	send_mail:function(gifts_id) {
		g_js.windows.mask.busy.show();
		g_js.js.ajax.get(g_js.js.dispatch_ex_bg(27,5,'gifts_id='+gifts_id),function(v){
			g_js.windows.mask.busy.close();
			if (v < 0){
				alert(js_language.cst_gifts_send_mail_error);
			} else {
				alert(js_language.cst_gifts_send_mail_success);
			}
			window.location.href = window.location.href;
		});
	},
	checkFloat:function(num) {
		var val = parseFloat(num);
		if ( isNaN(val) ) {
			return false;
		}

		if (val < 0) {
			return false;
		} 
		return true;
	},
	radio_checked:function() {
		$("#id_payment_method input:radio").each(function(){
		  var i = $(this).attr("rv");
		  if(this.checked){
			  var payment = $(this).val();
			  g_js.js.ajax.fg.get(10,6,'payment='+payment,function(v) {
						if ($('#div_payment_'+i).html().length > 0){
							$('#div_payment_'+i).css("display","block");
						}
					});
		  } else {
			  $('#div_payment_'+i).css("display","none");
		  }
		}); 

	},
	preview_order:function() {
		var check = check_form();
		if (check) {
			var first_name = $("#id_gift_firstname").val();
			var last_name = $("#id_gift_lastname").val();
			var email_address = $("#id_gift_emailaddress").val();
			var value_int = $("#id_gift_buyvalue").val();

			if (isNaN(value_int) || value_int < 0) {
				alert(js_language.cst_buygift_value_error);
				return false;
			}

			if(first_name.replace(/ /g,'')==''){
				alert(js_language.cst_buygift_firstname_error);
				return false;
			}

			if(last_name.replace(/ /g,'')==''){
				alert(js_language.cst_buygift_lastname_error);
				return false;
			}

			if (email_address.replace(/ /g,'')==''){
				alert(js_language.cst_gifts_email_address);
				return false;
			}

			if (!(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email_address))) {
				alert(js_language.cst_gifts_email_address_1);
				return false;
			}
			
			g_js.js.ajax.fg.post(10,1,"#id_process_payment_form","",function(v){
				var rtn = v;
				if (v > 0){
					var error = eval('js_language.cst_buygift_error_'+v);
					alert(error);
					return false;
				} else {
					g_js.js.ajax.fg.get(10,0,'',function(v) {
										g_js.windows.html.create_popup(v,function(){},800,600,true);
									});
				}
			});
		}
	},
	edit_address:function(address_type,address_id){
		if (address_type == '0'){
			g_js.js.ajax.fg.get(10,3,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_shipping_address").html(v);
							});
		} else {
			
			g_js.js.ajax.fg.get(10,3,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_billing_address").html(v);
							});
			
			
		}
	},
	change_country:function(address_type) {
		if (address_type == '0'){
			var country = $("#id_shipping_countries_id").val();
			g_js.js.ajax.fg.get(10,7,'address_type='+address_type+'&country='+country,function(v) {
						$("#id_shipping_change_zones").html(v);
							});
		} else {
			var country = $("#id_billing_countries_id").val();
			g_js.js.ajax.fg.get(10,7,'address_type='+address_type+'&country='+country,function(v) {
						$("#id_billing_change_zones").html(v);
							});
			
			
		}
		
	},
	apply_address:function(address_type) {
		if (address_type == '0'){
			var pre = 'shipping';
			var telephone = $('#id_'+pre+'_telephone').val(); 
		} else {
			var pre = 'billing';
		}
		var first_name = $('#id_'+pre+'_first_name').val();
		var last_name = $('#id_'+pre+'_last_name').val();
		var street_address = $('#id_'+pre+'_street_address').val();
		var city = $('#id_'+pre+'_city').val();
		var zone_id = $('#id_'+pre+'_zones_id').val();
		var zip = $('#id_'+pre+'_zipcode').val();
		var country = $('#id_'+pre+'_countries_id').val();
		

		if (first_name.replace(/ /g,'') =='') {
			alert(js_language.cst_first_name_required);
			return false;
		}

		if (last_name.replace(/ /g,'') == ''){
			alert(js_language.cst_last_name_required);
			return false;
		}

		if (street_address.replace(/ /g,'') == ''){
			alert(js_language.cst_street_address_require);
			return false;
		}

		if (city.replace(/ /g,'') == ''){
			alert(js_language.cst_city_require);
			return false;
		}

		if (zone_id.replace(/ /g,'') == ''){
			alert(js_language.cst_state_require);
			return false;
		}

		if (zip.replace(/ /g,'') == ''){
			alert(js_language.cst_zip_require);
			return false;
		}

		if (country.replace(/ /g,'') == ''){
			alert(js_language.cst_country_require);
			return false;
		}
		
		if (address_type == '0'){
			if (telephone.replace(/ /g,'') == ''){
				alert(js_language.cst_telephone_require);
				return false;
			}
		}

		g_js.js.ajax.fg.post(10,2,"#id_process_payment_form","",function(v){
			$("#id_"+pre+"_address").html(v);
			if (address_type == '0'){
				js_process_shipping.reset_shipping();
			}
		});
	},
	choose_address:function(address_type) {
		if (address_type == '0'){
			var address_id = $('#id_shipping_address_id').val();
			
			if (address_id == '0')	{
				g_js.js.ajax.fg.get(10,3,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_shipping_address").html(v);
							});
			} else {
				g_js.js.ajax.fg.get(10,4,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_shipping_address").html(v);
						js_process_shipping.reset_shipping();
							});
				
			}
		} else {
			var address_id = $('#id_billing_address_id').val();
			if (address_id == '0')	{
				g_js.js.ajax.fg.get(10,3,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_billing_address").html(v);
							});
				
			} else {
				g_js.js.ajax.fg.get(10,4,'address_type='+address_type+'&address_id='+address_id,function(v) {
						$("#id_billing_address").html(v);
							});
				
			}
		}
		
	},
	confirm_order:function() {
		$("#id_button_confirm").hide();
		$("#id_order_loading").show();
		g_js.js.ajax.fg.get(10,5,'',function(v) {
			var o = g_js.json.get(v); 
			if (o.type == 0){
				window.location.href = o.result;
				return false;
			} else if (o.type == 1){
				var error = eval('js_language.cst_order_error_'+o.result);
				alert(error);
			} else if (o.type == 2){
				var error = o.result;
				alert(error);
			} else {
				alert(js_language.cst_order_timeout);
			}
		});
	},
	order_search:function() {
		g_js.js.ajax.post(g_js.js.dispatch_bg(36,1),$('#id_form_view').serialize(),function(v){
			$('#id_cts_container').html(v);
		});
	},
	edit_order:function(oid) {
		g_js.js.ajax.get(g_js.js.dispatch_ex_bg(36,2,'oid='+oid),function(v){
										g_js.windows.html.create_popup(v,function(){},$("#id_cts_container").width()/2,360,true);
									});
	},
	save_order_status:function(oid) {
		$("#id_button_confirm").hide();
		$("#id_order_loading").show();
		g_js.js.ajax.post(g_js.js.dispatch_ex_bg(36,3,'oid='+oid),$('#form_order_status_edit').serialize(),function(v){
			g_js.windows.html.close_popup();
			alert(v);
			js_gifts.order_search();
		});
	},
	delete_order:function(oid) {
		if(g_js.windows.confirm.deletex()){
			g_js.js.ajax.get(g_js.js.dispatch_ex_bg(36,4,'oid='+oid),function(v){
				if(v.length>0){
					alert(v);
				}
				js_gifts.order_search();
			});
		}
	},
	ini:function() {
	}
};
(function(){js_gifts.ini();})();