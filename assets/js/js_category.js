var js_category={
	change_pagex:0,
	popup_video:function(url){
		var obj_video = '<div style="text-align:center;width:640px;height:363px;"><object width="640" height="363"><param name="movie" value="'+url+'"></param>	<param name="wmode" value="transparent"></param><embed src="'+url+'" type="application/x-shockwave-flash" wmode="transparent" width="640" height="363"></embed></object></div>'
		g_js.windows.html.create_popup(obj_video,function(){
		},640,386,true);
	}
	,
	change_product_image:function(obj,pid,pimg){
		var width = 180;
		var height = 160;
		g_js.js.ajax.fg.get(2,2,'&pid='+pid+'&pimg='+pimg+'&width='+width+'&height='+height,function(v) {
			$("#id_products_image_"+pid).html(v);
			$('#wish_heart_'+pid).attr('rv',$(obj).attr('rv'));
		});

		//$("#products_image_"+pid).attr("width","210").attr("height","210").attr("src",pimg);

	},
    lazyload:function(){
		
		$(window.document).ready(function () {
	        $(".lazyimg").lazyload({
	            effect:"fadeIn",
	            threshold: 500
	        });
	
	        $('.product_items_grid').hover(
    	            function(){
    	                $(this).css('z-index','100');
    	                
    	                $(this).find('.product_option_button').addClass('shadow');
    	                $(this).find('.product_option_button').stop().animate({
    	                    bottom:'-39px',
    	                    opacity:'1'
    	                  },500,function(){
    	                	  //$(this).css('z-index','100');
    	                  });
    	                
    	            },
    	            function(){
    	                $(this).css('z-index','0');
    	                $(this).find('.product_option_button').removeClass('shadow');
    	                $(this).find('.product_option_button').stop().animate({
    	                    bottom:'10px',
    	                    opacity:'1'
    	                  },500,function(){
    	                	  //$(this).css('z-index','100');
    	                  });
    	            }
    	   );
	        
	        
	        
	        setTimeout(function(){$('.msdropdown').msDropdown();}, 200);
	
	        js_category.ajust_height();
	        
	        $('.wish_heart').hover(function(){$(this).addClass('hover').parent().parent().find('.wish_tips').fadeIn();},function(){$(this).removeClass('hover').parent().parent().find('.wish_tips').slideUp();});
	        $('.list_star').hover(function(){$(this).find('.review_tips').fadeIn();},function(){$(this).find('.review_tips').slideUp();});
	        $('.checkbox.compare').hover(function(){$(this).find('.compare_tips').fadeIn();},function(){$(this).find('.compare_tips').slideUp();});
	        
	        //js_category.setlastgriditem();
		});
    }
	,
	display_counts:function(v){
		$("#perpage").val(v);
		$("#page").val(0);
		js_category.search_filter_refresh("perpage");
	}
	,
	switch_display_mode:function(mode){		
		var products_list_dispaly_mode = $("#display_mode").val();
		var v = 0;
		if(products_list_dispaly_mode==mode) return;
		v = products_list_dispaly_mode==1?0:1;
		$("#display_mode").val(v);
		
		g_js.js.ajax.fg.get(2,7,'&display_mode='+v,function(v) {
			js_category.search_filter_refresh();
		});
		
	}
	,
	change_page:function(obj){
		if($(obj).attr("p")==-1) return;
		$("#page").val($(obj).attr("p"));
		this.change_pagex = 1;
		js_category.search_filter_refresh();
	}
	,
	gp:function(page){
		if(isNaN(page) || page<=0) return false;
		page = page - 1;
		$("#page").val(page);
		js_category.search_filter_refresh();
	}
	,
	get_html_filter_item:function(fl,id_name,aid,fv,filter_title,filter_text){
		//Process Double Quotes
		ft = filter_text.replace(/`/g,"\"");
		var html_filter_item = '<div id="filter_'+id_name+'" class="search_filters_item" aid="'+aid+'" fv="'+fv+'"><div class="search_filters_item_l">'+filter_title+' '+ft+'</div> <div class="search_filters_item_r"><div class="search_filters_item_close_ico" onclick="javascript:js_category.remove_filter'+(fl==1?'1':'')+'(this);" onmouseover="js_category.close_ico_toggle(this,0);" onmouseout="js_category.close_ico_toggle(this,1);"></div></div></div>';
		return html_filter_item;
	}
	,
	close_ico_toggle:function(obj,v){
		if(v==0){
			$(obj).parent().parent().addClass("hover");
		}else{
			$(obj).parent().parent().removeClass("hover");
		}
	}
	,
	search_filter_refresh:function(para_type,obj){
		//consctruct sql condition
		var id,v,fv,arr_fv;
		var condition = "";
		var para = "";
        var para_byname = "";
        var para_bymodel = "";
        var para_bypopularity = "";
        var para_bysort = "";
		var para_bybrand = "";
		var para_byoption = "";
		var para_bymanufacturer = "";
		var para_byprice = "";
		var para_bycolor = "";
		var para_bytrend = "";
		var para_user_ratings = "";
		var para_customize_attribute = "";
		var tables = "";
		var para_page = 0;
		if($(".cascading_filter").length==0){
			$(".search_filters_item").each(function(idx,o){
				id = $(o).attr("id");
				v = id.substr(id.lastIndexOf("_")+1);
				//if(isNaN(v) || (v==0 && id.indexOf("byprice")===false)) return true;

                if(id.indexOf("byname")>0){
                    v = encodeURIComponent($(o).attr("fv"));
                    para_byname += v + "^^";
                }else if(id.indexOf("bymodel")>0){
                    v = encodeURIComponent($(o).attr("fv"));
                    para_bymodel += v + "^^";
                }
                else if(id.indexOf("bypopularity")>0){
                    v = 'true';
                    para_bypopularity += v + "^^";
                }else if(id.indexOf("bysort")>0){
                	v = encodeURIComponent($(o).attr("fv"));
                    para_bysort += v + "^^";
                }
				else if(id.indexOf("bybrand")>0){
					para_bybrand += v + "^^";
				}else if(id.indexOf("byoption")>0){
					para_byoption += v + "^^";
				}else{
					if(id.indexOf("bymanufacturer")>0){
						para_bymanufacturer += v + "^^";
					}else{
						if(id.indexOf("byprice")>0){
							fv = $(o).attr("fv");
							arr_fv = fv.split(",");
							if(arr_fv.length==2){
								price_from = arr_fv[0];
								price_to = arr_fv[1];
								para_byprice += price_from + "~~" + price_to + "^^";
							}				
						}else{
							if(id.indexOf("user_ratings_line")>0){
								para_user_ratings += v + "^^";
							}else{
								if(id.indexOf("bycolor")>0){
									para_bycolor += v + "^^";			
								}else{
									if(id.indexOf("bytrend")>0){
										para_bytrend += $(o).attr("fv") + "^^";			
									}else{
										//customize attribute
										fv = $(o).attr("fv");
										//Process Double Quotes
										//fv = fv1.replace(/`/g,'"');
										arr_fv = fv.split(":");
										if(arr_fv.length==2){
											para_customize_attribute += arr_fv[0] + "~~" + arr_fv[1] + "^^";
										}
									}
								}
							}
						}
					}
				}
			});

            if(para_byname.length>0){
                para += "@@byname::" + para_byname.substr(0,para_byname.lastIndexOf("^^"));
            }
            if(para_bymodel.length>0){
                para += "@@bymodel::" + para_bymodel.substr(0,para_bymodel.lastIndexOf("^^"));
            }
            if(para_bypopularity.length>0){
                para += "@@bypopularity::" + para_bypopularity.substr(0,para_bypopularity.lastIndexOf("^^"));
            }
            if(para_bysort.length>0){
                para += "@@bysort::" + para_bysort.substr(0,para_bysort.lastIndexOf("^^"));
            }
			if(para_bybrand.length>0){
				para += "@@bybrand::" + para_bybrand.substr(0,para_bybrand.lastIndexOf("^^"));
			}
			if(para_byoption.length>0){
				para += "@@byoption::" + para_byoption.substr(0,para_byoption.lastIndexOf("^^"));
			}
			if(para_bymanufacturer.length>0){
				para += "@@bymanufacturer::" + para_bymanufacturer.substr(0,para_bymanufacturer.lastIndexOf("^^"));
			}
			if(para_byprice.length>0){
				para += "@@byprice::" + para_byprice.substr(0,para_byprice.lastIndexOf("^^"));
			}
			if(para_bycolor.length>0){
				para += "@@bycolor::" + para_bycolor.substr(0,para_bycolor.lastIndexOf("^^"));
			}
			if(para_bytrend.length>0){
				para += "@@bytrend::" + para_bytrend.substr(0,para_bytrend.lastIndexOf("^^"));
			}
			if(para_user_ratings.length>0){
				para += "@@user_ratings::" + para_user_ratings.substr(0,para_user_ratings.lastIndexOf("^^"));
			}
			if(para_customize_attribute.length>0){
				para += "@@customize_attribute::" + para_customize_attribute.substr(0,para_customize_attribute.lastIndexOf("^^"));
			}
			if(para.length>0){
				para = para.substr(para.indexOf("@@")+2);
			}
			$("#search_condition").html(para);

			if((para.length>0 || para_type=="perpage") && this.change_pagex==0){
				para_page = 0;
			}else{
				para_page = $("#page").val();
				this.change_pagex=0;
			}
			var thisURL = document.location.href;
			var index_of_function;

			//tony change url
			var url_parameters = '';
			var base_url = '';

			if(thisURL.indexOf("advanced_search_result.html")>0 || thisURL.indexOf("search_")>0){
				index_of_function = 1;				
				condition = "cpath="+$("#cpath").val()+"&para="+para;
				//tony change url
				if(thisURL.indexOf("&")>0){
					var base_urls = thisURL.substr(0,thisURL.indexOf('&')).replace('http://','').split('index.html');
				}
				else if(thisURL.indexOf("|")>0){
					var base_urls = thisURL.substr(0,thisURL.indexOf('|')).replace('http://','').split('index.html');
				}
				base_url += 'http://';
				for(var urli=0;urli<base_urls.length;urli++){
					base_url += base_urls[urli];
					if(urli < base_urls.length - 1){
						base_url += "index.html";
					}
				}
			}
			else{
				index_of_function = 0;
				var landing_page_ex_id = $("#landing_page_ex_id").val();
				condition = "para="+para+"&landing_page_ex_id="+landing_page_ex_id;
				//tony change url
				condition = "para="+para;
			}

			//tony change url
			url_parameters = condition.replace(/::/g,"-");
			url_parameters = url_parameters.replace(/@@/g,"--");
			url_parameters = url_parameters.replace(/\^\^/g,"-");
			url_parameters = url_parameters.replace(/para=/g,"");
			if(url_parameters == 'cpath=undefined&'){
				url_parameters = '';
			}
			if(thisURL.indexOf("advanced_search_result.html")>0){
				url_parameters += "perpage="+$("#perpage").val()+'&';
				url_parameters += "page="+$("#page").val();
				var goto_url = base_url + '&' + url_parameters;
			}
			else{
				if(url_parameters != ''){
					url_parameters += '--';
				}
				url_parameters += "perpage-"+$("#perpage").val()+'--';
				url_parameters += "page-"+$("#page").val();
				if(thisURL.indexOf("--")>0){
					base_url = thisURL.split('--')[0];
					var goto_url = base_url + '--' + url_parameters + '.html';
				}
				else{
					base_url = thisURL;
					var goto_url = base_url.replace('.html','') + '--' + url_parameters + '.html';
				}
			}
			
			document.location.href = goto_url;
			return false;

			
			g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.post(2,index_of_function,"#form_rv",condition,function(v){
				$("#products_list_content").html(v);
				//re-bind events
				js_category.pagenav_bind_events();
				g_js.windows.mask.busy.close();
			});

			if($("#search_filters").find(".search_filters_item").length>0){
				$("#search_filters").show();
			}
			else{
				$("#search_filters").hide();
			}
		}
		else{
			//cascading
			$(".search_filters_item").each(function(idx,o){
				id = $(o).attr("id");
				v = id.substr(id.lastIndexOf("_")+1);
				if(isNaN(v) || (v==0 && id.indexOf("byprice")===false)) return true;

				if(id.indexOf("bybrand")>0){
					para_bybrand += v + "^^";
				}else{
					if(id.indexOf("bymanufacturer")>0){
						para_bymanufacturer += v + "^^";
					}else{
						if(id.indexOf("byprice")>0){
							fv = $(o).attr("fv");
							arr_fv = fv.split(",");
							if(arr_fv.length==2){
								price_from = arr_fv[0];
								price_to = arr_fv[1];
								para_byprice += price_from + "~~" + price_to + "^^";
							}				
						}else{
							if(id.indexOf("user_ratings_line")>0){
								para_user_ratings += v + "^^";
							}else{
	//							//customize attribute
								fv = $(o).attr("fv");
								//Process Double Quotes
	//							fv = fv1.replace(/`/g,'"');
								arr_fv = fv.split(":");
								if(arr_fv.length==2){
									para_customize_attribute += arr_fv[0] + "~~" + arr_fv[1] + "^^";
								}
							}
						}
					}
				}
			});

			if(para_bybrand.length>0){
				para += "@@bybrand::" + para_bybrand.substr(0,para_bybrand.lastIndexOf("^^"));
			}
			if(para_bymanufacturer.length>0){
				para += "@@bymanufacturer::" + para_bymanufacturer.substr(0,para_bymanufacturer.lastIndexOf("^^"));
			}
			if(para_byprice.length>0){
				para += "@@byprice::" + para_byprice.substr(0,para_byprice.lastIndexOf("^^"));
			}
			if(para_user_ratings.length>0){
				para += "@@user_ratings::" + para_user_ratings.substr(0,para_user_ratings.lastIndexOf("^^"));
			}
			if(para_customize_attribute.length>0){
				para += "@@customize_attribute::" + para_customize_attribute.substr(0,para_customize_attribute.lastIndexOf("^^"));
			}
			if(para.length>0){
				para = para.substr(para.indexOf("@@")+2);
			}
			$("#search_condition").html(para);

//			if($("#cnid").val()=="" || $("#cnid").val() != para_type){
//				if(!isNaN(para_type) && para_type>0){
//					$("#cnid").val(para_type);
//				}else{
//					$("#cnid").val(0);
//				}
//			}
			
			if(!isNaN(para_type) && para_type>0){
				$("#cnid").val(para_type);
			}else{
				if($("#cnid").val()==""){
					$("#cnid").val(0);
				}
			}
			var cnid = $("#cnid").val();
			if(cnid>=0){
				var obj_class = $(obj).attr("class");
				var para_page = 0;
				if(obj_class=="cascading_search" || obj_class=="cascading_reset"){
					para_page = 0;					
				}else{
					para_page = $("#page").val();
				}		

				var condition = "cpath="+$("#cpath").val()+"&cid="+$("#cid").val()+"&sort_by="+$("#sort_by").val()+"&perpage="+$("#perpage").val()+
									"&display_mode="+$("#display_mode").val()+"&page="+para_page+"&para="+para+"&cnid="+cnid;
//				$.jq_alert(condition);
				g_js.js.ajax.fg.post(2,4,"#form_cascading_"+cnid,condition,function(v){
					$("#products_list_content").html(v);
					js_category.pagenav_bind_events();
				});
			}
		}//end if($(".cascading_filter")==0)

	}
	,
	pagenav_bind_events:function(){
		$(".dd_sort_by").bind("change",function(){
				$("#sort_by").val($(this).val());
				js_category.search_filter_refresh();
		});
		$(".prev_page").hover(
			function(){
				if($(this).attr("p")==-1) return;
				$(this).addClass("hover");
			},
			function(){
				if($(this).attr("p")==-1) return;
				$(this).removeClass("hover");
		});
		$(".next_page").hover(
			function(){
				if($(this).attr("p")==-1) return;
				$(this).addClass("hover");
			},
			function(){
				if($(this).attr("p")==-1) return;
				$(this).removeClass("hover");
		});
		$(".product_img,.hot_product_img").hover(
			function(){
				$(this).find(".grid_quick_view").show();
			},
			function(){
				$(this).find(".grid_quick_view").hide();
		});
		$(".product_img_list").hover(
			function(){
				$(this).find(".list_quick_view").show();
			},
			function(){
				$(this).find(".list_quick_view").hide();
		});
		
		
		setTimeout(js_category.lazyload,200);
	}
	,
	generate_search_filter:function(obj){
		var attribute_name = $(obj).parent().parent().parent().prev().prev().text();		
		var filter_title = attribute_name.substr(attribute_name.indexOf(" ")+1);		

		if($(obj).attr("sel")==1){
			//unselect
			var filter_text = $(obj).attr("ft");
			if(filter_text==undefined){
				$(".search_filters_item").each(function(idx,o){
					if($(o).attr("aid")==$(obj).attr("aid")){
						$(o).remove();
					}
				});
			}else{
				$("#filter_" + $(obj).attr("id")).remove();
			}
		}else{
			var html_filter_item = "";
			var filter_text = $(obj).attr("ft");
			var id_name = "";			
			if(filter_text==undefined){
				//click select all
				//remove filter item of the same category
				$(".search_filters_item").each(function(idx,o){
					if($(o).attr("aid")==$(obj).attr("aid")){
						$(o).remove();
					}
				});
				var aid = $(obj).attr("aid");
				
				$(obj).parent().parent().find(".category_attribute_value_line").each(function(idx,o){
					if(idx > 0){
						filter_text = $(o).attr("ft");
						filter_value = $(o).attr("fv");
						if(filter_value==undefined){
							filter_value="";
						}
						id_name = $(o).attr("id");
						html_filter_item += js_category.get_html_filter_item(0,id_name,aid,filter_value,filter_title,filter_text);
					}
				});
			}else{
				if($(obj).attr("class")=="by_price_line"){
					var s = "";
					$(".search_filters_item").each(function(idx,o){
						s = $(o).attr("id");
						if(s.indexOf("filter_byprice_")!=-1){
							$(o).remove();
						}
					});
				}

				if($(obj).attr("class")=="user_ratings_line"){
					var s = "";
					$(".search_filters_item").each(function(idx,o){
						s = $(o).attr("id");
						if(s.indexOf("filter_user_ratings_line_")!=-1){
							$(o).remove();
						}
					});
				}

				if($(obj).attr("class")=="by_color_line"){
					var s = "";
					$(".search_filters_item").each(function(idx,o){
						s = $(o).attr("id");						
					});
					filter_title = 'Color';
				}

				if($(obj).attr("class")=="by_trend_line"){
					var s = "";
					$(".search_filters_item").each(function(idx,o){
						s = $(o).attr("id");						
					});
					filter_title = 'Trend';
				}

				filter_text = $(obj).attr("ft");
				filter_value = $(obj).attr("fv");
				if(filter_value==undefined){
					filter_value="";
				}
				id_name = $(obj).attr("id");
				var aid = $(obj).attr("aid");
				html_filter_item += js_category.get_html_filter_item(0,id_name,aid,filter_value,filter_title,filter_text);	
			}
			$("#search_filters").append(html_filter_item);
		}//end of if($(obj).attr("sel")==1){
		js_category.search_filter_refresh();
	}//end of function
	,
	remove_filter:function(obj){		
		var id1 = $(obj).parent().parent().attr("id"); 
		var id2 = id1.substr(7);
		$("#"+id2).trigger("click");
		$(obj).parent().parent().remove();
		js_category.search_filter_refresh();
	}
	,
	generate_search_filter1:function(obj){
		var id = $(obj).attr("id");
		var v = $(obj).val();
		var id_name = "";

		if(id=="by_brand"){
			var s = "";
			$(".search_filters_item").each(function(idx,o){
				s = $(o).attr("id");
				if(s.indexOf("filter_bybrand_")!=-1){
					$(o).remove();
				}
			});
			id_name = "bybrand_"+v;
		}else{
			if(id=="by_manufacturer"){
				var s = "";
				$(".search_filters_item").each(function(idx,o){
					s = $(o).attr("id");
					if(s.indexOf("filter_bymanufacturer_")!=-1){
						$(o).remove();
					}
				});
				id_name = "bymanufacturer_"+v;
			}
		}

		var html_filter_item = "";		
		var aid;
		var attribute_name = $(obj).parent().prev().prev().text();
		var filter_title = attribute_name.substr(attribute_name.indexOf(" ")+1);
		var filter_text = $("#"+id+" option:selected").text();
		html_filter_item += js_category.get_html_filter_item(1,id_name,aid,"",filter_title,filter_text);

		$("#search_filters").append(html_filter_item);

		js_category.search_filter_refresh();
	}
	,
	remove_filter1:function(obj){
		var id1 = $(obj).parent().parent().attr("id"); 
		if(id1.indexOf("bybrand")>1){
			$("#by_brand").val(0);
		}else{
			if(id1.indexOf("bymanufacturer")>1){
				$("#by_manufacturer").val(0);
			}
		}

		$(obj).parent().parent().remove();
		js_category.search_filter_refresh();
	}
	,
	filter_collapse_clicked:function(obj){
		if($(obj).attr("rv")=="0"){
			$(obj).addClass("e").attr("rv","1").parent().next().next().slideDown();
		}else{
			$(obj).removeClass("e").attr("rv","0").parent().next().next().slideUp();
		}
	}
	,
	cv_change:function(obj,cnid,cvid){
		$("#cascadingvalue_"+cnid+"_"+(cvid+1)).hide();
		$("#cascadingloader_"+cnid+"_"+(cvid+1)).show();

		var cn_cnt = 5;
		var cv = $(obj).val();
		var sel;
		for(var i=(cvid+1);i<=cn_cnt;i++){
			sel = $("#cascadingvalue_"+cnid+"_"+i);
			if(sel.length>0){
				$("#cascadingvalue_"+cnid+"_"+i).empty();
			}
		}

		g_js.js.ajax.fg.post(2,3,"#form_cascading_"+cnid,"cnid="+cnid+"&cvid="+cvid,function(v){
			$("#cascadingloader_"+cnid+"_"+(cvid+1)).hide();
			$("#cascadingvalue_"+cnid+"_"+(cvid+1)).show();
			if(v.length>0){
				$(v).appendTo("#cascadingvalue_"+cnid+"_"+(cvid+1));
			}
		});
	}
	,
	cascading_reset:function(cnid,obj){
		$.each($("#form_cascading_"+cnid+" select"),function(i,n){
			if(i==0){
				$(n).val(0);
			}else{
				$(n).empty();
			}
		});
		js_category.search_filter_refresh(cnid,obj);
	}
	,
	select_color:function(obj){
		js_category.generate_search_filter(obj);
		if($(obj).hasClass('selected')){
			$(obj).removeClass('selected');
			$(obj).attr("sel","0");
		}else{
			$(obj).addClass('selected');
			$(obj).attr("sel","1");
		}			
	},
	select_trend:function(obj){
		js_category.generate_search_filter(obj);
		if($(obj).hasClass('selected')){
			$(obj).removeClass('selected');
			$(obj).attr("sel","0");
		}else{
			$(obj).addClass('selected');
			$(obj).attr("sel","1");
		}			
	},
	show_all_color:function(){
		$('.by_price_list.by_color').addClass('full');
		$('.show_all_color').remove();
	},
    init_filter:function(){

//        $('.btn_ext').click(function(){
//            if($(this).parent().find('.ext_list').is(':hidden')){
//            	$(this).parent().find('.ext_list').show();
//            }else{
//            	$(this).parent().find('.ext_list').hide();
//            }
//        });
        
        $('.filter_div').click(function(){
            if($(this).find('.ext_list').is(':hidden')){
            	$(this).find('.ext_list').show();
            }else{
            	$(this).find('.ext_list').hide();
            }
        });

        $('.filter_item').click(function(){
        	$(this).parent().parent().parent().find('.ext_content').hide();
            var cid = $(this).attr('cid');
            $('#'+cid).show();
        });

        $('.btn_ext_active_icon').click(function(){
            $(this).parent().parent().parent().find('.ext_content').hide();
        });


        $('.btn_ext_active_back').click(function(){
            $(this).parent().parent().parent().find('.ext_content').hide();
            $(this).parent().parent().parent().find('.ext_list').show();
        });

        $('.f_item').click(function(){
            if($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }else{
                $(this).addClass('selected');
            }
        });
    },
    filter_search:function(obj){
        js_category.add_search_filter(obj);
    },
    get_filter_count:function(ftype){
        var count = 0;
        $('.search_filters_item').each(function(){
            s = $(this).attr("id");
            if(s.indexOf(ftype)!=-1){
                count++;
            }
        });

        return count;
    },
    filter_exist:function(id){
        if($('#'+id).length>0){
            return 'true';
        }
        return 'false';
    },
    add_search_filter:function(obj){
        var html_filter_item = "";
        var aid;
        var id_name = "byprice_0";
        var filter_title = "Price: "
        var filter_text = '';
        var fv = '';

        var ftype = $(obj).attr('ftype');
        var count = js_category.get_filter_count(ftype);

        if(ftype=='by_brand'){
            filter_title = 'Brand: ';
            $('.by_brand').each(function(){
                var vid = $(this).attr('vid');
                filter_text = $(this).text();
                id_name = 'bybrand_'+vid;
                fv = vid;
                if(js_category.filter_exist('filter_'+id_name)=='true'){
                    if(!$(this).hasClass('selected')){
                        $('#filter_'+id_name).remove();
                    }
                }else{
                    if($(this).hasClass('selected')){
                        html_filter_item += js_category.get_html_filter_item(0,id_name,aid,fv,filter_title,filter_text);
                    }
                }

            });
            fv = filter_text;
            if(filter_text==''){
                return false;
            }
        }else if(ftype=='by_option'){
            filter_title = $(obj).attr('filter_title')+': ';
            var opid = $(obj).attr('opid');
            $('.by_option'+opid).each(function(){
                var vid = $(this).attr('vid');
                filter_text = $(this).text();
                id_name = 'byoption_'+vid;
                fv = vid;
                if(js_category.filter_exist('filter_'+id_name)=='true'){
                    if(!$(this).hasClass('selected')){
                        $('#filter_'+id_name).remove();
                    }
                }else{
                    if($(this).hasClass('selected')){
                        html_filter_item += js_category.get_html_filter_item(0,id_name,aid,fv,filter_title,filter_text);
                    }
                }

            });
            fv = filter_text;
            if(filter_text==''){
                return false;
            }
        }else if(ftype=='by_name'){
            id_name = 'byname_0';
            filter_title = 'Name: ';
            filter_text = $(obj).parent().parent().find('.filter_keyword').val();
            fv = filter_text;
            if(js_category.filter_exist('filter_'+id_name)=='true'){
                if(!$(this).hasClass('selected')){
                    $('#filter_'+id_name).remove();
                }
            }
            if(filter_text==''){
                return false;
            }
            html_filter_item += js_category.get_html_filter_item(0,id_name,aid,fv,filter_title,filter_text);
        }else if(ftype=='by_model'){
            id_name = 'bymodel_0';
            filter_title = 'Model: ';
            filter_text = $(obj).parent().parent().find('.filter_keyword').val();
            fv = filter_text;
            if(js_category.filter_exist('filter_'+id_name)=='true'){
                if(!$(this).hasClass('selected')){
                    $('#filter_'+id_name).remove();
                }
            }
            if(filter_text==''){
                return false;
            }
            html_filter_item += js_category.get_html_filter_item(0,id_name,aid,fv,filter_title,filter_text);
        }else if(ftype=='by_popularity'){
            id_name = 'bypopularity_0';
            filter_title = 'Popularity';
            html_filter_item += js_category.get_html_filter_item(0,id_name,aid,fv,filter_title,filter_text);
        }else if(ftype=='by_sort'){
            id_name = 'bysort_0';
            filter_title = $(obj).attr('filter_title');
            fv = $(obj).attr('fv');
            if(js_category.filter_exist('filter_'+id_name)=='true'){
            	$('#filter_'+id_name).remove();
            }
            html_filter_item += js_category.get_html_filter_item(0,id_name,aid,fv,filter_title,filter_text);
        }else if(ftype=='by_price'){
            id_name = 'byprice_0';
            filter_title = 'Price: ';
            
            if($('#price_min').val()=='' || $('#price_max').val()==''){
            	alert('Please input filter price.');
            	return false;
            }
            
            price_from = parseFloat($('#price_min').val());
            price_to = parseFloat($('#price_max').val());
            
            
            filter_text = "$"+price_from+" to $"+price_to;;
            fv = filter_text;
            if(js_category.filter_exist('filter_'+id_name)=='true'){
                if(!$(this).hasClass('selected')){
                    $('#filter_'+id_name).remove();
                }
            }
            if(filter_text==''){
                return false;
            }
            html_filter_item += js_category.get_html_filter_item(0,id_name,aid,price_from+','+price_to,filter_title,filter_text);
        }

        if(ftype=='by_sort'){
        	$("#search_sort_items").append(html_filter_item);
        }else{
        	$("#search_filter_items").append(html_filter_item);
        }

        js_category.ajust_height();

        js_category.search_filter_refresh();

        $('.ext_content').hide();
        $('.ext_list').hide();
        $('.filter_keyword').val('');

    },
    ajust_height:function(){
        var filter_h = $('.filter_div').height();
        if(filter_h>40){
            $('.page_nav_wrapper').css('margin-bottom',(filter_h-40)+'px');
        }
    },
    setlastgriditem:function(){
    	$('.product_list_grid .product_items_grid, #featured_content .product_items_grid, #new_content .product_items_grid').removeClass('last_item');;
		$('.product_list_grid .product_items_grid, #featured_content .product_items_grid').each(function(index,element){
			var winW = $(window).width();
			if((index+1)%4==0 && winW>1000){
				$(this).addClass('last_item');
			}
		});
		
		$('#new_content .product_items_grid').each(function(index,element){
			var winW = $(window).width();
			if((index+1)%4==0 && winW>1000){
				$(this).addClass('last_item');
			}
		});
    },
	ini:function(){
		$(".category_attribute_value_line").click(function(){
			js_category.generate_search_filter(this);

			if($(this).attr("ft")==undefined){
				//Select All
				if($(this).attr("sel")==0){
					$(this).parent().parent().find(".category_attribute_value_line").attr("sel","1").css("background-position","0px -21px").css("color","#4d4d4d");
				}else{
					$(this).parent().parent().find(".category_attribute_value_line").attr("sel","0").css("background-position","0px 0px").css("color","#666666");
				}
			}else{
				if($(this).attr("sel")==1){
					$(this).attr("sel","0").css("background-position","0px 0px").css("color","#666666");
				}else{					
					$(this).attr("sel","1").css("background-position","0px -21px").css("color","#4d4d4d");
				}
				
				//Dealing with the "the select all" button of the state
				var select_all = true;
				$(this).parent().parent().find(".category_attribute_value_line").each(function(idx,obj){
					if($(obj).attr("sel")==0 && idx > 0){
						select_all = false;
					}
				});
				if(select_all){					
					$(this).parent().parent().find(".category_attribute_value_line").eq(0).attr("sel","1").css("background-position","0px -21px").css("color","#4d4d4d");
				}else{
					$(this).parent().parent().find(".category_attribute_value_line").eq(0).attr("sel","0").css("background-position","0px 0px").css("color","#666666");
				}
				//Dealing with the "the select all" button of the state
			}			
		});
		$(".user_ratings_line").click(function(){
			$(".user_ratings_line").each(function(idx,obj){
				$(obj).removeClass("selected");
			});

			if($(this).attr("sel")==0){				
				$(this).parent().parent().find(".user_ratings_line").each(function(idx,obj){
					$(obj).attr("sel","0").css("color","#666666");
				});	
				js_category.generate_search_filter(this);
				$(this).attr("sel","1").addClass("selected");
			}else{
				js_category.generate_search_filter(this);
				$(this).attr("sel","0").removeClass("selected");
			}
		});
		$(".by_price_line").click(function(){
			if($(this).attr("sel")==0){				
				$(this).parent().parent().find(".by_price_line").each(function(idx,obj){
					$(obj).attr("sel","0").css("color","#666666");
				});	
				js_category.generate_search_filter(this);
				$(this).attr("sel","1").css("color","#C44A9A");
			}else{
				js_category.generate_search_filter(this);
				$(this).attr("sel","0").css("color","#666666");
			}
		});
		$(".prev_page").hover(
			function(){
				if($(this).attr("p")==-1) return;
				$(this).addClass("hover");
			},
			function(){
				if($(this).attr("p")==-1) return;
				$(this).removeClass("hover");
		});
		$(".next_page").hover(
			function(){
				if($(this).attr("p")==-1) return;
				$(this).addClass("hover");
			},
			function(){
				if($(this).attr("p")==-1) return;
				$(this).removeClass("hover");
		});
		$(".dd_sort_by").change(function(){
			$("#sort_by").val($(this).val());
			js_category.search_filter_refresh();
		});
		$("#by_brand").change(function(){
			if($(this).val()>0){
				js_category.generate_search_filter1(this);
			}else{
				var s = "";
				$(".search_filters_item").each(function(idx,o){
					s = $(o).attr("id");
                    if(s.indexOf("filter_bybrand_")!=-1){
                        $(o).remove();
                    }
				});
				js_category.search_filter_refresh();
			}
		});
		$("#by_manufacturer").change(function(){
			if($(this).val()>0){
				js_category.generate_search_filter1(this);
			}else{
				var s = "";
				$(".search_filters_item").each(function(idx,o){
					s = $(o).attr("id");
					if(s.indexOf("filter_bymanufacturer_")!=-1){
						$(o).remove();
					}
				});
				js_category.search_filter_refresh();
			}
		});
		$(".button_go").click(function(){
			var price_from = parseFloat($("#by_price_custom_from").val());
			var price_to = parseFloat($("#by_price_custom_to").val());

			if(!isNaN(price_from) && !isNaN(price_to) && price_from>=0 && price_to>0 && price_from<=price_to){
				var s = "";
				$(".search_filters_item").each(function(idx,o){
					s = $(o).attr("id");
					if(s.indexOf("filter_byprice_")!=-1){
						$(o).remove();
					}
				});

				var html_filter_item = "";		
				var aid;
				var id_name = "byprice_0";
				var filter_title = "Price: "
				var filter_text = "$"+price_from+" to $"+price_to;
				html_filter_item += js_category.get_html_filter_item(0,id_name,aid,+price_from+','+price_to,filter_title,filter_text);

				$("#search_filters").append(html_filter_item);
				
				js_category.search_filter_refresh();
			}else{
				return;
			}
		});
		$(".product_img,.hot_product_img").hover(
			function(){
				$(this).find(".grid_quick_view").show();
			},
			function(){
				$(this).find(".grid_quick_view").hide();
		});
		$(".product_img_list").hover(
			function(){
				$(this).find(".list_quick_view").show();
			},
			function(){
				$(this).find(".list_quick_view").hide();
		});

//		$('.wish_heart').hover(function(){$(this).addClass('hover').parent().parent().find('.wish_tips').fadeIn();},function(){$(this).removeClass('hover').parent().parent().find('.wish_tips').slideUp();});
//		$('.list_star').hover(function(){$(this).find('.review_tips').fadeIn();},function(){$(this).find('.review_tips').slideUp();});
//		$('.checkbox.compare').hover(function(){$(this).find('.compare_tips').fadeIn();},function(){$(this).find('.compare_tips').slideUp();});
		
//        js_category.init_filter();
        
        
        
//        $('body').bind('click',function(e) {
//          if($(e.target).is('.btn_ext_active_icon')|| 
//        		  ( !$(e.target).is('.filter_div') && !$(e.target).is('.btn_ext_active_back')  
//        		  && !$(e.target).is('.btn_ext')&& !$(e.target).is('.filter_item')
//        		  && !$(e.target).is('.filter_item_content') &&!$(e.target).closest('.ext_content').length )){
//              $('.ext_content').hide();
//          }
//      });

    }// end of function int()
	
};
(function(){js_category.ini();})();