var js_user={
	gift:{
		view:function(){

			g_js.windows.dialog.show('#id_dialog_for_gift');
		}
		,
		chk_agree_clicked:function(){
			var if_checked = $("#chk_agree").attr("checked");
			if(if_checked =="checked" || if_checked ==true){
				$("#btn_agree").removeAttr("disabled");
			}else{
				$("#btn_agree").attr("disabled","disabled");
			}
		},
		btn_agree_clicked:function(customer_id){
			g_js.js.ajax.fg.get(0,26,'customer_id='+customer_id,function(v){
				if(v=="1"){					
					window.location.href = 'user5744.html?v_view_id=14';
				}else{
					$.jq_alert(v);
				}
			});
		},
		ini:function(){

			g_js.windows.dialog.create	(
										'#id_dialog_for_gift',
										function() { 
											
											/* query
											 *
											 */
											g_js.js.ajax.fg.post(0,20,'#id_form_for_cst_order_gift_card_balance',null,function(v){
												
												$.jq_alert(v);
											});
										});
		}
	}
	,
	order:{
		pay_for_tenpay:function( ){
			
			/*window.location.href=$('#id_gf_pay_for_tenpy').html();*/
		}
		,
		pay_for_alipay:function(v_id){/*g_js.js.ajax.fg.get(0,27,'v_order_id='+v_id,function(v){$('#id_form_for_alipay').html(g_js.json.get(v).result);});*/}
		,
		printx:function(){

			$(".shipping_address_info1,.billing_address_info1").css({width:'300px'});
		
			/* update ui
			 *
			 */		
			var o = $(window.frames['order_print'].window);$(o.get(0).document.getElementById ('id_for_p')).html($('#id_payment_process_ex').html());
			var d = o.get(0).document;
		

			/* hide button
			 *
			 */
			for(var i=0;i<d.images.length;i++){

				var o_i = d.images[i];if(o_i.src.indexOf('button_print_this_form.html')!=-1||o_i.src.indexOf('button_continue_shopping.html')!=-1){$(o_i).hide();}
			}

			/* print
			 *
			 */			
			f = o.get(0);f.focus();f.print();
		}
		,
		tracking:{
			show:function(v_order_id){

				/* get order tracking history
				 *
				 */
				g_js.js.ajax.fg.get(0,22,'v_order_id='+v_order_id,function(v){$('#id_for_order_tracking').html(v);});

				/* get order product tracking history
				 *
				 */
				g_js.js.ajax.fg.get(0,23,'v_order_id='+v_order_id,function(v){$('#id_for_order_product_tracking').html(v);});
				
				/* show ui
				 *
				 */				
				g_js.windows.dialog.show('#id_dialog_order_tracking');
			}
			,
			ini:function(){

				g_js.windows.dialog.create	(
												'#id_dialog_order_tracking',
												function() {},
												function() {},
												true,
												'800'
											);
			}
		}
		,
		m_n_current_order_id:0	/* current order id */
		,
		view:function(v_url){window.open(v_url);}
		,
		cancel:function(v_id){if(g_js.windows.confirm.info( js_language.cst_are_you_sure_continue )){this.m_n_current_order_id = v_id;g_js.windows.dialog.show('#id_dialog_order_cancel');}}
		,
		payit:function(v_id){
			g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.get(0,24,'v_order_id='+v_id,function(v){
					var o = g_js.json.get(v); 
					if (o.type == 0){
						window.location.href = o.result;
					} else if (o.type == 2){
						var error = o.result;
						$.jq_alert(error);
					} else {
						$.jq_alert(js_language.cst_order_timeout);
					}
					g_js.windows.mask.busy.close();
					});

		},
		ini:function(){	
			
			g_js.windows.dialog.create	(
										'#id_dialog_order_cancel',
										function() { 
											
											var v_cancel_order_comment = $('#id_for_cancel_order').val();
											if( v_cancel_order_comment == '' ){ $.jq_alert(js_language.cst_order_remark_is_required);return}

											/* cancel order
											 *
											 */
											g_js.js.ajax.fg.post(0,19,'#id_form_for_cancel_order','v_id='+js_user.order.m_n_current_order_id,function(v){

												$('#id_my_order_list_one_item_'+js_user.order.m_n_current_order_id+'_order_status').html(v);
												$('#id_my_order_list_one_item_'+js_user.order.m_n_current_order_id+'_cancel_button').hide('fast');
											});
										});
		}
	}
	,
	baby:{
		create:function(){g_js.js.ajax.fg.post(0,16,'#id_form_for_baby',null,function(v){$.jq_alert(v);});}
		,
		agree:function(){if($('#id_for_baby_wishlist_agree').get(0).checked){$('#id_baby_create_button').show('fast');}else{$('#id_baby_create_button').hide('fast');}}
	}
	,
	wedding:{			
		deletex:function(){g_js.js.ajax.fg.get(0,21,null,function(v){js_go_to(window.location.href);});}
		,
		ab_same_as_before:function(){

			if( $('#id_ab_same_as_before_event').get(0).checked ){
	
				if(!g_js.windows.confirm.info( js_language.cst_are_you_sure_continue )){ $('#id_ab_same_as_before_event').get(0).checked = false;return;}

				/* hide create button
				 *
				 */
				$('#id_for_ab_create_after').hide('fast');

				/* get second wedding after address
				 *
				 */
				var v_ab_id_0 = $('#id_for_wedding_wishlist_ab_id').val();
				var v_wishlist_id = $('#id_for_wedding_wishlist_id').val();
				if( ''== v_ab_id_0){$.jq_alert(js_language.cst_wedding_for_create_first_ab);}else{
					
					g_js.js.ajax.fg.get(0,15,'v_ab_id_0='+v_ab_id_0+'&v_wishlist_id='+v_wishlist_id,function(v){
						
						$('#id_wishlist_wedding_after').html( v );
					});}
			}else{$('#id_for_ab_create_after').show('fast');}
		}
		,
		ab_create_after:function(){ this.ab_create_before('yes');	}
		,
		ab_create_before:function(v_ab_2){

			v_ab_2 = (js_is_null(v_ab_2))?'':'yes';

			var v_ok_click	  = false;
			var v_wishlist_id = $('#id_for_wedding_wishlist_id').val();if( v_wishlist_id == '' ){$.jq_alert(js_language.cst_wedding_for_create_first);return;}
			g_js.js.ajax.fg.get(0,8,null,function(v){
				
					g_js.windows.html.create_popup(v,function(){$('#id_for_create_new_address_book').click(function(){
						
						v_ok_click = true;				/* have click ok	*/

						/* update wishlist id
						 *
						 */
						g_js.js.ajax.fg.get(0,14,'v_ok_click='+v_ok_click+'&v_wishlist_id='+v_wishlist_id+'&v_ab_2='+v_ab_2,function(v){
														
							var o = g_js.json.get(v);	/* get json */
							if( v_ab_2 == '' ){

								/* before
								 *
								 */
								$('#id_wishlist_wedding_before').html	( o.ab_text ); /* address book		*/
								$('#id_for_wedding_wishlist_ab_id').val	( o.ab_id	); /* address book id	*/
							}else{

								/* after
								 *
								 */
								$('#id_wishlist_wedding_after').html	( o.ab_text );
							}

							/* close
							 *
							 */
							g_js.windows.html.close_popup();
						});
					});},600,555,true,function(){});
			});
		}
		,
		save:function(){

			if(''==$('input:[name=Registry_Name]').val()){	$.jq_alert(js_language.cst_wedding_for_title	);return;	}
			g_js.js.ajax.fg.post(0,13,'#id_form_wedding_create',null,function(v){

				var o = g_js.json.get(v);
				if('ok'==o.ok){
					
					/* get wishlist id
					 *
					 */
					$.jq_alert(o.ok);$('#id_for_wedding_wishlist_id').val(o.id);
				}else{$.jq_alert(js_language.cst_wedding_for_crreate);}
			});	
		}
	}
	,
	wish_list:{	
		wl_add_to_cart_ex:function(v_pid,v_wish_list_id){js_cart.add_wish_to_cart(v_pid,v_wish_list_id,1);}
		,
		wl_add_to_cart:function(v_pid){js_cart.add_to_cart(0,v_pid,1);}
		,
		wl_delete:function(){

			if(g_js.windows.confirm.info( js_language.cst_are_you_sure_delete )){

				/* is system default wislist
				 *
				 */
				if(3==$('select[name=name_wishlist] option:selected').attr('wishlist_type')){$.jq_alert(js_language.cst_wishlist_is_default);return;}
				
				/* wishlist id
				 *
				 */
				var v_current_wishlist_id	= $('select[name=name_wishlist] option:selected').val();
				g_js.js.ajax.fg.get(0,12,'v_current_wishlist_id='+v_current_wishlist_id,function(v){
					
					/* remove
					 *
					 */
					var o_s = $('select[name=name_wishlist]').get(0);o_s.options.remove(o_s.selectedIndex);
				});	
			}
		}
		,
		wl_modify:function(){

			/* is system default wislist
			 *
			 */
			if(3==$('select[name=name_wishlist] option:selected').attr('wishlist_type')){$.jq_alert(js_language.cst_wishlist_is_default);return;}

			/* go...
			 *
			 */
			var v_current_wishlist_id	= $('select[name=name_wishlist] option:selected').val();window.location.href='userfaef.html?v_view_id=3&amp;v_current_wishlist_id='+v_current_wishlist_id;
		}
		,
		wl_share_by_email:function(){

			var v_for_send_me_a_copy	= $('#id_for_send_me_a_copy').get(0).checked;
			var v_current_wishlist_id	= $('input:[name=name_for_current_wishlist_id]').val();
			g_js.windows.mask.busy.show();g_js.js.ajax.fg.post(0,11,'#id_share_wishlist','v_for_send_me_a_copy='+v_for_send_me_a_copy+'&v_current_wishlist_id='+v_current_wishlist_id,function(v){
				
				g_js.windows.mask.busy.close();
				g_js.windows.html.close_popup();
			});
		}
		,
		wl_share:function( ){

			/* is public wishlist
			 *
			 */
			var v_is_public = ($('select[name=name_wishlist] option:selected').attr('is_public'));
			if( v_is_public == 1 ){	$.jq_alert(js_language.cst_wishlist_is_private);		return;		}

			/* send to my friend
			 *
			 */
			var v_current_wishlist_id	= $('select[name=name_wishlist] option:selected').val();
			g_js.js.ajax.fg.get(0,10,'v_current_wishlist_id='+v_current_wishlist_id,function(v){

				g_js.windows.html.create_popup(v,function(){},800,500,true);
			});			 
		}
		,
		wl_modify_wishlist_product:function(v_wishlist_id){
			/* current address book id
			 *
			 */
			var v_ad_id	= $('#id_for_all_address_book').val();

			/* get all product related information to make key value pair return
			 *
			 */
			var v_v = '';
			$('.wishlist_one_item').each(function(i){

				var v_id			= ($(this).attr('attr_wishlist_products_id'));
				var v_priority		= ($('select[name=name_for_wishlist_product_priority_'+v_id+'] option:selected').val());
				var v_desired		= ($('input[name=name_for_desired_'+v_id+']').val());
				var v_received		= ($('input[name=name_for_received_'+v_id+']').val());
				var v_wishlist_id	= ($('select[name=name_wishlist_'+v_id+'] option:selected').val());

				v_v += 'v_id='			+v_id		+ g_js.string.spliter();
				v_v += 'v_priority='	+v_priority	+ g_js.string.spliter();
				v_v += 'v_desired='		+v_desired	+ g_js.string.spliter();
				v_v += 'v_received='	+v_received + g_js.string.spliter();
				v_v += 'v_wishlist_id='	+v_wishlist_id ;

				v_v += g_js.string.spliter()+g_js.string.spliter();
			});	
			
			/* update ui
			 *
			 */
			var v_this = this;
			g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.get(6,2,'v_wishlist_id='+v_wishlist_id+'&v_ad_id='+v_ad_id+'&v_v='+v_v,function(v){
				
				v_this.wl_show();
				g_js.windows.mask.busy.close();
			});
		}
		,
		wl_delete_a_product:function(v_id){
			$.jq_confirm( js_language.cst_are_you_sure_delete,null,null,'js_user.wish_list.wl_delete_a_product_c('+v_id+')','');
		},
		wl_delete_a_product_c:function(v_id){
			g_js.js.ajax.fg.get(6,1,'v_id='+v_id,function(v){$('#id_wishlist_one_item_'+v_id).hide('fast');});
		}
		,
		wl_continue_shopping:function(){js_go_to(js_get_web_root());}
		,
		wl_save:function(pid,wid,obj){g_js.js.ajax.fg.get(6,0,'pid='+pid+'&wid='+wid,function(v){if( 'ok'==v ){
			$.jq_alert(js_language.cst_add_to_wishlist_success);		
			if($(obj).length>0){
				$(obj).addClass('selected');
			}
		}/*js_go_to(js_get_web_root());*/});}
		,
		wl_add_product_to_wishlist_for_baby		: function(v_product_id,v_wishlist_id){
			
			var v_url = 'wishlist_ex5284.html?v_wishlist_type=2&amp;v_p_id='+v_product_id+'&v_wishlist_id='+v_wishlist_id;js_go_to(v_url);			
		}
		,
		wl_add_product_to_wishlist_for_wedding	: function(v_product_id,v_wishlist_id){

			var v_url = 'wishlist_ex9872.html?v_wishlist_type=1&amp;v_p_id='+v_product_id+'&v_wishlist_id='+v_wishlist_id;js_go_to(v_url);			
		}
		,
		wl_add_product_to_wishlist:function(v_product_id){
			
			var v_url = 'wishlist_exa3b2.html?v_wishlist_type=0&amp;v_p_id='+v_product_id;js_go_to(v_url);
		}
		,
		wl_show:function( ){

			var v_current_wishlist_id	= $('select[name=name_wishlist] option:selected').val();	
			g_js.js.ajax.fg.get(0,7,'v_current_wishlist_id='+v_current_wishlist_id,function(v){
				$('#id_wishlist_product_list_container').html(v);

				/* auto update default address book id 
				 *
				 */				 			
				//var v_ad_id			= $('#id_for_all_address_book').val();/* current address book id */			
				//g_js.js.ajax.fg.get(6,3,'v_current_wishlist_id='+v_current_wishlist_id+'&v_ad_id='+v_ad_id,function(v){});				
			});			
		}
		,
		wl_create:function(){

			/* validate
			 *
			 */
			if(''==$('input[name=name_wishlist_name]').val()){$.jq_alert(js_language.cst_wish_list_create_name);return;}			
			g_js.js.ajax.fg.post(0,6,'#id_form_for_wish_list',null,function(v){if(v=='ok'){$.jq_alert(js_language.cst_success);js_go_to('user3f5e.html?v_view_id=4');}else{$.jq_alert(js_language.cst_fail);}});
		}
		,
		ini:function(){}
	}
	,
	ab:{
		ab_close:function(){g_js.js.ajax.fg.get(0,9,null,function(v){$('#id_select_for_all_address_book').html(v);});}
		,
		ab_create_popup:function(){g_js.js.ajax.fg.get(0,8,null,function(v){g_js.windows.html.create_popup(v,function(){},600,555,true,function(){js_user.ab.ab_close();});});}
		,
		ab_edit:function(v_id){window.location.href='?v_view_id=1&v_id='+v_id;}
		,
		ab_delete:function(v_id){
			$.jq_confirm( 'This address will be permanently deleted. Please click confirm to continue.',null,null,'js_user.ab.ab_delete_c('+v_id+')','');
		},
		ab_delete_c:function(v_id){
			g_js.js.ajax.fg.get(0,5,'v_id='+v_id,function(v){$('#id_address_book_one_item_'+js_get_user_id()+'_'+v_id).hide('fast');window.location.href=window.location.href});
		}
		,
		ab_new:function(v_id){

			if($('input[name="company"]'		).val()=="")		{$.jq_alert('Company is required!')	;return false;}
			if($('input[name="email"]'		).val()=="")		{$.jq_alert('Email is required!')	;return false;}
			if($('input[name="title"]'		).val()=="")		{$.jq_alert('Contact Title is required!')	;return false;}
			
			if($('input[name="cst_customer_firstname"]'		).val()=="")		{$.jq_alert(js_language.cst_first_name_required)	;return false;}
			if($('input[name="cst_customer_lastname"]'		).val()=="")		{$.jq_alert(js_language.cst_last_name_required)	;return false;}
			if($('select[name="cst_address_book_country"]'	).val()=='')		{$.jq_alert(js_language.cst_country_require)		;return false;}
			if($('select[name="cst_address_book_zone"]'		).val()=="")		{$.jq_alert(js_language.cst_state_require)		;return false;}
			if($('input[name="cst_customer_city"]'			).val()=="")		{$.jq_alert(js_language.cst_city_require)		;return false;}
			if($('input[name="cst_customer_street_address"]').val()=="")		{$.jq_alert(js_language.cst_save_shipping_info)	;return false;}
			if($('input[name="cst_customer_telephone"]'		).val()=="")		{$.jq_alert(js_language.cst_telephone_require)	;return false;}

			g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.post(0,4,'#id_form_for_address_book','v_id='+v_id,function(v){
				g_js.windows.mask.busy.close();
				if(v!=''){
					alert(v);
				}else{
					window.location.href='useraced.html?v_view_id=2';
				}
			});	
		}
	}
	,
	save_cc:function(){

		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(0,33,'#id_form_for_cc','',function(v){
			g_js.windows.mask.busy.close();
			if(v!=''){
				alert(v);
			}
			//window.location.href='user.php?v_view_id=2';
		});	
	},
	process_save_cc:function(){

		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(0,33,'#id_process_payment_form','page=checkout',function(v){
			g_js.windows.mask.busy.close();
			if(v!=''){
				alert(v);
			}
			//window.location.href='user.php?v_view_id=2';
		});	
	},
	select_cc:function(){
		g_js.windows.mask.busy.show();
		var cim_id = $('#id_authorizenet_cim').val();
		g_js.js.ajax.fg.post(5,18,'#id_form_for_cc','cim_id='+cim_id,function(v){
			g_js.windows.mask.busy.close();
			if(v!=''){
				$('#authoriznet_content').html(v);
			}
			//window.location.href='user.php?v_view_id=2';
		});	
	},
	user_change_settings:function(){
		var is_error = 0;
		$.jq_remove_error();
		
		if($('input[name="cst_customer_company"]').val()==''){
			$.jq_field_error($('input[name="cst_customer_company"]'), 'This is a required field');
			is_error = 1;
		}
		
		if($('input[name="cst_customer_firstname"]').val()==''){
			$.jq_field_error($('input[name="cst_customer_firstname"]'), 'This is a required field');
			is_error = 1;
		}
		
		if($('input[name="cst_customer_lastname"]').val()==''){
			$.jq_field_error($('input[name="cst_customer_lastname"]'), 'This is a required field');
			is_error = 1;
		}
		
		if($('input[name="agent"]').val()==''){
			$.jq_field_error($('input[name="agent"]'), 'This is a required field');
			is_error = 1;
		}
		
		if($('input[name="cst_customer_telephone"]').val()==''){
			$.jq_field_error($('input[name="cst_customer_telephone"]'), 'This is a required field');
			is_error = 1;
		}
		
		if(is_error==1){
			return false;
		}
		g_js.windows.mask.busy.show();g_js.js.ajax.post(g_js.js.dispatch_fg(0,3),$('#id_form_for_modify_customer').serialize(),function(v){

			g_js.windows.mask.busy.close();
			var o = g_js.json.get(v);
			if (o.error == 1){
				$.jq_field_error($('input[name="'+o.field+'"]'), o.result);
			}else{
				$.jq_alert('Profile information has been saved');
				//js_user.refresh_page();
			}
			
		});
		}
	,
    user_create:function(v_go_to_url){
        v_go_to_url = ((js_is_null(v_go_to_url))?'':v_go_to_url);

        /* create an account
         *
         */
        var user_name = $("#id_new_user_account_name").val();
        if (js_validate_mail(user_name)){
            var user_pwd		= $("#password").val();
            var user_rep_pwd	= $("#id_new_user_account_reppwd").val();
            if (user_pwd.length < 6)		{alert(js_language.cst_password_error_1);return false;}
            if (user_pwd != user_rep_pwd)	{alert(js_language.cst_password_error_2);return false;}
            g_js.windows.mask.busy.show();
            g_js.js.ajax.fg.post(0,2,'#id_form_create','v_go_to_url='+v_go_to_url,function(v){
                if('ok'==v){js_go_to((''==v_go_to_url)?'user.php': v_go_to_url);}else{g_js.windows.mask.busy.close();alert(v);}
            });
        }

    },
	user_create_or_checkout:function(v_go_to_url){
		v_go_to_url = ((js_is_null(v_go_to_url))?'':v_go_to_url);

		var v_guest = arguments[1]?arguments[1]:0; 
		if(v_guest==0){

					/* create an account 					 
					 *
					 */
					var user_name = $("#id_new_user_account_name").val();
					if (js_validate_mail(user_name)){

						var user_pwd		= $("#password").val();
						var user_rep_pwd	= $("#id_new_user_account_reppwd").val();
						if (user_pwd.length < 6)		{$.jq_alert(js_language.cst_password_error_1);return false;}
						if (user_pwd != user_rep_pwd)	{$.jq_alert(js_language.cst_password_error_2);return false;}
						g_js.windows.mask.busy.show();
						g_js.js.ajax.fg.post(0,2,'#id_form_create','v_go_to_url='+v_go_to_url,function(v){

							if('ok'==v){js_go_to((''==v_go_to_url)?'user.php': v_go_to_url);}else{g_js.windows.mask.busy.close();$.jq_alert(v);}
						});
					}
		
		}

		if(v_guest==1){

					/* guest mode 
					 *
					 */					
					if(v_go_to_url.indexOf ('?')== -1 ){ v_go_to_url = v_go_to_url+'?0=0';}		/* guest		*/
					var v_guest_email = $('#id_create_anccount_for_guest').val();				/* guest email	 */
					if(js_validate_mail(v_guest_email)){js_go_to(v_go_to_url+'&guest_email_address='+v_guest_email);}
		}
		

	}
	,
	create_configurator_user:function(){

		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(0,34,'#configurator_start_form','',function(v){

			if('ok'==v){
				js_go_to('configurator0c17.html?step=1');
			}else{
				g_js.windows.mask.busy.close();
				$.jq_alert(v);
			}
		});
	}
	,
	user_guest_checkout:function(v_go_to_url){
		v_go_to_url = ((js_is_null(v_go_to_url))?'':v_go_to_url);

		/* guest mode 
		 *
		 */					
		if(v_go_to_url.indexOf ('?')== -1 ){ v_go_to_url = v_go_to_url+'?0=0';}		/* guest		*/
		var v_guest_email = $('#id_create_anccount_for_guest').val();				/* guest email	 */
		if(js_validate_mail(v_guest_email)){js_go_to(v_go_to_url+'&guest_email_address='+v_guest_email);}
	
		return false;
	
	},
	login_ex:function(v_go_to_url){

 		/* login check
		 *
		 */												 
		g_js.js.ajax.fg.post(0,1,'#id_form_login',null,function(v){
			if(v=='ok'){
				js_go_to(((js_is_null(v_go_to_url))?'TeamTG.html':v_go_to_url));
			}else{
				if (v=='need to retail')
				{
					$.jq_alert('Retailer accounnts must log in through the RETAIL log in page. ');
				}else if (v=='need to wholesale')
				{
					$.jq_alert('Wholeseller accounts must log in through the WHOLESALE log in page.');
				}else{
					$.jq_alert('The email address and/or password you provided was incorrect. Please check your spelling and try again.');
				}
			}
		});
	}
	,
	login_bak:function(v_go_to_url,v_login_type){

		v_go_to_url		= ((js_is_null(v_go_to_url))?'':v_go_to_url)	;	/* will go to url	*/
		if (v_login_type == 0) {} else {v_login_type = 1;}

		//v_login_type	= (js_is_null(v_login_type))?1:v_login_type		;	/* logon type		*/
		/* user current login status
		 *
		 */
		if( js_get_user_id()==0 || g_guest_flag == 1){

			/* i am gust only, i want to login or register
			 *
			 */
			g_js.js.ajax.fg.get(0,0,'v_go_to_url='+v_go_to_url+'&v_login_type='+v_login_type,function(v){var pop_height = 530;if (v_login_type == 1) {pop_height = 420;}

				g_js.windows.html.create_popup_big(v,function(){
					
					/* bind keyboard event for enter key to do login 
					 *
					 */
					
					g_js.keyboard.enter($('#id_text_password'),function(){ js_user.login_ex(v_go_to_url); });
					$('.lr_container').addClass('popup');
					$('.lrc_continue').addClass('login_popup');
				},1100,435,true,null,null,null,true,0.7);
			});			 
		}
	},
    login:function(v_go_to_url,v_login_type){

        v_go_to_url		= ((js_is_null(v_go_to_url))?'':v_go_to_url)	;	/* will go to url	*/
        if (v_login_type == 0) {} else {v_login_type = 1;}

        //v_login_type	= (js_is_null(v_login_type))?1:v_login_type		;	/* logon type		*/
        /* user current login status
         *
         */
        if( js_get_user_id()==0 || g_guest_flag == 1){

            /* i am gust only, i want to login or register
             *
             */
            g_js.js.ajax.fg.get(0,0,'v_go_to_url='+v_go_to_url+'&v_login_type='+v_login_type,function(v){
                var pop_width = 860;
                if (v_login_type == 1) {
                    pop_width = 780;
                }

                g_js.windows.html.create_popup(v,function(){

                    /* bind keyboard event for enter key to do login
                     *
                     */

                    g_js.keyboard.enter($('#id_text_password'),function(){ js_user.login_ex(v_go_to_url); });
                },pop_width,420,true);
            });
        }

        setTimeout(js_user.pop_init,1000);

    },
    pop_init:function(){
    	
    	$('#id_create_anccount_for_guest').keydown(function(e){
			if(e.keyCode==13){
				$('.btn_guest').trigger('click');
			}
		});
    	
    	$('#login_password').keydown(function(e){
			if(e.keyCode==13){
				$('.btn_login').trigger('click');
			}
		});
		
		
		$('#id_new_user_account_reppwd').keydown(function(e){
			if(e.keyCode==13){
				$('.btn_reg').trigger('click');
			}
		});
		
		
    }
	,
	is_user_center_page:function(){ return g_js.page.is_eq('user.html');	}
	,
	user_change_country_or_province:function(v_sel_for_country,v_sel_for_province,v_default_val){

 		var v_sp		= ($(v_sel_for_province).get(0));
 		if( v_sp==null ) return;
  		var ar_current	= (ar_sp[ g_js.form.select.attr(v_sel_for_country,'attr_id') ]);
  		
		/* update select item data
		 *
		 */
		while(v_sp.options.length>0){v_sp.remove(0);}
		for(var i=0;i<ar_current.length;i++){ $(v_sp).append("<option value='"+ar_current[i].split('|')[0]+"'>"+ar_current[i].split('|')[1]+"</option>");}

		/* select default value 
		 *
		 */
		if( !js_is_null(v_default_val) ){$(v_sel_for_province).val(v_default_val);}
		
		if(v_sel_for_province.hasClass('msdropdown')){
			var oDropdown = v_sel_for_province.msDropdown().data("dd");
			oDropdown.destroy();
			v_sel_for_province.msDropdown();
		}
	}
	,
	change_page:function(p){g_js.js.ajax.fg.get(0,25,'page='+p,function(v){$('#revard_list').html(v);});},
	quick_track_ex:function() {
		var order_id = $('#id_text_order_id').val();
		var user_email = $('#id_text_user_email').val();
		var zip_code = $('#id_text_zip_code').val();
		if (order_id.replace(/ /g,'') == ''){
			$.jq_alert("Please input your order id.");
			return false;
		}
		
		if (user_email.replace(/ /g,'') == '' && zip_code.replace(/ /g,'') == ''){
			$.jq_alert("Please input your email address or shipping zip code.");
			return false;
		}
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(14,1,'#id_form_check_order',null,function(v){
			 g_js.windows.mask.busy.close();
			 var o = g_js.json.get(v); 
				if (o.type == 0){
					window.location.href = o.result;
				} else {
					$.jq_alert("No order was found matching that information. Please verify the information and try again.");
				}
		});
	},
	unreview_products:{
		edit_review:function(v_product_id,v_orders_products_id){
			g_js.js.ajax.fg.get(0,29,'v_product_id='+v_product_id+'&v_orders_products_id='+v_orders_products_id,function(v){
				//$.jq_alert(v);
				g_js.windows.html.create_popup(v,function(){},800,500,true);
			});			 
		}
		,
		submit_review:function(){
			g_js.windows.mask.busy.show();
			g_js.js.ajax.fg.post(0,30,'#submit_orders_review','',function(v){
				
				g_js.windows.mask.busy.close();
				g_js.windows.html.close_popup();
				window.location.href = 'userca8a.html?v_view_id=10';
				//$.jq_alert(v);
			});
		}
		,
	},
	wholesale_registration:function(){
		var is_error = 0;
		$.jq_remove_error();
		if($.trim($('input[name="cst_customer_company"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_company"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_number"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_number"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_first_name"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_first_name"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_last_name"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_last_name"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="customer_name"]').val())==''){
			$.jq_field_error($('input[name="customer_name"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_email_address"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_email_address"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_telephone"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_telephone"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_street_address"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_street_address"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_city"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_city"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="cst_customer_postcode"]').val())==''){
			$.jq_field_error($('input[name="cst_customer_postcode"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="password"]').val())==''){
			$.jq_field_error($('input[name="password"]'), 'This is a required field');
			is_error = 1;
		}
		if($.trim($('input[name="password"]').val()).length < 6){
			
			$.jq_field_error($('input[name="password"]'), 'Password must be at least 6 characters');
			is_error = 1;
		}
		if($.trim($('input[name="password"]').val()) != $.trim($('input[name="password2"]').val())){
			$.jq_field_error($('input[name="password2"]'), 'Passwords do not match');
			is_error = 1;
		}
		if($('#cst_read').attr("checked")!="checked"){
			$.jq_alert('Price Policy required !');
			return false;
		}
		
		/*
		if($.trim($('input[name="cst_customer_contact"]').val())==''){
			$.jq_alert('Contact is required !');
			return false;
		}
		if($.trim($('input[name="cst_customer_company"]').val())==''){
			$.jq_alert('Company is required !');
			return false;
		}
		if($.trim($('input[name="cst_customer_street_address"]').val())==''){
			$.jq_alert('Bus. Address is required !');
			return false;
		}
		if($.trim($('input[name="cst_customer_postcode"]').val())==''){
			$.jq_alert('Zip / Postal Code is required !');
			return false;
		}
		if($.trim($('input[name="cst_customer_city"]').val())==''){
			$.jq_alert('City is required !');
			return false;
		}
		if($.trim($('#id_cst_address_book_country').val())==''){
			$.jq_alert('Country is required !');
			return false;
		}
		if($.trim($('#id_cst_address_book_zone').val())==''){
			$.jq_alert('State / Province is required !');
			return false;
		}
		if($.trim($('input[name="cst_customer_telephone"]').val())==''){
			$.jq_alert('Business Phone is required !');
			return false;
		}
		
		if($.trim($('input[name="cst_customer_resale_no"]').val())==''){
			$.jq_alert('Resale/Business/FFL license# is required !');
			return false;
		}
		if($.trim($('#cst_m_sales').val())==''){
			$.jq_alert('Monthly sales volume of business is required !');
			return false;
		}
		if($.trim($('input[name="cst_print_name"]').val())==''){
			$.jq_alert('Print Name is required !');
			return false;
		}
		if($.trim($('input[name="cst_e_signature"]').val())==''){
			$.jq_alert('E-Signature is required !');
			return false;
		}
		*/
		
		if(is_error==1){
			return false;
		}
		
		g_js.windows.mask.busy.show();
		g_js.js.ajax.fg.post(0,31,'#id_wholesale_registration_form','',function(v){			
			g_js.windows.mask.busy.close();
			if(v.length>0){
				if(v==1){
					$('.send_contactus').fancybox({
						'centerOnScroll':true,
						'autoDimensions':false,
						'width':720,
						'height':340,
						'titleShow':false,
						'type':'ajax',
						'href':'ajaxget_message.php?page=user&itemid=1&message='
					});
					$('.send_contactus').click();
					/*
					var msg = 'You will receive an email shortly saying we received your application, and once is verified and approved, your account will be activated.';
					alert(msg);
					window.location.href = 'default.php';
					*/
				}
				else{
					$.jq_field_error($('input[name="cst_customer_email_address"]'), v);
					return false;
				}
			}
		});
	},
	show_wholesale_terms:function(id){
		g_js.js.ajax.fg.get(0,32,null,function(v){
			g_js.windows.html.create_popup(v,function(){
			},790,400,true,null,null,null,true,0.7);
		});	
	},
	clear_wholesale_registration:function(){
		$('#id_wholesale_registration_form input').val('');
	},
	refresh_page:function(){
		window.location.href = window.location.href;
	},
	ini:function(){
		
		/* check user session
		 *
		 */
		//if( this.is_user_center_page() ){ this.login(); }

		/*
		 *
		 */
		this.user_change_country_or_province($('#id_cst_address_book_country'),$('#id_cst_address_book_zone'),$('#id_cst_address_book_zone').attr('attr_val'));
		this.user_change_country_or_province($('#id_Country'),$('#id_Wedding_State'),$('#id_Wedding_State').attr('attr_val'));

		/* wish list
		 *
		 */
		this.wish_list.ini();

		/* order
		 *
		 */
		this.order.ini();
		this.order.tracking.ini();
		/* gift
		 *
		 */
		this.gift.ini();

		$('.bke').keydown(function(e){
			if(e.keyCode==13){
				var bid = $('.bke').attr('bid');
				$('#'+bid).trigger('click');
			}
		});
		
	}
};$().ready(function(){js_user.ini();});


function show_apply()
{
	if($('#authorizenet_save').attr("checked")=="checked")
	{
		$('#cc_nickname').show();
	}
	else
	{
		$('#cc_nickname').hide();
	}
}