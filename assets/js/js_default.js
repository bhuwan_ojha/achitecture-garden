
var js_default={
	products_slider_width:980,//一次滚动宽度
	display_one_page:5,//一次显示的商品个数
	display_two_page:5*2,
	cnt_tab:6,//TAB的个数
	process_products_slider:function(){
		$(".tab_nav ul li a").click(function(){
			if($(".items_list_content:animated").length > 0) return false; 

			var si = $(".tab_nav ul li a").index(this);

			if($(".items_list_content").eq(si).find("ul").length==0){
				js_default.ini_products_slider(si);
			}

			$(".tab_nav ul li.selected").removeClass("selected");
			$(this).parent().addClass("selected");

			$(".items_list_content.selected").removeClass("items_list_content selected").addClass("items_list_content");
			$(".items_list_content").eq(si).addClass("items_list_content selected");

		});

		$(".button_left").click(function(){
			if($(".items_list_content.selected").attr("rv") == "0"){
				return false;
			}

			$(".items_list_content.selected:not(:animated)").animate({marginLeft: '+='+js_default.products_slider_width}, "slow",function(){
				$(".items_list_content.selected").attr("rv",parseInt($(".items_list_content.selected").attr("rv")) - 1);
			});

		});
		$(".button_right").click(function(){
			var ci_content = parseInt($(".items_list_content.selected").attr("rv"));
			if( ci_content +1 == $(".items_list_content.selected ul").length){
				return false;
			}
			
			$(".items_list_content.selected:not(:animated)").animate({marginLeft: '-='+js_default.products_slider_width}, "slow",function(){
				$(".items_list_content.selected").attr("rv",ci_content+1);
			});

			var ci_item = $(".items_list_content").index($(".items_list_content.selected"));
			var condition = 'limit='+(js_default.display_two_page+js_default.display_one_page*ci_content)+','+js_default.display_one_page;

			if(ci_content + 2 == $(".items_list_content.selected ul").length){
				g_js.js.ajax.fg.post(4,ci_item,"",condition,function(v){
					$(".items_list_content.selected").css("width",js_default.products_slider_width*(ci_content+3)).append(v);
				});	
			}
		});
	}
	,
	ini_products_slider:function(si){
		g_js.js.ajax.fg.post(4,si,"","limit=0,"+js_default.display_two_page,function(v){				
			$(".items_list_content").eq(si).addClass("selected").append(v).css("width",js_default.products_slider_width*2);	
		});	
	}
	,
	ini:function(){
		js_default.process_products_slider();
		var i,item_list_content;
		for(i=0;i<js_default.cnt_tab;i++){
			$(".items_list").append('<div class="items_list_content" rv="0"></div>');
		}
		
		js_default.ini_products_slider(0);

        var featured_from = 0;
        $(document).ready(function(){
            $('#loadmore').click(function() {
                featured_from += 4;
                var total = parseInt($(this).attr('total'));
                g_js.windows.mask.busy.show();
                g_js.js.ajax.fg.get(2,5,'from='+featured_from,function(v){
                    $('#featured_content').append(v);
                    setTimeout(js_category.lazyload,200);
                    
                    if((featured_from+4)>=total){
                        $('#loadmore').hide();
                    }
                    g_js.windows.mask.busy.close();
                });
            });


            var new_from = 0;
            $('#loadmore_new').click(function() {
                new_from += 4;
                var total = parseInt($(this).attr('total'));
                g_js.windows.mask.busy.show();
                g_js.js.ajax.fg.get(2,6,'from='+new_from,function(v){
                    $('#new_content').append(v);
                    
                    setTimeout(js_category.lazyload,200);
                    
                    if((new_from+4)>=total){
                        $('#loadmore_new').hide();
                    }
                    g_js.windows.mask.busy.close();
                });
            });

        });

	}// end of function int()	
};
(function(){js_default.ini();})();