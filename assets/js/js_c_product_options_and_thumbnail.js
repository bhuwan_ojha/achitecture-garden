/* js core code be processed for product thumbnail and product option 
 * All rights reserved Ruiit.com Co., Ltd. Chengdu,Sichuan
 * js core code Written By Breezer,2011.12.08
 * 
 */

/* product thumbnail
 *
 */
var c_product_options_pic	= function (){
	
	var m_thumbnail_class_name		;
	var m_optionsx					;
	var m_str_thumbnail_image_id	;
}
c_product_options_pic.prototype.ini	= function(v_thumbnail_class_name,v_optionsx,str_thumbnail_image_id){
	
	var v_this						= this						;
	this.m_str_thumbnail_image_id	= str_thumbnail_image_id	;
	this.m_thumbnail_class_name		= v_thumbnail_class_name	;
	this.m_optionsx					= v_optionsx				;	

	/* thumbnail click event
	 *
	 */
	$('.'+this.m_thumbnail_class_name).click(function(){v_this.pic_click_product_option_picture( $(this).attr('attr_options_value_id'));});
} 
c_product_options_pic.prototype.option_picture_reset			 = function(){
	
	$('.product_options_class_for_image_ex').each(function(i,o){$(o).get(0).src = $(o).get(0).src.replace($(o).attr('attr_img_src_ok'),$(o).attr('attr_img_src'));});
}
c_product_options_pic.prototype.pic_show = function(v_img_id,v_url){
	//mimagediv
	g_js.js.ajax.fg.get(1,10,'m_image_name='+v_url,function(v){
		$("#mimagediv").attr("style",v);		
		$(v_img_id).get(0).src = js_get_web_root()+v_url;
	});	
}

c_product_options_pic.prototype.change_product_big				 = function(v_pic_url){this.pic_show('#'+this.m_str_thumbnail_image_id,v_pic_url)	;}
c_product_options_pic.prototype.option_box_mouseover_highlight   = function(fb){
	//$(fb).css({"text-decoration":"underline"});
	//$(fb).css({"border":"solid 2px #FF8500","padding":"2px"});
}
c_product_options_pic.prototype.option_box_mouseout_recover   		= function(fb){
	//$(fb).css({"text-decoration":"none"});
	
}
c_product_options_pic.prototype.pic_click_product_option_picture = function(v_option_id,v_img_this){
	var v_this = this;
	/* reset use default product option
	*
	*/
	js_product_detail.custom.m_b_custom_product_option = false;
	/* highlight option picture
	*
	*/
	if(!js_is_null(v_img_this)){
		$('.product_options_class_for_image_ex').each(function(i,o){
			if($(v_img_this).attr('attr_row_index') == $(o).attr('attr_row_index')){
				/* check row index,reset option picture
				 *
				*/
				$(o).get(0).src = $(o).get(0).src.replace($(o).attr('attr_img_src_ok'),$(o).attr('attr_img_src'));
			}
		});
		/* hight light selected thumbnail
		*
		*/		
		$(v_img_this).get(0).src = $(v_img_this).get(0).src.replace($(v_img_this).attr('attr_img_src'),$(v_img_this).attr('attr_img_src_ok'));
	}	
	/* get product options
	 *
	*/
	if(!js_is_null(v_img_this)){
		this.m_optionsx.get_optins_combination( v_img_this );
	}		
	/* change product thumbnail and big picture
	 * 
	*/
	var v_find_it = false;
	if(v_option_id.split('_')[0]!=''){
		$('.'+this.m_thumbnail_class_name).each(function(i,d){
			$(this).removeClass('p_thumbnail_selected');
		});
	}
	$('.'+this.m_thumbnail_class_name).each(function(i,d){
		if( $(this).attr('attr_options_value_id')==v_option_id ){
			v_this.change_product_big($(this).attr('attr_url_for_image'));
			$('#id_product_picture').attr('attr_url_for_image_large',$(this).attr('attr_url_for_image_large'));
			//zoomple
			if($(this).attr('class').indexOf('for_qv')<0){
				//quickview 时不调用此方法！
				$('#zoom_preview img').attr('src',$(this).attr('attr_url_for_image_zoom')).css({'width':$(this).attr('attr_url_for_image_zoom_w'),'height':$(this).attr('attr_url_for_image_zoom_h')});
			}
			if(v_option_id.split('_')[0]!=''){
				$(this).addClass('p_thumbnail_selected');	return false;
			}
		}
	});
}
c_product_options_pic.prototype.pic_click_product_option_div = function(v_option_id,v_img_this,option_id){var v_this = this;

	/* reset use default product option
	 *
	 */
	js_product_detail.custom.m_b_custom_product_option = false;
	//$('.'+option_id+'_option_box').css({"border":"solid 1px #706b73","padding":"3px","background":"none"});
	//$(v_img_this).css({"border":"solid 2px #FF8500","padding":"2px","background":"url('images/selectcolor.png') no-repeat scroll right bottom transparent"});

//	$('.'+option_id+'_option_box').css({"background-color":"#CCCACD","color":"#8FB138"});
//	$(v_img_this).css({"background-color":"#68A200","color":"#fff"});

	//$(v_img_this).unbind('mouseout');
	//var v_this = this;
	
	
			
	/* get product options
	 *
	 */
	if(!js_is_null(v_img_this)){this.m_optionsx.get_optins_combination( v_img_this );}		

	/* change product thumbnail and big picture
	 * 
	 */
//	var v_find_it = false;
	$('.'+this.m_thumbnail_class_name).each(function(i,d){
		$(this).removeClass('p_thumbnail_selected');
//		if(v_option_id==0||v_option_id.split('_')[1]==0){
//			
//		}else{
//			if(v_option_id.split('_')[1]==$(this).attr('attr_options_value_id').split('_')[1]){
//				$(this).show();
//				v_find_it=true;
//			}else{
//				$(this).hide();
//			}
//		}
	});
	
//	if(v_find_it){
//		
//	}else{
//		$('.'+this.m_thumbnail_class_name).each(function(i,o){
//			var v_v_v=$(this).attr('attr_options_value_id');
//			if(v_v_v==0||v_v_v.split('_')[1]==0){
//				$(this).show();
//			}
//		});
//	}
//	
	$('.'+this.m_thumbnail_class_name).each(function(i,d){

		if( $(this).attr('attr_options_value_id')==v_option_id ){
			v_this.change_product_big( $(this).attr('attr_url_for_image') );
			$('#id_product_picture').attr('attr_url_for_image_large',$(this).attr('attr_url_for_image_large'));
			$(this).addClass('p_thumbnail_selected');	return false;
		}
	});
}
/* product option
 *
 */
var c_product_options_x	= function (){

	var m_n_parent_product_id	;
	var m_n_child_product_id	;
	var m_str_combination_id	;
	var m_n_options_max_index	;
	var m_callback_for_update_ui;
}
c_product_options_x.prototype.ini	= function(n_parent_product_id,n_options_max_index,callback_for_update_ui){

	this.m_n_parent_product_id		= n_parent_product_id		;
	this.m_n_options_max_index		= n_options_max_index		;
	this.m_callback_for_update_ui	= callback_for_update_ui	;
}
c_product_options_x.prototype.get_optins_combination = function(v_o){	 

	/* product thumbnail css class
	 *
	 */
	var v_css_product_options_class = '.'+this.m_n_parent_product_id+'_product_options_class';

	/* update current checked option value
	 *
	 */
	var v_options_name = $(v_o).attr('attr_options_name');$(v_css_product_options_class).each(function(i,d){			
		
		if($(this).attr('attr_options_name')==v_options_name){
			$(this).attr('attr_opionts_checked','no');
//			$(this).css({'border':'1px solid #ccc'});
			$(this).removeClass('selected');
		}	/* reset stauts */				
		if(this==v_o){
			$(this).attr('attr_opionts_checked','yes');
//			$(this).css({'border':'1px solid #6c9a00'});
			$(this).addClass('selected');
		}											/* checked */
		
	});

	/* get current option id and option value id
	 *
	 */
	var vx='';$(v_css_product_options_class).each(function(i,d){if($(this).attr('attr_opionts_checked')=='yes'){vx+=$(this).attr('attr_options_value');}});

	/* last combination result
	 *
	 */
	this.m_str_combination_id = (this.m_n_parent_product_id/* product id */+''+vx/* option id and option value id */);

	/* all options select ok ?
	 *
	 */
	
	if(this.m_str_combination_id.match(/{/ig).length==this.m_n_options_max_index ){this.get_child_product_id();/* ye,try to get child prodcut id */}
} 
c_product_options_x.prototype.get_child_product_id = function(){
	g_js.windows.mask.busy.show();
	
	var v_this = this;g_js.js.ajax.fg.get(1,0,'v_combination_id='+this.m_str_combination_id,function(v){
		v_this.m_n_child_product_id = v;		/* save child product id				*/
		if (v > 0){	v_this.get_product_info(v);	/* get child product detail information */} else {g_js.windows.mask.busy.close(); $.jq_alert(js_language.cst_product_option_error); return false;}
	});
}
c_product_options_x.prototype.get_product_info = function(v_p_id){
	
	var v_this=this;g_js.js.ajax.fg.get(1,1,'v_product_id='+v_p_id,function(v){g_js.windows.mask.busy.close(); v_this.update_product_ui(g_js.json.get(v));});
}
c_product_options_x.prototype.update_product_ui = function(o){/* call back */this.m_callback_for_update_ui(o);}

/* cart
 *
 */
var c_cart				= function	(){};
c_cart.prototype.ini	= function	(){};
c_cart.prototype.add	= function	(
	v_n_qty,v_n_product_type,v_n_product_parent_id,v_n_product_child_id,v_onlyonechild_id,v_n_type){

	var v_p_id	= 0;
	//var v_p_num	= (isNaN(parseInt($(o_qty).val())))?1:parseInt($(o_qty).val());$(o_qty).val(v_p_num);
	var v_p_num	= v_n_qty;

	//如果是onlyonechild就直接给值吧，用户也别选了。
	if(v_onlyonechild_id>0){
		v_n_product_child_id=v_onlyonechild_id;
	}
	
	/* check product type
	 *
	 */
	if( 0==v_n_product_type			)	{	v_p_id	= v_n_product_parent_id	;/* single prodcut */}
	else if (1==v_n_product_type	)	{	v_p_id	= v_n_product_child_id	;/* parent product */}
	else{}

	/* validate
	 *
	 */
	if( v_p_id>0 ){}else{
		//alert(js_language.cst_please_choose_product_otions);
		//g_js.windows.html.create_popup_small('<div style="text-align:center;min-height:50px;">'+js_language.cst_please_choose_product_otions+'</div>',function(){
		//},400,100,true,null,null,null,false,0.7,'');
		$.jq_alert(js_language.cst_please_choose_product_otions);
		return;
	}
	
	/* ok,add to cart
	 *
	 */
	if(js_is_null(v_n_type)){js_cart.add_to_cart(0,v_p_id,v_p_num);}else{js_fb_cart.add_to_cart(0,v_p_id,v_p_num);}
}
//quick view
c_cart.prototype.add1	= function	(v_n_qty,v_n_product_type,v_n_product_parent_id,v_n_product_child_id,v_n_type){

	var v_p_id	= 0;
	//var v_p_num	= (isNaN(parseInt($(o_qty).val())))?1:parseInt($(o_qty).val());$(o_qty).val(v_p_num);
	var v_p_num	= v_n_qty;
	
	/* check product type
	 *
	 */
	if( 0==v_n_product_type			)	{	v_p_id	= v_n_product_parent_id	;/* single prodcut */}
	else if (1==v_n_product_type	)	{	v_p_id	= v_n_product_child_id	;/* parent product */}
	else{}

	/* validate
	 *
	 */
	if( v_p_id>0 ){}else{
		$.jq_alert(js_language.cst_please_choose_product_otions);
		return;
	}
	
	/* ok,add to cart
	 *
	 */
	if(js_is_null(v_n_type)){js_cart.add_to_cart(0,v_p_id,v_p_num);}else{js_fb_cart.add_to_cart(0,v_p_id,v_p_num);}
}